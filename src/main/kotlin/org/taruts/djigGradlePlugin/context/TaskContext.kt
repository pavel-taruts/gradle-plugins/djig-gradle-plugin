package org.taruts.djigGradlePlugin.context

import org.apache.commons.io.FileUtils
import org.gradle.api.Project
import org.gradle.api.logging.Logger
import org.springframework.context.ApplicationContext
import org.taruts.djigGradlePlugin.InitPersonalDynamicProjectsTask
import org.taruts.djigGradlePlugin.boot.SpringBootUtils
import org.taruts.djigGradlePlugin.credentials.CredentialsProvider
import org.taruts.djigGradlePlugin.credentials.SourceCredentialsProviderWrapper
import org.taruts.djigGradlePlugin.credentials.TargetCredentialsProviderWrapper
import org.taruts.djigGradlePlugin.credentials.impl.*
import org.taruts.djigGradlePlugin.extension.DjigPluginExtension
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.djigGradlePlugin.hostingPlace.HostingPlaceCreator
import org.taruts.djigGradlePlugin.hostingPlace.SourceProjectPathExtractor
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.SourceHostingStrategySelector
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.hub.SourceHubStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.lab.SourceLabStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.TargetHostingStrategySelector
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.hub.TargetHubStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab.TargetLabStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab.groupsCopying.HubToLabGroupsCopier
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab.groupsCopying.LabToLabGroupsCopier
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab.groupsCopying.ToLabGroupsCopierSelector
import org.taruts.djigGradlePlugin.prefixes.GradleProjectPropertyPrefixesProvider
import org.taruts.djigGradlePlugin.prefixes.coordinates.GradleProjectCoordinatesFactory
import org.taruts.djigGradlePlugin.prefixes.coordinates.ProjectCoordinates
import org.taruts.djigGradlePlugin.propertyWriting.*
import org.taruts.djigGradlePlugin.propertyWriting.strategies.BranchPropertyWriteStrategy
import org.taruts.djigGradlePlugin.propertyWriting.strategies.CredentialsPropertyWriteStrategy
import org.taruts.djigGradlePlugin.propertyWriting.strategies.ProjectPropertyWriteStrategy
import org.taruts.djigGradlePlugin.propertyWriting.utils.LocationUtils
import org.taruts.djigGradlePlugin.push.TargetProjectPusher
import org.taruts.djigGradlePlugin.sourceDirectory.TargetProjectSourcesCreator
import org.taruts.djigGradlePlugin.targetBranch.TargetBranchProvider
import org.taruts.djigGradlePlugin.targetBranch.impl.*
import java.io.File

/**
 * The DI container for objects that [InitPersonalDynamicProjectsTask] uses for its work directly or indirectly.
 *
 * It works with two types of objects: those created before and after [InitPersonalDynamicProjectsTask.action] is called.
 *
 * Objects created before [InitPersonalDynamicProjectsTask.action] is called (externally created objects)
 * are the task itself and those supplied by user via [DjigPluginExtension].
 * These receive their dependency injection via their init methods.
 *
 * Thus, objects created by user can be reused between [InitPersonalDynamicProjectsTask.action] calls,
 * receiving a new set of dependencies for every call
 * via their init methods.
 *
 * Objects created after [InitPersonalDynamicProjectsTask.action] is called
 * are those created by calling their constructors in the field initializers here in [TaskContext].
 * These receive their dependency injection via their constructors. They are new objects for every [InitPersonalDynamicProjectsTask] execution.
 */
class TaskContext(private val task: InitPersonalDynamicProjectsTask) {

    private val gradleProject: Project = task.project
    private val logger: Logger = task.logger

    private val extension: DjigPluginExtension = task.extension
    private val taskDescription: TaskDescription = task.taskDescription

    private val workspaceProjectDirectory: File = gradleProject.projectDir
    private val appProjectDirectoryRelativePath: String = extension.app.sourcesRelativePath.get()
    private val appProjectDirectory: File = FileUtils.getFile(workspaceProjectDirectory, appProjectDirectoryRelativePath)
    private val appClassPathResourcesDirectory: File = FileUtils.getFile(appProjectDirectory, "src/main/resources")
    private val appWorkingDirectory: File = extension.app.workingDirectory.get()

    private val appCoordinates: ProjectCoordinates = GradleProjectCoordinatesFactory.get(appProjectDirectory)
    private val gradleProjectPropertyPrefixesProvider: GradleProjectPropertyPrefixesProvider = GradleProjectPropertyPrefixesProvider(appCoordinates)

    private val springBootUtils = SpringBootUtils(extension.app, appClassPathResourcesDirectory, appWorkingDirectory)
    private val locationUtils = LocationUtils(appClassPathResourcesDirectory, appWorkingDirectory)

    private val sourceSpringBootProfile: String = extension.sourceDynamicProjects.springBootProfile.get()
    private val sourceProjectBranchMode: Boolean = taskDescription.target.sourceProjectBranchMode.get()
    private val sourceCredentialsProvider: CredentialsProvider? = extension.sourceDynamicProjects.credentialsProvider.orNull
    private val sourceCredentialsProviderWrapper = SourceCredentialsProviderWrapper(sourceCredentialsProvider, sourceSpringBootProfile)

    private val targetSpringBootProfile: String = taskDescription.target.springBootProfile.get()
    private val targetBranchProvider: TargetBranchProvider = defineTargetBranchProvider()

    private val targetCredentialsProvider: CredentialsProvider = defineTargetCredentialsProvider()
    private val targetCredentialsProviderWrapper = TargetCredentialsProviderWrapper(
        targetCredentialsProvider,
        sourceProjectBranchMode,
        targetSpringBootProfile,
        taskDescription
    )
    private val targetApplicationContext: ApplicationContext = springBootUtils.launchApplication(targetSpringBootProfile)
    private val targetProjectSourcesCreator = TargetProjectSourcesCreator(gradleProject, taskDescription)

    private val sourceProjectPathExtractor = SourceProjectPathExtractor()

    private val sourceHubStrategy = SourceHubStrategy(extension)
    private val sourceLabStrategy = SourceLabStrategy()
    private val sourceHostingStrategySelector = SourceHostingStrategySelector(sourceHubStrategy, sourceLabStrategy)

    private val hubToLabGroupsCopier = HubToLabGroupsCopier()
    private val labToLabGroupsCopier = LabToLabGroupsCopier(sourceProjectPathExtractor)
    private val toLabGroupsCopierSelector = ToLabGroupsCopierSelector(hubToLabGroupsCopier, labToLabGroupsCopier)

    private val targetHubStrategy = TargetHubStrategy(taskDescription)
    private val targetLabStrategy = TargetLabStrategy(taskDescription, toLabGroupsCopierSelector)
    private val targetHostingStrategySelector = TargetHostingStrategySelector(taskDescription, targetHubStrategy, targetLabStrategy)
    private val targetProjectPusher = TargetProjectPusher(taskDescription)

    private val projectPropertyWriteStrategy = ProjectPropertyWriteStrategy(
        taskDescription,
        locationUtils,
        targetSpringBootProfile,
        targetApplicationContext
    )
    private val branchPropertyWriteStrategy = BranchPropertyWriteStrategy(
        taskDescription,
        projectPropertyWriteStrategy,
        locationUtils,
        sourceProjectBranchMode,
        targetSpringBootProfile,
        targetApplicationContext
    )
    private val credentialsPropertyWriteStrategy = CredentialsPropertyWriteStrategy(
        logger,
        taskDescription,
        locationUtils,
        targetSpringBootProfile,
        targetApplicationContext,
        targetCredentialsProvider
    )

    private val propertyBlocksSerializer = PropertyBlocksSerializer()
    private val environmentPropertyBlocksWriter = EnvironmentPropertyBlocksWriter(propertyBlocksSerializer)
    private val filePropertyBlocksWriter = FilePropertyBlocksWriter(propertyBlocksSerializer)
    private val propertyBlocksWriter = PropertyBlocksWriter(environmentPropertyBlocksWriter, filePropertyBlocksWriter)
    private val propertyWriter = PropertyWriter(projectPropertyWriteStrategy, branchPropertyWriteStrategy, credentialsPropertyWriteStrategy, propertyBlocksWriter)

    private val hostingPlaceCreator = HostingPlaceCreator(
        taskDescription,
        sourceProjectPathExtractor,
        sourceProjectBranchMode,
        sourceHostingStrategySelector,
        targetCredentialsProviderWrapper,
        targetBranchProvider,
        targetHostingStrategySelector
    )

    init {
        injectDependenciesIntoExternallyCreatedObjects()
    }

    private fun injectDependenciesIntoExternallyCreatedObjects() {
        sequenceOf(
            task,
            sourceCredentialsProvider,
            targetCredentialsProvider,
            targetBranchProvider
        ).filterNotNull().forEach { o: Any ->
            injectDependenciesIntoExternallyCreatedObject(o)
        }
    }

    private fun injectDependenciesIntoExternallyCreatedObject(o: Any) {
        when (o) {
            //@formatter:off
            is InitPersonalDynamicProjectsTask                   -> o.init(
                                                                        springBootUtils,
                                                                        sourceSpringBootProfile,
                                                                        sourceCredentialsProviderWrapper,
                                                                        hostingPlaceCreator,
                                                                        targetProjectSourcesCreator,
                                                                        targetProjectPusher,
                                                                        propertyWriter
                                                                    )

            is ApplicationPropertiesTargetBranchProvider         -> o.init(springBootUtils, targetSpringBootProfile)
            is GitUserNameTargetBranchProvider                   -> o.init(appProjectDirectory)
            is GitUserEmailTargetBranchProvider                  -> o.init(appProjectDirectory)
            is GradleProjectPropertiesTargetBranchProvider       -> o.init(gradleProject, gradleProjectPropertyPrefixesProvider, targetSpringBootProfile)
            is MasterTargetBranchProvider                        -> o.init()
            is SimpleTargetBranchProvider                        -> o.init()
            is SimpleGradleProjectPropertiesTargetBranchProvider -> o.init(gradleProject)
            is SourceTargetBranchProvider                        -> o.init()

            is ApplicationPropertiesCredentialsProvider          -> o.init(springBootUtils)
            is GitLabContainerCredentialsProvider                -> o.init(gradleProject)
            is WorkspaceCredentialsProvider                      -> o.init(gradleProject)
            is GradleProjectPropertiesCredentialsProvider        -> o.init(gradleProject, gradleProjectPropertyPrefixesProvider)
            is SimpleCredentialsProvider                         -> o.init()
            is SimpleGradleProjectPropertiesCredentialsProvider  -> o.init(gradleProject)
            //@formatter:on

            else -> error("Unexpected class ${o::class}")
        }
    }

    private fun defineTargetCredentialsProvider(): CredentialsProvider {
        val nullableTargetCredentialsProvider: CredentialsProvider? = taskDescription.target.hosting.credentialsProvider.orNull
        if (nullableTargetCredentialsProvider == null) {
            if (sourceProjectBranchMode) {
                if (sourceCredentialsProvider == null) {
                    error(
                        "Neither source nor target credentials provider is specified. " +
                                "taskDescription.target.sourceProjectBranch == true, " +
                                "so the target credentials provider, being not specified, might inherit from the source one, " +
                                "but the source one isn't specified as well."
                    )
                } else {
                    return sourceCredentialsProvider
                }
            } else {
                error("Target credentials provider isn't specified")
            }
        } else {
            return nullableTargetCredentialsProvider
        }
    }

    private fun defineTargetBranchProvider(): TargetBranchProvider {
        val extensionTargetBranchProvider: TargetBranchProvider? = taskDescription.target.branchProvider.orNull
        return if (extensionTargetBranchProvider == null) {
            if (sourceProjectBranchMode) {
                error("target.branchProvider must be specified")
            } else {
                SourceTargetBranchProvider()
            }
        } else {
            if (sourceProjectBranchMode && extensionTargetBranchProvider is SourceTargetBranchProvider) {
                error("SourceTargetBranchProvider should not be used when target.sourceProjectBranch is set to true")
            } else {
                extensionTargetBranchProvider
            }
        }
    }
}
