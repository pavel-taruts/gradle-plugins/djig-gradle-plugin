package org.taruts.djigGradlePlugin.push

import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.gitUtils.GitUtils
import org.taruts.gitUtils.hosting.credentials.CredentialsUtils
import org.taruts.processUtils.ProcessRunner
import java.io.File

/**
 * See [pushToTargetHosting].
 */
class TargetProjectPusher(private val taskDescription: TaskDescription) {

    /**
     * Attaches the source code of the target dynamic project to the remote from [targetProjectProperties] and pushes there.
     *
     * [localSourceDirectory] must have the desired source code.
     *
     * Should conflicts pop up, then the force push is used if allowed by [TaskDescription.Target.allowForcePush].
     */
    fun pushToTargetHosting(localSourceDirectory: File, targetProjectProperties: DjigConfigurationProperties.DynamicProject) {
        val projectUrl = targetProjectProperties.url

        val remoteUrlWithoutCredentials = projectUrl.toString()

        // Setting the Git repo URL as remote for the local Git repo. Fetching the remote
        val (username, password) = CredentialsUtils.apiCredentialsToGitUtilityCredentials(targetProjectProperties.username, targetProjectProperties.password)
        val remoteUrlWithCredentials = GitUtils.addCredentialsToGitRepositoryUrl(remoteUrlWithoutCredentials, username, password)
        ProcessRunner.runProcess(localSourceDirectory, "git", "remote", "add", "-f", "origin", remoteUrlWithCredentials)

        // Getting the target branch
        val targetBranchName: String = targetProjectProperties.branch

        // Creating the target branch
        ProcessRunner.runProcess(localSourceDirectory, "git", "checkout", "-b", targetBranchName)

        // Pushing
        val needForcePush: Boolean = conflictsWithRemote(localSourceDirectory, targetBranchName)
        if (needForcePush) {
            if (taskDescription.target.allowForcePush.get()) {
                ProcessRunner.runProcess(localSourceDirectory, "git", "push", "--force", "--set-upstream", "origin", targetBranchName)
            } else {
                error(
                    "There are conflicts with origin/$targetBranchName " +
                            "and force push is not allowed via DjigPluginExtension.taskDescriptions[...].target.allowForcePush"
                )
            }
        } else {
            ProcessRunner.runProcess(localSourceDirectory, "git", "push", "--set-upstream", "origin", targetBranchName)
        }

        // Removing credentials from the .git/config file
        ProcessRunner.runProcess(localSourceDirectory, "git", "remote", "set-url", "origin", remoteUrlWithoutCredentials)
    }

    private fun conflictsWithRemote(localSourceDirectory: File, targetBranchName: String): Boolean {

        val remoteBranchExists = ProcessRunner.isSuccess {
            ProcessRunner.runProcess(localSourceDirectory, "git", "show-ref", "--hash", "refs/remotes/origin/$targetBranchName")
        }

        val conflictsWithRemote = if (remoteBranchExists) {
            val remoteOnlyCommitsStr = ProcessRunner.runProcess(localSourceDirectory, "git", "rev-list", "^HEAD", "origin/$targetBranchName")
            val remoteOnlyCommitsExist = !remoteOnlyCommitsStr.isNullOrBlank()

            val localOnlyCommitsStr = ProcessRunner.runProcess(localSourceDirectory, "git", "rev-list", "^origin/$targetBranchName", "HEAD")
            val localOnlyCommitsExist = !localOnlyCommitsStr.isNullOrBlank()

            remoteOnlyCommitsExist && localOnlyCommitsExist
        } else {
            false
        }
        return conflictsWithRemote
    }
}
