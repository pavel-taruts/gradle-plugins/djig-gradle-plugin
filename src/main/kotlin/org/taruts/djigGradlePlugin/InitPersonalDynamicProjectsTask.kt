package org.taruts.djigGradlePlugin

import org.gradle.api.DefaultTask
import org.gradle.api.tasks.Input
import org.gradle.api.tasks.TaskAction
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djig.configurationProperties.DjigConfigurationProperties.DynamicProject
import org.taruts.djigGradlePlugin.boot.SpringBootUtils
import org.taruts.djigGradlePlugin.cache.Caches
import org.taruts.djigGradlePlugin.context.TaskContext
import org.taruts.djigGradlePlugin.credentials.Credentials
import org.taruts.djigGradlePlugin.credentials.SourceCredentialsProviderWrapper
import org.taruts.djigGradlePlugin.extension.DjigPluginExtension
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.djigGradlePlugin.hostingPlace.HostingPlaceCreator
import org.taruts.djigGradlePlugin.propertyWriting.PropertyWriter
import org.taruts.djigGradlePlugin.push.TargetProjectPusher
import org.taruts.djigGradlePlugin.sourceDirectory.TargetProjectSourcesCreator
import java.io.File
import javax.inject.Inject

/**
 * A task of this type has a name like init**TaskNameSuffix**PersonalDynamicProjects,
 * where **TaskNameSuffix** is the CamelCase of the corresponding [TaskDescription.taskShortName]
 * or [TaskDescription.Target.springBootProfile] if [TaskDescription.taskShortName] is not set.
 *
 * The task reads source dynamic projects from the configuration properties
 * of the source Spring Boot profile ([DjigPluginExtension.SourceDynamicProjects.springBootProfile])
 * of the djig application
 * and copies the dynamic projects to the target hosting, applying some transformations in the process (see [TaskDescription.Transform]).
 *
 * The properties of the target dynamic projects are saved to the target Spring Boot profile ([TaskDescription.Target.springBootProfile]).
 *
 * @see DjigPluginExtension
 * @see TaskDescription
 * @see DjigPlugin
 */
open class InitPersonalDynamicProjectsTask
@Inject
constructor(
    @Input val extension: DjigPluginExtension,
    @Input val taskDescription: TaskDescription
) : DefaultTask() {

    //@formatter:off
    private lateinit var springBootUtils: SpringBootUtils
    private lateinit var sourceSpringBootProfile: String
    private lateinit var sourceCredentialsProviderWrapper: SourceCredentialsProviderWrapper
    private lateinit var hostingPlaceCreator: HostingPlaceCreator
    private lateinit var targetProjectSourcesCreator: TargetProjectSourcesCreator
    private lateinit var targetProjectPusher: TargetProjectPusher
    private lateinit var propertyWriter: PropertyWriter
    //@formatter:on

    fun init(
        springBootUtils: SpringBootUtils,
        sourceSpringBootProfile: String,
        sourceCredentialsProviderWrapper: SourceCredentialsProviderWrapper,
        hostingPlaceCreator: HostingPlaceCreator,
        targetProjectSourcesCreator: TargetProjectSourcesCreator,
        targetProjectPusher: TargetProjectPusher,
        propertyWriter: PropertyWriter
    ) {
        this.springBootUtils = springBootUtils
        this.sourceSpringBootProfile = sourceSpringBootProfile
        this.sourceCredentialsProviderWrapper = sourceCredentialsProviderWrapper
        this.hostingPlaceCreator = hostingPlaceCreator
        this.targetProjectSourcesCreator = targetProjectSourcesCreator
        this.targetProjectPusher = targetProjectPusher
        this.propertyWriter = propertyWriter
    }

    private lateinit var sourceDynamicProjects: Map<String, DynamicProject>

    init {
        group = DjigPlugin.TASK_GROUP

        val sourceSpringBootProfile: String = extension.sourceDynamicProjects.springBootProfile.get()

        val projectNameToSourcePathFunction = taskDescription.transform.djigProjectNameToSourcePathFunction.get()
        val exampleProjectRelativePath = projectNameToSourcePathFunction("<original project name>")

        description = """
        Forks dynamic projects from application-$sourceSpringBootProfile.properties. 
        Each fork goes in the project subdirectory $exampleProjectRelativePath.
        The forks are also pushed to the chosen Git repository hosting.
        """.trimIndent()
    }

    @TaskAction
    fun action() {
        try {
            Caches.clear()
            TaskContext(this)
            validateDjigProjectNameToSourcePathFunction()
            sourceDynamicProjects = buildSourceDynamicProjects()
            val targetDynamicProjectsMap: Map<String, DynamicProject> = forkProjects()
            propertyWriter.writeTargetProperties(targetDynamicProjectsMap)
        } finally {
            Caches.clear()
        }
    }

    private fun validateDjigProjectNameToSourcePathFunction() {
        val djigProjectNameToSourcePathFunction = taskDescription.transform.djigProjectNameToSourcePathFunction.get()
        val testSourcePath1: String = djigProjectNameToSourcePathFunction("1")
        val testSourcePath2: String = djigProjectNameToSourcePathFunction("2")
        if (testSourcePath1 == testSourcePath2) {
            error(
                "TaskDescription.transform.projectNameToSourcePathFunction returns $testSourcePath1 regardless of what the project name is. " +
                        "The returned value must include the project name"
            )
        }
    }

    private fun buildSourceDynamicProjects(): Map<String, DynamicProject> {
        val djigConfigurationProperties: DjigConfigurationProperties = springBootUtils.getDjigConfigurationPropertiesForTask(sourceSpringBootProfile)
        val dynamicProjects: MutableMap<String, DynamicProject> = djigConfigurationProperties.dynamicProjects

        dynamicProjects.forEach { (projectName, sourceProjectProperties) ->
            val sourceHostingCredentials: Credentials? = sourceCredentialsProviderWrapper.getSourceHostingCredentials(projectName, sourceProjectProperties)
            if (sourceHostingCredentials != null) {
                sourceProjectProperties.username = sourceHostingCredentials.username
                sourceProjectProperties.password = sourceHostingCredentials.password
            }
        }

        return dynamicProjects
    }

    private fun forkProjects(): Map<String, DynamicProject> {

        val targetDynamicProjects: MutableMap<String, DynamicProject> = HashMap()

        sourceDynamicProjects.entries.forEach { (projectName, sourceProjectProperties) ->
            val targetProjectProperties: DynamicProject = forkProject(projectName, sourceProjectProperties)
            targetDynamicProjects[projectName] = targetProjectProperties
        }

        return targetDynamicProjects
    }

    private fun forkProject(projectName: String, sourceProjectProperties: DynamicProject): DynamicProject {
        val targetProjectProperties: DynamicProject = hostingPlaceCreator.createHostingPlace(projectName, sourceProjectProperties)
        val dynamicLocalSourceDir: File = targetProjectSourcesCreator.createTargetProjectSources(projectName, sourceProjectProperties, targetProjectProperties)
        targetProjectPusher.pushToTargetHosting(dynamicLocalSourceDir, targetProjectProperties)
        return targetProjectProperties
    }
}
