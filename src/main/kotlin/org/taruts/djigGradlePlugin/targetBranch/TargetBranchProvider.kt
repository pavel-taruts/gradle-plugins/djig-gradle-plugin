package org.taruts.djigGradlePlugin.targetBranch

import org.taruts.djig.configurationProperties.DjigConfigurationProperties

/**
 * The base interface to be implemented by all strategies of providing the branch name for target dynamic projects.
 *
 * See the implementations.
 */
interface TargetBranchProvider {

    /**
     * Provides the Git branch to sit in for a target dynamic project named [dynamicProjectName]
     * with properties [targetProjectProperties],
     * which is being copied from a source project also named [dynamicProjectName]
     * with properties [sourceProjectProperties].
     */
    fun getTargetBranch(
        dynamicProjectName: String,
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        targetProjectProperties: DjigConfigurationProperties.DynamicProject
    ): String
}
