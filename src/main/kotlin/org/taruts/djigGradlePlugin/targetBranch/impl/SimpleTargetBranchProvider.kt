package org.taruts.djigGradlePlugin.targetBranch.impl

import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djigGradlePlugin.targetBranch.TargetBranchProvider

/**
 * A [TargetBranchProvider] always returning [branch] as the target branch name for all target projects.
 */
class SimpleTargetBranchProvider(private val branch: String) : TargetBranchProvider {
    fun init() {
    }

    override fun getTargetBranch(
        dynamicProjectName: String,
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        targetProjectProperties: DjigConfigurationProperties.DynamicProject
    ): String = branch
}
