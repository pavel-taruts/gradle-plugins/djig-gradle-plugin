package org.taruts.djigGradlePlugin.targetBranch.impl

import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djig.configurationProperties.utils.DynamicProjectPropertiesDefaultsApplier
import org.taruts.djigGradlePlugin.boot.SpringBootUtils
import org.taruts.djigGradlePlugin.targetBranch.TargetBranchProvider

/**
 * A [TargetBranchProvider] implementation which gets the branch name
 * from the Spring Boot target profile properties for the dynamic project.
 */
class ApplicationPropertiesTargetBranchProvider : TargetBranchProvider {

    private lateinit var springBootUtils: SpringBootUtils
    private lateinit var targetSpringBootProfile: String

    fun init(springBootUtils: SpringBootUtils, targetSpringBootProfile: String) {
        this.springBootUtils = springBootUtils
        this.targetSpringBootProfile = targetSpringBootProfile
    }

    override fun getTargetBranch(
        dynamicProjectName: String,
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        targetProjectProperties: DjigConfigurationProperties.DynamicProject
    ): String {

        val djigConfigurationProperties: DjigConfigurationProperties = springBootUtils.getDjigConfigurationPropertiesForTask(targetSpringBootProfile)

        val dynamicProjectProperties: DjigConfigurationProperties.DynamicProject =
            DynamicProjectPropertiesDefaultsApplier.applyDefaults(djigConfigurationProperties, dynamicProjectName)

        return dynamicProjectProperties.branch
    }
}
