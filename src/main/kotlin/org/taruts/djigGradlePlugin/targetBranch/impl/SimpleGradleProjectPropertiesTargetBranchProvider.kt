package org.taruts.djigGradlePlugin.targetBranch.impl

import org.apache.commons.lang3.StringUtils
import org.gradle.api.Project
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djigGradlePlugin.targetBranch.TargetBranchProvider

/**
 * A [TargetBranchProvider] getting the target branch name from Gradle project property [propertyName] for all target projects.
 */
class SimpleGradleProjectPropertiesTargetBranchProvider(private val propertyName: String) : TargetBranchProvider {

    private lateinit var gradleProject: Project

    fun init(gradleProject: Project) {
        this.gradleProject = gradleProject
    }

    override fun getTargetBranch(
        dynamicProjectName: String,
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        targetProjectProperties: DjigConfigurationProperties.DynamicProject
    ): String {
        return StringUtils.trimToNull(gradleProject.property(propertyName).toString())
    }
}
