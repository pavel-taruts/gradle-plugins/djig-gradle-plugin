package org.taruts.djigGradlePlugin.targetBranch.impl

import org.apache.commons.lang3.StringUtils
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djigGradlePlugin.targetBranch.TargetBranchProvider
import org.taruts.processUtils.ProcessRunner
import java.io.File

/**
 * A [TargetBranchProvider] implementation that returns "user.email" Git setting
 * actual for the djig application Git repository
 * as the target branch name.
 *
 * If there's no "user.email" for the djig application Git repository then the value for the workspace repository is used.
 */
class GitUserEmailTargetBranchProvider : TargetBranchProvider {

    private lateinit var appProjectDirectory: File

    fun init(appProjectDirectory: File) {
        this.appProjectDirectory = appProjectDirectory
    }

    override fun getTargetBranch(
        dynamicProjectName: String,
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        targetProjectProperties: DjigConfigurationProperties.DynamicProject
    ): String {
        val branch1: String = ProcessRunner.runProcess(appProjectDirectory, "git", "config", "user.email")
        val branch2: String = ProcessRunner.runProcess(null, "git", "config", "user.email")
        val branch = StringUtils.firstNonBlank(branch1, branch2)
        if (branch.isBlank()) {
            error("Git user email is not configured")
        }
        return branch
    }
}
