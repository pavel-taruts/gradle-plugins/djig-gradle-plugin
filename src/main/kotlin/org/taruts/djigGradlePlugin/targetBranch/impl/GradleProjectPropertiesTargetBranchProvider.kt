package org.taruts.djigGradlePlugin.targetBranch.impl

import org.apache.commons.lang3.StringUtils
import org.gradle.api.Project
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djigGradlePlugin.prefixes.GradleProjectPropertyPrefixesProvider
import org.taruts.djigGradlePlugin.targetBranch.TargetBranchProvider

/**
 * A [TargetBranchProvider] implementation getting the target branch name from Gradle project properties.
 *
 * There are several property names it can use.
 *
 * All properties are named "&lt;prefix&gt;.branch", where the &lt;prefix&gt; is one of those supported by [GradleProjectPropertyPrefixesProvider].
 *
 * See [GradleProjectPropertyPrefixesProvider] for the list of prefixes and their priorities.
 */
class GradleProjectPropertiesTargetBranchProvider(
    private val default: String = "master",
    /**
     * @see [GradleProjectPropertyPrefixesProvider].
     */
    private val alternativeGroupId: String? = null
) : TargetBranchProvider {

    private lateinit var gradleProject: Project
    private lateinit var gradleProjectPropertyPrefixesProvider: GradleProjectPropertyPrefixesProvider
    private lateinit var targetSpringBootProfile: String

    fun init(gradleProject: Project, gradleProjectPropertyPrefixesProvider: GradleProjectPropertyPrefixesProvider, targetSpringBootProfile: String) {
        this.gradleProject = gradleProject
        this.gradleProjectPropertyPrefixesProvider = gradleProjectPropertyPrefixesProvider
        this.targetSpringBootProfile = targetSpringBootProfile
    }

    override fun getTargetBranch(
        dynamicProjectName: String,
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        targetProjectProperties: DjigConfigurationProperties.DynamicProject
    ): String {
        val prefixes: List<String> = gradleProjectPropertyPrefixesProvider.getPrefixes(
            targetProjectProperties.url,
            targetSpringBootProfile,
            dynamicProjectName,
            alternativeGroupId
        )
        val workspaceGradleProject = gradleProject
        val branch: String? = prefixes.firstNotNullOfOrNull { prefix ->
            val propertyName = "$prefix.branch"

            if (workspaceGradleProject.hasProperty(propertyName))
                StringUtils.trimToNull(workspaceGradleProject.property(propertyName).toString())
            else null
        }
        return branch ?: default
    }
}
