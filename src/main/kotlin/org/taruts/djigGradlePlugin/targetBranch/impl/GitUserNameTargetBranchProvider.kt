package org.taruts.djigGradlePlugin.targetBranch.impl

import org.apache.commons.lang3.StringUtils
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djigGradlePlugin.targetBranch.TargetBranchProvider
import org.taruts.processUtils.ProcessRunner
import java.io.File

/**
 * A [TargetBranchProvider] implementation getting the target branch name
 * from "user.name" Git setting
 * actual for the djig application Git repository.
 *
 * All non latin letter and non digit character sequences are replaced with hyphens if they are in the middle and removed if they are at the beginning or at the end.
 *
 * If there's no "user.name" for the djig application Git repository then the value for the workspace repository is used.
 */
class GitUserNameTargetBranchProvider : TargetBranchProvider {

    private lateinit var appProjectDirectory: File

    fun init(appProjectDirectory: File) {
        this.appProjectDirectory = appProjectDirectory
    }

    override fun getTargetBranch(
        dynamicProjectName: String,
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        targetProjectProperties: DjigConfigurationProperties.DynamicProject
    ): String {
        val branch1: String = ProcessRunner.runProcess(appProjectDirectory, "git", "config", "user.name")
        val branch2: String = ProcessRunner.runProcess(null, "git", "config", "user.name")
        var branch = StringUtils.firstNonBlank(branch1, branch2)
        if (branch.isBlank()) {
            error("Git user name is not configured")
        }
        branch = branch.replace("[^a-zA-Z0-9-]+".toRegex(), "-")
        branch = branch.replace("^-".toRegex(), "")
        branch = branch.replace("-$".toRegex(), "")
        branch = branch.replace("-+".toRegex(), "-")
        branch = branch.lowercase()
        return branch
    }
}
