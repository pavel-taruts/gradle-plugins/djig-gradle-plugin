package org.taruts.djigGradlePlugin.targetBranch.impl

import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djigGradlePlugin.targetBranch.TargetBranchProvider

/**
 * A [TargetBranchProvider] returning the target project branch same as the branch of the corresponding source project.
 *
 * That is, if a source project sits in "master", then the target one will be in "master" too.
 */
class SourceTargetBranchProvider : TargetBranchProvider {
    fun init() {
    }

    override fun getTargetBranch(
        dynamicProjectName: String,
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        targetProjectProperties: DjigConfigurationProperties.DynamicProject
    ): String {
        return sourceProjectProperties.branch
    }
}
