package org.taruts.djigGradlePlugin.extension.tasks

import org.apache.commons.lang3.StringUtils
import org.gradle.api.Action
import org.gradle.api.Named
import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Nested
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djigGradlePlugin.InitPersonalDynamicProjectsTask
import org.taruts.djigGradlePlugin.credentials.CredentialsProvider
import org.taruts.djigGradlePlugin.credentials.impl.GitLabContainerCredentialsProvider
import org.taruts.djigGradlePlugin.credentials.impl.GradleProjectPropertiesCredentialsProvider
import org.taruts.djigGradlePlugin.extension.DjigPluginExtension
import org.taruts.djigGradlePlugin.extension.Visibility
import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.ExpressionParser
import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.PathTransformer
import org.taruts.djigGradlePlugin.propertyWriting.enums.PropertyType
import org.taruts.djigGradlePlugin.propertyWriting.enums.PropertyWritingPlace
import org.taruts.djigGradlePlugin.propertyWriting.utils.LocationUtils
import org.taruts.djigGradlePlugin.targetBranch.TargetBranchProvider
import org.taruts.djigGradlePlugin.targetBranch.impl.*
import org.taruts.djigGradlePlugin.utils.ReflectionUtils
import java.net.URL
import javax.inject.Inject

/**
 * This is used by [DjigPluginExtension] to describe a [InitPersonalDynamicProjectsTask].
 */
abstract class TaskDescription : Named {

    /**
     * The PascalCase of it will be used in the name of the task as the suffix.
     *
     * The task name with then be like init**TaskShortName**PersonalDynamicProjects.
     *
     * [taskShortName] doesn't have to use the PascalCase itself.
     */
    abstract val taskShortName: Property<String>

    /**
     * @see Target
     */
    @get:Nested
    abstract val target: Target

    /**
     * @see Transform
     */
    @get:Nested
    abstract val transform: Transform

    /**
     * @see Target
     */
    fun target(action: Action<Target>) {
        action.execute(target)
    }

    /**
     * @see Transform
     */
    fun transform(action: Action<Transform>) {
        action.execute(transform)
    }

    /**
     * Groups properties describing target projects,
     * the place and other details of their creation
     * and how they are to be written to Spring Boot configuration properties.
     */
    abstract class Target @Inject constructor(
        private val project: Project,
    ) {
        /**
         * @see HostingDescription
         */
        @get:Nested
        abstract val hosting: HostingDescription

        /**
         * The Spring Boot profile of the djig application where the properties of the dynamic projects created by the task will be written.
         */
        abstract val springBootProfile: Property<String>

        /**
         * @see GitHub
         */
        @get:Nested
        abstract val gitHub: GitHub

        /**
         * The visibility of the target projects.
         */
        abstract val visibility: Property<Visibility>

        /**
         * The preferred file extension for Spring Boot configuration property files of the target profile.
         *
         * When searching for existing configuration property files in a directory location (e.g. classpath:/config/ or file:./config/),
         * those with this extension will be searched for first.
         *
         * Also, when no files are found to write the target properties to, the newly created file will have this extension.
         */
        abstract val preferredFileExtension: Property<String>

        /**
         * The settings of writing normal project properties into target profile properties.
         * @see ProjectPropertyWriteStrategyConfiguration
         */
        @get:Nested
        abstract val projectPropertyWriteStrategy: ProjectPropertyWriteStrategyConfiguration

        /**
         * The settings of writing target project credentials into target profile properties.
         * @see PropertyWriteStrategyConfiguration
         */
        @get:Nested
        abstract val credentialsPropertyWriteStrategy: PropertyWriteStrategyConfiguration

        /**
         * The strategy of determining the Git branch name for target projects to sit in.
         * @see TargetBranchProvider
         * @see ApplicationPropertiesTargetBranchProvider
         * @see GitUserEmailTargetBranchProvider
         * @see GitUserNameTargetBranchProvider
         * @see GradleProjectPropertiesTargetBranchProvider
         * @see MasterTargetBranchProvider
         * @see SimpleGradleProjectPropertiesTargetBranchProvider
         * @see SimpleTargetBranchProvider
         * @see SourceTargetBranchProvider
         */
        abstract val branchProvider: Property<TargetBranchProvider>

        /**
         * Enables or disables force pushing while pushing a target dynamic project sources.
         * It's **disabled** by default.
         */
        abstract val allowForcePush: Property<Boolean>

        /**
         * The settings of writing target project branch name into target profile properties.
         * @see PropertyWriteStrategyConfiguration
         */
        @get:Nested
        abstract val branchPropertyWriteStrategy: PropertyWriteStrategyConfiguration

        /**
         * Turns on and off the mode of creating target projects in the same Git repository as the corresponding source project, but in another branch.
         * It's **turned off** by default.
         */
        abstract val sourceProjectBranchMode: Property<Boolean>

        /**
         * @see HostingDescription
         */
        @Suppress("unused")
        fun hosting(action: Action<HostingDescription>) {
            action.execute(hosting)
        }

        /**
         * @see GitHub
         */
        @Suppress("unused")
        fun gitHub(action: Action<GitHub>) {
            action.execute(gitHub)
        }

        /**
         * Configures the settings of writing normal project properties into target profile properties.
         * @see ProjectPropertyWriteStrategyConfiguration
         */
        @Suppress("unused")
        fun projectPropertyWriteStrategy(action: Action<ProjectPropertyWriteStrategyConfiguration>) {
            action.execute(projectPropertyWriteStrategy)
        }

        /**
         * Configures the settings of writing target project credentials into target profile properties.
         * @see PropertyWriteStrategyConfiguration
         */
        @Suppress("unused")
        fun credentialsPropertyWriteStrategy(action: Action<PropertyWriteStrategyConfiguration>) {
            action.execute(credentialsPropertyWriteStrategy)
        }

        /**
         * Configures the settings of writing target project branch name into target profile properties.
         * @see PropertyWriteStrategyConfiguration
         */
        @Suppress("unused")
        fun branchPropertyWriteStrategy(action: Action<PropertyWriteStrategyConfiguration>) {
            action.execute(branchPropertyWriteStrategy)
        }

        init {
            visibility.convention(Visibility.PRIVATE)
            preferredFileExtension.convention("properties")
            allowForcePush.convention(false)
            sourceProjectBranchMode.convention(false)
        }

        /**
         * Configures the target projects to be created on a local GitLab Docker container created by the
         * [org.taruts.gitlab-container](https://plugins.gradle.org/plugin/org.taruts.gitlab-container)
         * Gradle plugin.
         *
         * It also allows for additional configuration via [action].
         *
         * @see Target
         */
        @Suppress("unused")
        fun gitLabContainer(action: Action<Target>) {
            hosting.run {
                gitLabContainer.set(true)
                hostingType.set(HostingType.GitLab)

                // We need to interact with an extension of another plugin.
                // We can retrieve it from the project, but we don't have its class in the ClassLoader of our current plugin.
                // Different Gradle plugins are run using separate class loaders.
                // So we access data of the extension of the other plugin via reflection.
                val gitLabContainerPluginExtension: Any = project.extensions.getByName("gitLabContainer")
                val urlProperty: Property<URL> = ReflectionUtils.getObjectPropertyValue(gitLabContainerPluginExtension, "url")
                url.set(urlProperty.get())

                credentialsProvider.set(GitLabContainerCredentialsProvider())
            }
            visibility.set(Visibility.PUBLIC)
            action.execute(this)
        }

        /**
         * Configures the target projects to be created on [gitlab.com](https://gitlab.com).
         *
         * Allows for additional configuration via [action].
         *
         * By default, the target projects will be private and the credentials will be determined by means of [GradleProjectPropertiesCredentialsProvider].
         */
        @Suppress("unused")
        fun publicGitLab(action: Action<Target>) {
            hosting.run {
                hostingType.set(HostingType.GitLab)
                url.set(URL("https://gitlab.com"))
                credentialsProvider.set(GradleProjectPropertiesCredentialsProvider())
            }
            visibility.set(Visibility.PRIVATE)
            action.execute(this)
        }

        /**
         * Configures the target projects to be created on [github.com](https://github.com).
         *
         * Allows for additional configuration via [action].
         *
         * By default, the target projects will be private and the credentials will be determined by means of [GradleProjectPropertiesCredentialsProvider].
         */
        @Suppress("unused")
        fun publicGitHub(action: Action<Target>) {
            hosting.run {
                hostingType.set(HostingType.GitHub)
                url.set(URL("https://github.com"))
                credentialsProvider.set(GradleProjectPropertiesCredentialsProvider())
            }
            visibility.set(Visibility.PRIVATE)
            action.execute(this)
        }

        /**
         * Configures the target projects to be created in the same Git repository as the corresponding source project, but in another branch.
         *
         * The branch name is determined by means of [targetBranchProvider].
         *
         * Allows for additional configuration via [action].
         *
         * By default, the credentials will be those used to access the source project.
         */
        @Suppress("unused")
        fun sourceProjectBranch(targetBranchProvider: TargetBranchProvider, action: Action<Target>) {
            sourceProjectBranchMode.set(true)
            this.branchProvider.set(targetBranchProvider)
            action.execute(this)
        }

        /**
         * Describes the target hosting, where the target projects are to be created, and how to work with it.
         */
        abstract class HostingDescription {

            /**
             * The software brand that the target hosting uses.
             * @see HostingType
             */
            abstract val hostingType: Property<HostingType>

            /**
             * The URL at which the target hosting is accessible via its REST API (**and never via the Git utility API**).
             */
            abstract val url: Property<URL>

            /**
             * The strategy of getting credentials for both the hosting API and the Git utility API.
             */
            abstract val credentialsProvider: Property<CredentialsProvider>

            /**
             * @see GitLab
             */
            @get:Nested
            abstract val gitLab: GitLab

            /**
             * Indicates whether the hosting is local GitLab sitting in a Docker container, created by the
             * [org.taruts.gitlab-container](https://plugins.gradle.org/plugin/org.taruts.gitlab-container)
             * Gradle plugin.
             *
             * Typically, this set by [TaskDescription.Target.gitLabContainer] and should not be configured manually.
             */
            abstract val gitLabContainer: Property<Boolean>

            /**
             * @see GitLab
             */
            @Suppress("unused")
            fun gitLab(action: Action<GitLab>) {
                action.execute(gitLab)
            }

            /**
             * Groups those target hosting properties that are only relevant if its GitLab.
             */
            abstract class GitLab {

                /**
                 * Configures attempts at deleting an existing target project via the GitLab API before creating a new one on top of it.
                 * @see Attempts
                 */
                @get:Nested
                abstract val deleteProject: Attempts

                /**
                 * Configures attempts at creating a target project via the GitLab API.
                 * @see Attempts
                 */
                @get:Nested
                abstract val createProject: Attempts

                /**
                 * Configures attempts at deleting an existing target project via the GitLab API before creating a new one on top of it.
                 * @see Attempts
                 */
                @Suppress("unused")
                fun deleteProject(action: Action<Attempts>) {
                    action.execute(deleteProject)
                }

                /**
                 * Configures attempts at creating a target project via the GitLab API.
                 * @see Attempts
                 */
                @Suppress("unused")
                fun createProject(action: Action<Attempts>) {
                    action.execute(createProject)
                }

                /**
                 * This is used to configure attempts for actions with target projects via the GitLab API, like creation or deletion of a project.
                 */
                abstract class Attempts {
                    /**
                     * The maximum of attempts of performing the API action.
                     *
                     * The default is 300.
                     */
                    abstract val maxAttempts: Property<Int>

                    /**
                     * The delay in milliseconds between attempts of performing the API action.
                     *
                     * The default is 5000 ms.
                     */
                    abstract val delay: Property<Long>
                }

                init {
                    deleteProject.maxAttempts.convention(300)
                    deleteProject.delay.convention(5000)

                    createProject.maxAttempts.convention(300)
                    createProject.delay.convention(5000)
                }
            }

            init {
                gitLabContainer.convention(false)
            }
        }

        /**
         * This is used to configure a strategy of writing a group of variables
         * to a place where the djig application using the target profile could use them as
         * Spring Boot configuration properties [DjigConfigurationProperties].
         *
         * As of now, there are two such groups of variables: credentials and branch name (which is a group of just one variable).
         */
        abstract class PropertyWriteStrategyConfiguration {

            /**
             * The property type that must be used to write the variables to.
             * @see PropertyType
             */
            abstract val propertyType: Property<PropertyType>

            /**
             * The kind of the place in general to write the variables to.
             * @see PropertyWritingPlace
             */
            abstract val place: Property<PropertyWritingPlace>

            /**
             * The return value of this lambda is used to affect the choice of the file to write the variables to.
             *
             * It only makes sense if [place] is [PropertyWritingPlace.APP_PROPERTIES_RESOURCE].
             *
             * See [LocationUtils.getPropertyFile] to learn which return values of [location] there might be and how they are interpreted.
             */
            abstract val location: Property<(configName: String, profile: String) -> String>
        }

        /**
         * This is used to configure a strategy of writing of normal project properties
         * to a place where the djig application using the target profile could use them as
         * Spring Boot configuration properties [DjigConfigurationProperties].
         */
        abstract class ProjectPropertyWriteStrategyConfiguration {

            /**
             * The return value of this lambda is used to affect the choice of the file to write the variables to.
             *
             * See [LocationUtils.getPropertyFile] to learn which return values of [location] there might be and how they are interpreted.
             */
            abstract val location: Property<(configName: String, profile: String) -> String>
        }

        /**
         * Groups those properties of the target hosting that are applicable only if the hosting is [github.com](https://github.com).
         */
        abstract class GitHub {
            abstract val groupsSeparator: Property<String>

            init {
                groupsSeparator.convention("--")
            }
        }
    }

    /**
     * This describes the transformation of names and paths between source and target projects.
     *
     * It's both for paths the projects have on Git repository hostings and on the file system.
     */
    abstract class Transform {
        /**
         * The expression of transforming a source project namespace to the corresponding target project namespace.
         *
         * The default is "**" which means no transformation.
         *
         * @see PathTransformer
         * @see ExpressionParser
         */
        abstract val transformNamespaceExpression: Property<String>

        /**
         * The lambda to transform the name of a source project it has on its **hosting** to the name the corresponding target project will have on its **hosting**.
         *
         * The default lambda returns the target name to be the same as the source one.
         *
         * **Warning**. The project names that we operate with here are those the projects have on their **Git repository hostings**.
         * They're not the names the projects have in [DjigConfigurationProperties].
         */
        abstract val hostingProjectNameTransformer: Property<(sourceProjectHostingName: String) -> String>

        /**
         * The lambda to get the relative directory path for the sources of target projects.
         *
         * The path is relative to **workspace-directory**&#47;projects.
         *
         * The argument is the name of both the source and the target projects in [DjigConfigurationProperties].
         *
         * By default, the sources of a target dynamic project will go to **workspace-directory**&#47;projects&#47;**djig-project-name**.
         *
         * **Warning**. The project name we operate with here is something different from the names projects have on their Git repository hostings.
         */
        abstract val djigProjectNameToSourcePathFunction: Property<(djigProjectName: String) -> String>

        init {
            transformNamespaceExpression.convention("**")
            hostingProjectNameTransformer.convention { it }
            djigProjectNameToSourcePathFunction.convention { it }
        }
    }

    override fun getName(): String {
        return StringUtils.firstNonBlank(taskShortName.orNull, target.springBootProfile.get())
    }
}
