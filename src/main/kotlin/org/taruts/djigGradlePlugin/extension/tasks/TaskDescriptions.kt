package org.taruts.djigGradlePlugin.extension.tasks

import org.gradle.api.Action
import org.gradle.api.internal.CollectionCallbackActionDecorator
import org.gradle.api.internal.DefaultNamedDomainObjectSet
import org.gradle.api.model.ObjectFactory
import org.gradle.internal.reflect.Instantiator
import org.taruts.djigGradlePlugin.InitPersonalDynamicProjectsTask
import org.taruts.djigGradlePlugin.extension.DjigPluginExtension
import javax.inject.Inject

/**
 * The container for [TaskDescription] objects of [DjigPluginExtension].
 */
open class TaskDescriptions
@Inject
constructor(
    private val objectFactory: ObjectFactory,
    instantiator: Instantiator,
    callbackActionDecorator: CollectionCallbackActionDecorator
) : DefaultNamedDomainObjectSet<TaskDescription>(
    TaskDescription::class.java,
    instantiator,
    callbackActionDecorator
) {
    /**
     * Adds a [InitPersonalDynamicProjectsTask] to the project.
     * The task would create a set of target dynamic projects
     * and put their properties in Spring Boot configuration properties
     * of the [targetSpringBootProfile] profile of the djig application.
     *
     * The details of how the task should work are described via configuration of a new [TaskDescription] object by [action].
     *
     * The name of the task will be init**TaskNameSuffix**PersonalDynamicProjects, where **TaskNameSuffix**
     * is the PascalCase of [TaskDescription.taskShortName] (if it is set by [action])
     * or the PascalCase of [targetSpringBootProfile] if [TaskDescription.taskShortName] is null.
     */
    fun register(targetSpringBootProfile: String, action: Action<TaskDescription>) {
        val taskDescription = objectFactory.newInstance(TaskDescription::class.java)
        action.execute(taskDescription)
        taskDescription.target.springBootProfile.set(targetSpringBootProfile)
        add(taskDescription)
    }
}
