package org.taruts.djigGradlePlugin.extension

/**
 * Visibility of a target dynamic project.
 */
enum class Visibility {
    PUBLIC,
    PRIVATE,
}
