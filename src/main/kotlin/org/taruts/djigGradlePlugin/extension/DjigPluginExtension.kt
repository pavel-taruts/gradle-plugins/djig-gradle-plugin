package org.taruts.djigGradlePlugin.extension

import org.gradle.api.Action
import org.gradle.api.Project
import org.gradle.api.model.ObjectFactory
import org.gradle.api.provider.MapProperty
import org.gradle.api.provider.Property
import org.gradle.api.tasks.Nested
import org.taruts.djigGradlePlugin.DjigPlugin
import org.taruts.djigGradlePlugin.credentials.CredentialsProvider
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescriptions
import java.io.File
import javax.inject.Inject

/**
 * The extension that [DjigPlugin] adds to the Gradle [Project] of the djig [workspace](https://plugins.gradle.org/plugin/org.taruts.workspace).
 */
abstract class DjigPluginExtension {

    /**
     * @see App
     */
    @get:Nested
    abstract val app: App

    /**
     * @see SourceDynamicProjects
     */
    @get:Nested
    abstract val sourceDynamicProjects: SourceDynamicProjects

    /**
     * @see TaskDescriptions
     */
    @get:Nested
    abstract val taskDescriptions: TaskDescriptions

    /**
     * @see App
     */
    fun app(action: Action<App>) {
        action.execute(this.app)
    }

    /**
     * @see SourceDynamicProjects
     */
    fun sourceDynamicProjects(
        action: Action<SourceDynamicProjects>
    ) {
        action.execute(this.sourceDynamicProjects)
    }

    /**
     * The shortcut for [TaskDescriptions.register].
     */
    fun task(targetSpringBootProfile: String, action: Action<TaskDescription>) {
        taskDescriptions.register(targetSpringBootProfile, action)
    }

    /**
     * This describes the djig application: where it is in the workspace and how it's supposed to be launched.
     */
    abstract class App @Inject constructor(objectFactory: ObjectFactory) {
        /**
         * The path of the djig application sources relative to the workspace project root.
         */
        abstract val sourcesRelativePath: Property<String>

        /**
         * The working directory with which the djig application is run.
         * We need it for our Gradle plugin to work with the very Spring Boot configuration property files that the app uses.
         * It's used to read external configuration of the source Spring Boot profile of the djig application.
         * Also, it's used to read and write to external configuration of the target profile.
         */
        abstract val workingDirectory: Property<File>

        /**
         * The additional Spring Properties with which the djig application is run.
         * We need it for our Gradle plugin to work with the very Spring Boot configuration property files that the app uses.
         *
         * It's empty by default.
         */
        val springBootProperties: MapProperty<String, String> = objectFactory.mapProperty(String::class.java, String::class.java)

        init {
            springBootProperties.convention(mapOf())
        }
    }

    /**
     * This describes how to get the source dynamic projects of the djig application.
     */
    abstract class SourceDynamicProjects {

        /**
         * The source Spring Boot profile we get the source dynamic projects from.
         */
        abstract val springBootProfile: Property<String>

        /**
         * @see GitHub
         */
        @get:Nested
        abstract val gitHub: GitHub

        /**
         * The strategy of getting credentials for accessing the source dynamic projects.
         */
        abstract val credentialsProvider: Property<CredentialsProvider>

        /**
         * @see GitHub
         */
        fun gitHub(action: Action<GitHub>) {
            action.execute(this.gitHub)
        }

        /**
         * Specifics of working with source projects stored at GitHub.
         */
        abstract class GitHub {
            /**
             * GitHub doesn't have a thing like groups in its domain model.
             * But if we want we can still interpret the name of a GitHub project as a path,
             * containing a succession of groups in the beginning and the project short name at the end.
             * This might be useful in particular if we copy projects from GitHub to GitLab,
             * and we want to create target projects in GitLab groups deduced from the corresponding GitHub source project name.
             *
             * The default is ".".
             */
            abstract val groupsSeparator: Property<String>

            init {
                groupsSeparator.convention(".")
            }
        }
    }
}
