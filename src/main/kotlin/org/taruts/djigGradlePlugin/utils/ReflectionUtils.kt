package org.taruts.djigGradlePlugin.utils

import kotlin.reflect.KClass
import kotlin.reflect.KProperty1
import kotlin.reflect.full.memberProperties

object ReflectionUtils {

    /**
     * Get the property [propertyName] of the object [obj] using reflection.
     *
     * This is for cases when we don't have the class of [obj] in the current class loader.
     */
    fun <T> getObjectPropertyValue(obj: Any, propertyName: String): T {
        val kClass: KClass<Any> = obj.javaClass.kotlin
        val kProperty: KProperty1<Any, *> = kClass.memberProperties.find { it.name == propertyName }!!

        @Suppress("UNCHECKED_CAST")
        val propertyValue: T = kProperty.get(obj) as T

        return propertyValue
    }
}
