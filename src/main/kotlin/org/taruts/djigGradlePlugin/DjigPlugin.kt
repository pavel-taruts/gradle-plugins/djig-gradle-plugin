package org.taruts.djigGradlePlugin

import org.apache.commons.lang3.StringUtils
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.api.tasks.TaskProvider
import org.taruts.djigGradlePlugin.extension.DjigPluginExtension
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription

/**
 * The djig Gradle plugin itself.
 *
 * It can be applied to a [workspace](https://plugins.gradle.org/plugin/org.taruts.workspace) project,
 * containing a djig application as one of its projects.
 *
 * It reads [DjigPluginExtension] and creates the following tasks.
 *
 * - **init*TaskNameSuffix*PersonalDynamicProjects**. One like this is created for each [TaskDescription]
 * in the [DjigPluginExtension.taskDescriptions] container.
 * The ***TaskNameSuffix*** is the CamelCase of the corresponding [TaskDescription.taskShortName]
 * or [TaskDescription.Target.springBootProfile] if [TaskDescription.taskShortName] is not set.
 *
 * - **initPersonalDynamicProjects**. This task aggregates all init*TaskNameSuffix*PersonalDynamicProjects tasks.
 *
 * - **initAll**. This task is like a one big button initializing everything
 * in a [workspace](https://plugins.gradle.org/plugin/org.taruts.workspace)
 * where there is a djig application which maybe also uses [gitlab-container](https://plugins.gradle.org/plugin/org.taruts.gitlab-container),
 * when run locally.
 * It is supposed to be run by a developer who just starts working with the workspace, right after cloning the workspace on their computer.
 * It's like you clone the workspace, run initAll, all the workspace projects are cloned automatically, the plugin container is created if needed,
 * personal dynamic projects are created and their properties are written to the personal profile of the djig application.
 * This task executes the following tasks in the following order.
 *      - gitLabContainerCreateAll (the [gitlab-container](https://plugins.gradle.org/plugin/org.taruts.gitlab-container) plugin) **(&#42;)**
 *      - initPersonalDynamicProjects
 *      - cloneAll (the [workspace](https://plugins.gradle.org/plugin/org.taruts.workspace) plugin)
 *
 * **(&#42;)** - **gitLabContainerCreateAll** is only attached to **initAll**
 * only if there is at least one init*TaskNameSuffix*PersonalDynamicProjects
 * created with [TaskDescription.Target.gitLabContainer], because it is going to create its target dynamic projects in the container,
 * so the container should be created at first.
 *
 * @see DjigPluginExtension
 * @see TaskDescription
 * @see InitPersonalDynamicProjectsTask
 */
class DjigPlugin : Plugin<Project> {

    companion object {
        const val DJIG_NAME = "djig"
        const val TASK_GROUP = DJIG_NAME
        const val EXTENSION_NAME = DJIG_NAME
    }

    override fun apply(project: Project) {
        val extension: DjigPluginExtension = project.extensions.create(EXTENSION_NAME, DjigPluginExtension::class.java)
        project.afterEvaluate {
            configureExtension(it, extension)
        }
    }

    private fun configureExtension(project: Project, extension: DjigPluginExtension) {
        val hasContainerGitLabTask = object {
            var value: Boolean = false
        }

        val sourceSpringBootProfile: String = extension.sourceDynamicProjects.springBootProfile.orNull ?: return

        val tasksByShortNames: Map<String, TaskDescription> = extension.taskDescriptions.asMap

        val taskNames: List<String> = tasksByShortNames.map { (shortName, taskDescription) ->

            val taskNameSuffix = shortName
                .split("-")
                .joinToString(separator = "", transform = StringUtils::capitalize)
            val taskName = "init${taskNameSuffix}PersonalDynamicProjects"

            val taskProvider: TaskProvider<InitPersonalDynamicProjectsTask> = project.tasks.register(
                taskName,
                InitPersonalDynamicProjectsTask::class.java,
                extension,
                taskDescription
            )

            if (taskDescription.target.hosting.gitLabContainer.get()) {
                hasContainerGitLabTask.value = true
                taskProvider.configure {
                    it.mustRunAfter("gitLabContainerCreateAll")
                }
            }

            taskName
        }

        project.tasks.register("initPersonalDynamicProjects") {
            it.group = TASK_GROUP
            it.description = """
                Initializes dynamic projects from application-${sourceSpringBootProfile}.properties for all GitLabs.
                This is an aggregator task for these tasks: ${taskNames.joinToString(", ")}.
                """.trimIndent()
            it.dependsOn(*taskNames.toTypedArray())
        }

        // One button initializer of all right after cloning the workspace.
        project.tasks.register("initAll") {
            it.group = TASK_GROUP
            it.description = """
                An aggregator tasks initializing everything for the workspace.
                """.trimIndent()
            it.dependsOn("cloneAll", "initPersonalDynamicProjects")
        }

        // Before initPersonalDynamicProjects is executed, at least the djig application must be already cloned, so cloneAll goes first.
        project.tasks.named("initPersonalDynamicProjects") {
            it.mustRunAfter("cloneAll")
        }

        // If one of the tasks creates projects in a local container GitLab created by gitlab-container-plugin,
        // then add the container initialization to initAll and make it run before all other tasks that initAll aggregates.
        if (hasContainerGitLabTask.value) {

            // Add the container initialization to initAll
            project.tasks.named("initAll") {
                it.dependsOn("gitLabContainerCreateAll")
            }

            // Make all other tasks in initAll run after the container is initialized
            project.tasks.named("initPersonalDynamicProjects") {
                it.mustRunAfter("gitLabContainerCreateAll")
            }
            project.tasks.named("cloneAll") {
                it.mustRunAfter("gitLabContainerCreateAll")
            }
        }
    }
}
