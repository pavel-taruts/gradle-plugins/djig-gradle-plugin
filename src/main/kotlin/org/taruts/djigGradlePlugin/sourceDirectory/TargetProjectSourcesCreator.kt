package org.taruts.djigGradlePlugin.sourceDirectory

import org.apache.commons.io.FileUtils
import org.gradle.api.Project
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.gitUtils.GitUtils
import org.taruts.gitUtils.hosting.credentials.CredentialsUtils
import org.taruts.processUtils.ProcessRunner
import java.io.File
import java.nio.file.Path

/**
 * See [createTargetProjectSources].
 */
class TargetProjectSourcesCreator(
    private val gradleProject: Project,
    private val taskDescription: TaskDescription,
) {
    /**
     * Clones the source project named [projectName], the properties of which is [sourceProjectProperties],
     * into a subdirectory in the [workspace](https://plugins.gradle.org/plugin/org.taruts.workspace),
     * and prepares the newly created Git repo to be the one for the target project named [projectName],
     * the properties of which is [targetProjectProperties].
     */
    fun createTargetProjectSources(
        projectName: String,
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        targetProjectProperties: DjigConfigurationProperties.DynamicProject
    ): File {
        val dynamicLocalSourceDir = cloneTargetProjectSourcesFromSourceProject(projectName, sourceProjectProperties)
        configureLocalGitRepoSettings(dynamicLocalSourceDir, targetProjectProperties)
        removeRemotesAndLocalBranches(dynamicLocalSourceDir)
        return dynamicLocalSourceDir
    }

    /**
     * Cloning to a subdirectory in the project
     */
    private fun cloneTargetProjectSourcesFromSourceProject(projectName: String, sourceProjectProperties: DjigConfigurationProperties.DynamicProject): File {
        val dynamicLocalSourceDir = getDynamicLocalSourceDir(projectName)

        var sourceHostingUrl = sourceProjectProperties.url.toString()
        if (!sourceProjectProperties.username.isNullOrBlank()) {
            val (sourceHostingUsername, sourceHostingPassword) = CredentialsUtils.apiCredentialsToGitUtilityCredentials(
                sourceProjectProperties.username,
                sourceProjectProperties.password
            )
            sourceHostingUrl = GitUtils.addCredentialsToGitRepositoryUrl(sourceHostingUrl, sourceHostingUsername, sourceHostingPassword)
        }

        // Getting the source branch
        val sourceProjectBranch: String = sourceProjectProperties.branch

        GitUtils.clone(sourceHostingUrl, sourceProjectBranch, dynamicLocalSourceDir)
        return dynamicLocalSourceDir
    }

    private fun getDynamicLocalSourceDir(projectName: String): File {
        // Determining the path of the local source directory
        val projectNameToSourcePathFunction = taskDescription.transform.djigProjectNameToSourcePathFunction.get()
        val projectRelativePath = projectNameToSourcePathFunction(projectName)
        val dynamicLocalDirectory: File = Path
            // todo The "projects" here must be either a parameter of the workspace Gradle plugin or a constant of its
            .of(gradleProject.rootProject.projectDir.path, "projects", projectRelativePath)
            .toAbsolutePath()
            .normalize()
            .toFile()
        // Remove the source directory if it exists
        if (dynamicLocalDirectory.exists()) {
            FileUtils.forceDelete(dynamicLocalDirectory)
        }
        FileUtils.forceMkdir(dynamicLocalDirectory)
        return dynamicLocalDirectory
    }

    private fun configureLocalGitRepoSettings(dynamicLocalSourceDir: File, targetProjectProperties: DjigConfigurationProperties.DynamicProject) {
        val username = if (targetProjectProperties.password.isNullOrBlank()) "token" else targetProjectProperties.username
        ProcessRunner.runProcess(dynamicLocalSourceDir, "git", "config", "--local", "user.name", username)
        ProcessRunner.runProcess(dynamicLocalSourceDir, "git", "config", "--local", "user.email", "${username}@mail.com")
        ProcessRunner.runProcess(dynamicLocalSourceDir, "git", "config", "--local", "advice.detachedHead", "false")
    }

    private fun removeRemotesAndLocalBranches(dynamicLocalSourceDir: File) {
        // Moving HEAD from the local source branch to its hash
        val currentHash = ProcessRunner.runProcess(dynamicLocalSourceDir, "git", "rev-parse", "HEAD")
        ProcessRunner.runProcess(dynamicLocalSourceDir, "git", "checkout", currentHash)

        // Delete the local source branch and all other local branches
        val branchesStr = ProcessRunner.runProcess(dynamicLocalSourceDir, "git", "branch", "--list")
        branchesStr
            .lineSequence()
            // Filter out the current branch which is marked by *
            .filter { !it.contains("*") }
            .forEach {
                ProcessRunner.runProcess(dynamicLocalSourceDir, "git", "branch", "--delete", it.trim())
            }

        // Remove all remotes
        val remotesStr = ProcessRunner.runProcess(dynamicLocalSourceDir, "git", "remote")
        remotesStr
            .lineSequence()
            .forEach {
                ProcessRunner.runProcess(dynamicLocalSourceDir, "git", "remote", "remove", it)
            }
    }
}
