package org.taruts.djigGradlePlugin.hostingPlace

/**
 * The model for project groups. This model is used by algorithms abstracted from the hosting software brand.
 */
class InternalGroup(
    val name: String,
    val originalIndex: Int?
) {
    /**
     * How this group is seen as a part of the project path string.
     */
    override fun toString(): String = name
}
