package org.taruts.djigGradlePlugin.hostingPlace

import org.taruts.djig.configurationProperties.DjigConfigurationProperties.DynamicProject
import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djig.configurationProperties.utils.HostingTypeDetector
import org.taruts.djigGradlePlugin.credentials.Credentials
import org.taruts.djigGradlePlugin.credentials.TargetCredentialsProviderWrapper
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.SourceHostingStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.SourceHostingStrategySelector
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.TargetHostingStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.TargetHostingStrategySelector
import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.PathTransformer
import org.taruts.djigGradlePlugin.targetBranch.TargetBranchProvider

/**
 * @see createHostingPlace
 */
class HostingPlaceCreator(
    private val taskDescription: TaskDescription,
    private val sourceProjectPathExtractor: SourceProjectPathExtractor,
    private val sourceProjectBranchMode: Boolean,
    private val sourceHostingStrategySelector: SourceHostingStrategySelector,
    private val targetCredentialsProviderWrapper: TargetCredentialsProviderWrapper,
    private val targetBranchProvider: TargetBranchProvider,
    private val targetHostingStrategySelector: TargetHostingStrategySelector
) {

    /**
     * Creates a place on the target hosting
     * to host a target project named [projectName]
     * which is copied from a source project of the same name
     * with properties [sourceProjectProperties].
     *
     * It only creates the place on the hosting for the target project to sit in. It does not put the project sources there.
     *
     * @return The [DynamicProject] with properties of the target project
     */
    fun createHostingPlace(projectName: String, sourceProjectProperties: DynamicProject): DynamicProject {
        val targetHostingCredentials: Credentials = targetCredentialsProviderWrapper.getTargetHostingCredentials(projectName, sourceProjectProperties)

        val targetHttpUrlToRepo: String = createHostingPlace(sourceProjectProperties, targetHostingCredentials)

        val targetProjectProperties: DynamicProject = buildTargetProjectProperties(
            projectName,
            sourceProjectProperties,
            targetHttpUrlToRepo,
            targetHostingCredentials
        )

        validateTargetProjectProperties(projectName, sourceProjectProperties, targetProjectProperties)

        return targetProjectProperties
    }

    private fun createHostingPlace(sourceProjectProperties: DynamicProject, targetHostingCredentials: Credentials): String {
        if (sourceProjectBranchMode) {
            return sourceProjectProperties.url
        }

        val sourceHostingType: HostingType = sourceProjectProperties.hostingType ?: HostingTypeDetector.detect(sourceProjectProperties.url)
        val sourceHostingStrategy: SourceHostingStrategy = sourceHostingStrategySelector.select(sourceHostingType)

        val targetHostingStrategy: TargetHostingStrategy = targetHostingStrategySelector.select()

        return copy(sourceHostingStrategy, sourceProjectProperties, targetHostingStrategy, targetHostingCredentials)
    }

    private fun copy(
        sourceHostingStrategy: SourceHostingStrategy,
        sourceProjectProperties: DynamicProject,
        targetHostingStrategy: TargetHostingStrategy,
        targetHostingCredentials: Credentials
    ): String {
        val sourcePathParts: List<String> = getSourceProjectPathParts(sourceProjectProperties.url, sourceHostingStrategy)

        val sourceGroupNames: List<String> = sourcePathParts.subList(0, sourcePathParts.size - 1)
        val sourceProjectName = sourcePathParts.last()

        val sourceInternalGroups: List<InternalGroup> =
            sourceGroupNames.mapIndexed { index: Int, sourceGroupName: String ->
                InternalGroup(sourceGroupName, index)
            }

        val targetInternalGroups: List<InternalGroup> = transformNamespace(sourceInternalGroups)

        val hostingProjectNameTransformer = taskDescription.transform.hostingProjectNameTransformer.get()
        val targetHostingProjectName = hostingProjectNameTransformer(sourceProjectName)

        return targetHostingStrategy.recreateProject(
            sourceProjectProperties,
            sourceHostingStrategy,
            targetHostingCredentials,
            targetInternalGroups,
            targetHostingProjectName
        )
    }

    private fun getSourceProjectPathParts(urlStr: String, sourceHostingStrategy: SourceHostingStrategy): List<String> {
        val sourceProjectPath: String = sourceProjectPathExtractor.getSourceProjectPathFromUrl(urlStr, sourceHostingStrategy)
        return sourceProjectPath.split(sourceHostingStrategy.sourceGroupsSeparator)
    }

    private fun transformNamespace(sourceGroups: List<InternalGroup>): List<InternalGroup> {

        val parse: (str: String?) -> List<InternalGroup> = parse@{ str: String? ->
            if (str == null) {
                return@parse listOf()
            } else {
                return@parse str
                    .split("/")
                    .map { InternalGroup(it, null) }
            }
        }

        val format: (segments: List<InternalGroup>) -> String = format@{ segments: List<InternalGroup> ->
            return@format segments.joinToString("/")
        }

        val transformNamespaceExpression: String = taskDescription.transform.transformNamespaceExpression.get()
        return PathTransformer<InternalGroup>().transformPath(sourceGroups, transformNamespaceExpression, parse, format)
    }

    private fun buildTargetProjectProperties(
        projectName: String,
        sourceProjectProperties: DynamicProject,
        httpUrlToRepo: String,
        targetHostingCredentials: Credentials
    ): DynamicProject {
        val targetProjectProperties = DynamicProject()
        targetProjectProperties.url = httpUrlToRepo
        targetProjectProperties.dynamicInterfacePackage = sourceProjectProperties.dynamicInterfacePackage
        targetProjectProperties.buildType = sourceProjectProperties.buildType
        targetProjectProperties.hostingType = if (sourceProjectBranchMode) {
            sourceProjectProperties.hostingType
        } else {
            taskDescription.target.hosting.hostingType.get()
        }
        targetProjectProperties.username = targetHostingCredentials.username
        targetProjectProperties.password = targetHostingCredentials.password
        targetProjectProperties.branch = targetBranchProvider.getTargetBranch(projectName, sourceProjectProperties, targetProjectProperties)
        return targetProjectProperties
    }

    private fun validateTargetProjectProperties(
        projectName: String,
        sourceProjectProperties: DynamicProject,
        targetProjectProperties: DynamicProject
    ) {
        if (targetProjectProperties.url == sourceProjectProperties.url && targetProjectProperties.branch == sourceProjectProperties.branch) {
            error(
                "The target project $projectName has the same URL and branch as the source project. " +
                        "URL = ${targetProjectProperties.url}, branch = ${targetProjectProperties.branch}"
            )
        }
    }
}
