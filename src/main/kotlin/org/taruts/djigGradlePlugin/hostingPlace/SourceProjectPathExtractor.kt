package org.taruts.djigGradlePlugin.hostingPlace

import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.SourceHostingStrategy
import org.taruts.djigGradlePlugin.hostingPlace.util.GitUrlUtils

/**
 * @see getSourceProjectPathFromUrl
 */
class SourceProjectPathExtractor {

    /**
     * Extracts the slash separated path for the project the URL of which is [urlStr].
     */
    fun getSourceProjectPathFromUrl(urlStr: String, sourceHostingStrategy: SourceHostingStrategy): String {
        val sourceProjectUrlPathWithoutFileExtension: String = GitUrlUtils.getProjectUrlPathWithoutFileExtension(urlStr)
        return sourceHostingStrategy.sourceProjectUrlPathWithoutExtensionToProjectPath(sourceProjectUrlPathWithoutFileExtension)
    }
}
