package org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab.groupsCopying

import org.taruts.djig.configurationProperties.HostingType

/**
 * The selector of the [ToLabGroupsCopier] implementation to use based on the source hosting type.
 * @see HubToLabGroupsCopier
 * @see LabToLabGroupsCopier
 */
class ToLabGroupsCopierSelector(
    private val hubToLabGroupsCopier: HubToLabGroupsCopier,
    private val labToLabGroupsCopier: LabToLabGroupsCopier
) {
    fun select(sourceHostingType: HostingType): ToLabGroupsCopier {
        return when (sourceHostingType) {
            HostingType.GitHub -> hubToLabGroupsCopier
            HostingType.GitLab -> labToLabGroupsCopier
        }
    }
}
