package org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab

import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.GitLabApiException
import org.gitlab4j.api.models.Group
import org.gitlab4j.api.models.Project
import org.gitlab4j.api.models.Visibility
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djigGradlePlugin.credentials.Credentials
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.SourceHostingStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.TargetHostingStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab.groupsCopying.ToLabGroupsCopier
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab.groupsCopying.ToLabGroupsCopierSelector
import org.taruts.djigGradlePlugin.hostingPlace.util.GitLabApiFactory
import java.net.URI
import java.net.URL

/**
 * The [TargetHostingStrategy] implementation used when the target hosting is [gitlab.com](https://gitlab.com).
 */
class TargetLabStrategy(
    private val taskDescription: TaskDescription,
    private val toLabGroupsCopierSelector: ToLabGroupsCopierSelector
) : TargetHostingStrategy {

    override val targetHostingType: HostingType = HostingType.GitLab

    private var deepestTargetGroup: Group? = null

    private var targetGitLabApi: GitLabApi? = null

    private val targetGitLabVisibility: Visibility
        get() = when (taskDescription.target.visibility.get()) {
            org.taruts.djigGradlePlugin.extension.Visibility.PUBLIC -> Visibility.PUBLIC
            org.taruts.djigGradlePlugin.extension.Visibility.PRIVATE -> Visibility.PRIVATE
            null -> error("Shouldn't be here ever")
        }

    override fun recreateProject(
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        sourceHostingStrategy: SourceHostingStrategy,
        targetHostingCredentials: Credentials,
        targetInternalGroups: List<org.taruts.djigGradlePlugin.hostingPlace.InternalGroup>,
        targetHostingProjectName: String
    ): String {
        val targetHostingUrl: URL = taskDescription.target.hosting.url.get()

        targetGitLabApi = GitLabApiFactory.getGitLabApi(
            targetHostingUrl,
            targetHostingCredentials.username,
            targetHostingCredentials.password
        )

        val toLabGroupsCopier: ToLabGroupsCopier = toLabGroupsCopierSelector.select(sourceHostingStrategy.sourceHostingType)

        deepestTargetGroup = toLabGroupsCopier.copyProjectGroups(
            sourceProjectProperties,
            sourceHostingStrategy,
            targetGitLabApi!!,
            targetGitLabVisibility,
            targetInternalGroups
        )

        val targetProject = recreateProject(targetHostingProjectName)

        return replaceDomainNamePlaceholder(targetProject.httpUrlToRepo, targetHostingUrl)
    }

    private fun recreateProject(projectName: String): Project {
        deleteProject(projectName)
        return createProject(projectName)
    }

    private fun deleteProject(projectName: String) {
        val maxAttempts: Int = taskDescription.target.hosting.gitLab.deleteProject.maxAttempts.get()
        val delay: Long = taskDescription.target.hosting.gitLab.deleteProject.delay.get()

        var deletionWasTried = false
        var attemptNumber = maxAttempts
        while (attemptNumber > 0) {
            val project: Project? =
                targetGitLabApi!!.projectApi.getOptionalProject(deepestTargetGroup?.fullPath, projectName).orElse(null)
            if (project == null) {
                break
            }
            if (!deletionWasTried) {
                deletionWasTried = true
                println("Deleting existing project...")
            }
            println("Attempts left: $attemptNumber...")
            targetGitLabApi!!.projectApi.deleteProject(project.id)
            Thread.sleep(delay)
            attemptNumber--
        }
        if (attemptNumber == 0) {
            throw RuntimeException("Could not delete project $projectName")
        }
        if (deletionWasTried) {
            println("Project has been deleted")
        }
    }

    private fun createProject(projectName: String): Project {
        println("Creating project...")

        // There are attempts because after disappearing from the API a project may still exist for some time in GitLab underwater,
        // making attempts to create another project with the same name and in the same namespace fail

        val maxAttempts: Int = taskDescription.target.hosting.gitLab.createProject.maxAttempts.get()
        val delay: Long = taskDescription.target.hosting.gitLab.createProject.delay.get()

        for (attemptNumber in maxAttempts downTo 1) {
            try {
                val project: Project? =
                    targetGitLabApi!!.projectApi.getOptionalProject(deepestTargetGroup?.fullPath, projectName)
                        .orElse(null)
                if (project != null) {
                    throw RuntimeException("Gitlab project creation error: attempt $attemptNumber, there is a project by this path already")
                }
                val labProject = Project()
                    .withName(projectName)
                    .withVisibility(targetGitLabVisibility)
                println("Attempts left: $attemptNumber...")
                val createdProject = targetGitLabApi!!.projectApi.createProject(deepestTargetGroup?.id, labProject)
                println("Project has been created")
                return createdProject
            } catch (e: GitLabApiException) {
                // Most probably, a project with the same name and in the same namespace is still being deleted
                if (attemptNumber > 1) {
                    val validationErrors = e.validationErrors
                    val message = if (validationErrors == null) {
                        e.message
                    } else {
                        validationErrors.entries.stream()
                            .findFirst()
                            .map { (key, value) -> "key = $key, value = $value" }
                            .orElse("no message")
                    }
                    println("Gitlab project creation error: attempt $attemptNumber, $message")
                    Thread.sleep(delay)
                } else {
                    throw RuntimeException("Run out of attempts", e)
                }
            }
        }
        throw IllegalStateException("Execution isn't supposed to reach here")
    }

    private fun replaceDomainNamePlaceholder(targetProjectUrlStr: String, targetHostingUrl: URL): String {
        val targetHostingUri = targetHostingUrl.toURI()
        var targetProjectUri = URI(targetProjectUrlStr)

        val domainNamePlaceholder = "gitlab.domain.name.placeholder"
        if (targetProjectUri.authority.equals(domainNamePlaceholder)) {
            targetProjectUri = URI(
                targetProjectUri.scheme,
                targetHostingUri.authority,
                targetProjectUri.path,
                targetProjectUri.query,
                targetProjectUri.fragment
            )
        }

        return targetProjectUri.toString()
    }
}
