package org.taruts.djigGradlePlugin.hostingPlace.strategies.target.hub

import org.kohsuke.github.GHRepository
import org.kohsuke.github.GitHub
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djigGradlePlugin.credentials.Credentials
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.SourceHostingStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.TargetHostingStrategy
import org.taruts.djigGradlePlugin.hostingPlace.util.GitHubFactory

/**
 * The [TargetHostingStrategy] implementation used when the target hosting is [github.com](https://github.com).
 */
class TargetHubStrategy(
    private val taskDescription: TaskDescription
) : TargetHostingStrategy {

    override val targetHostingType: HostingType = HostingType.GitHub

    private val targetGitHubVisibility: GHRepository.Visibility
        get() = when (taskDescription.target.visibility.get()) {
            org.taruts.djigGradlePlugin.extension.Visibility.PUBLIC -> GHRepository.Visibility.PUBLIC
            org.taruts.djigGradlePlugin.extension.Visibility.PRIVATE -> GHRepository.Visibility.PRIVATE
            null -> error("Shouldn't be here ever")
        }

    override fun recreateProject(
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        sourceHostingStrategy: SourceHostingStrategy,
        targetHostingCredentials: Credentials,
        targetInternalGroups: List<org.taruts.djigGradlePlugin.hostingPlace.InternalGroup>,
        targetHostingProjectName: String
    ): String {
        val targetGroupsSeparator = taskDescription.target.gitHub.groupsSeparator.get()
        val targetProjectNamePrefix: String = targetInternalGroups
            .map(org.taruts.djigGradlePlugin.hostingPlace.InternalGroup::name)
            .joinToString(targetGroupsSeparator, "", targetGroupsSeparator)

        val targetProjectFullName = targetProjectNamePrefix + targetHostingProjectName

        val gitHubRepository: GHRepository = recreateGitHubRepository(targetProjectFullName, targetHostingCredentials)

        return gitHubRepository.httpTransportUrl
    }

    private fun recreateGitHubRepository(targetProjectFullName: String, targetHostingCredentials: Credentials): GHRepository {

        val targetGitHub: GitHub = GitHubFactory.getGitHub(
            targetHostingCredentials.username,
            targetHostingCredentials.password
        )

        targetGitHub.myself.getRepository(targetProjectFullName)?.delete()

        return targetGitHub
            .createRepository(targetProjectFullName)
            .private_(targetGitHubVisibility == GHRepository.Visibility.PRIVATE)
            .create()
    }
}
