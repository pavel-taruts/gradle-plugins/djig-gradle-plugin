package org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab.groupsCopying

import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.Group
import org.gitlab4j.api.models.Namespace
import org.gitlab4j.api.models.Project
import org.gitlab4j.api.models.Visibility
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djigGradlePlugin.hostingPlace.InternalGroup
import org.taruts.djigGradlePlugin.hostingPlace.SourceProjectPathExtractor
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.SourceHostingStrategy
import org.taruts.djigGradlePlugin.hostingPlace.util.GitLabApiFactory
import java.net.URI

/**
 * The [ToLabGroupsCopier] implementation copying project groups from [gitlab.com](https://gitlab.com).
 */
class LabToLabGroupsCopier(private val sourceProjectPathExtractor: SourceProjectPathExtractor) : ToLabGroupsCopier() {

    override val sourceHostingType: HostingType = HostingType.GitLab

    override fun copyProjectGroups(
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        sourceHostingStrategy: SourceHostingStrategy,
        targetGitLabApi: GitLabApi,
        targetGitLabVisibility: Visibility,
        targetInternalGroups: List<InternalGroup>
    ): Group? {

        val sourceGroups: List<Group> = getProjectGroups(sourceProjectProperties, sourceHostingStrategy)

        var previousTargetGroup: Group? = null

        for (targetGroupInternal in targetInternalGroups) {
            val targetGroup: Group =
                if (targetGroupInternal.originalIndex == null)
                    Group()
                        .withPath(targetGroupInternal.name)
                        .withName(targetGroupInternal.name)
                else
                    sourceGroups[targetGroupInternal.originalIndex]

            previousTargetGroup = copyGroup(targetGroup, previousTargetGroup, targetGitLabApi, targetGitLabVisibility)
        }

        return previousTargetGroup
    }

    private fun getProjectGroups(sourceProjectProperties: DjigConfigurationProperties.DynamicProject, sourceHostingStrategy: SourceHostingStrategy): List<Group> {
        val groups: MutableList<Group> = ArrayList()

        val sourceProjectPath = sourceProjectPathExtractor.getSourceProjectPathFromUrl(sourceProjectProperties.url, sourceHostingStrategy)

        val projectUri = URI(sourceProjectProperties.url)
        val gitlabUri = URI(projectUri.scheme, projectUri.authority, null, null, null)

        val sourceGitLabApi: GitLabApi = GitLabApiFactory.getGitLabApi(
            gitlabUri.toURL(),
            sourceProjectProperties.username,
            sourceProjectProperties.password
        )

        val sourceGitLabProject: Project = sourceGitLabApi.projectApi.getProject(sourceProjectPath)

        val namespace: Namespace? = sourceGitLabProject.namespace
        if (namespace != null && namespace.kind == "group") {
            var group: Group? = sourceGitLabApi.groupApi.getGroup(namespace.id)
            while (group != null) {
                groups.add(group)
                group = group.parentId?.let(sourceGitLabApi.groupApi::getGroup)
            }
        }

        return groups.asReversed()
    }
}
