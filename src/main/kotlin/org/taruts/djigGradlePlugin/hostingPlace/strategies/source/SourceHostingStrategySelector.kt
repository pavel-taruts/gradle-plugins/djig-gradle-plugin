package org.taruts.djigGradlePlugin.hostingPlace.strategies.source

import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.hub.SourceHubStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.lab.SourceLabStrategy

/**
 * The selector of the [SourceHostingStrategy] to use, depending on the software brand of the source hosting.
 */
class SourceHostingStrategySelector(
    private val sourceHubStrategy: SourceHubStrategy,
    private val sourceLabStrategy: SourceLabStrategy
) {

    /**
     * Selects the [SourceHostingStrategy] based on [sourceHostingType].
     */
    fun select(sourceHostingType: HostingType): SourceHostingStrategy {
        return when (sourceHostingType) {
            HostingType.GitHub -> sourceHubStrategy
            HostingType.GitLab -> sourceLabStrategy
        }
    }
}
