package org.taruts.djigGradlePlugin.hostingPlace.strategies.source.lab

import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.SourceHostingStrategy

/**
 * The [SourceHostingStrategy] for [sourceHostingType] of [HostingType.GitLab].
 */
class SourceLabStrategy : SourceHostingStrategy {
    override val sourceHostingType: HostingType = HostingType.GitLab

    override val sourceGroupsSeparator: String = "/"

    /**
     * In case of gitlab [gitlab.com](https://gitlab.com), the path a project has on the hosting is the same as the path of the URL, omitting the **.git** at the end.
     * So here, we just return the [sourceProjectUrlPathWithoutFileExtension] (which is already stripped of the **.git**) as the project path, without any transformation.
     */
    override fun sourceProjectUrlPathWithoutExtensionToProjectPath(sourceProjectUrlPathWithoutFileExtension: String): String {
        return sourceProjectUrlPathWithoutFileExtension.replace(
            """^/""".toRegex(),
            ""
        )
    }
}
