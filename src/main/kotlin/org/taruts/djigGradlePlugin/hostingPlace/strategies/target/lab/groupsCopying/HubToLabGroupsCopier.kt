package org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab.groupsCopying

import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.models.Group
import org.gitlab4j.api.models.Visibility
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djigGradlePlugin.hostingPlace.InternalGroup
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.SourceHostingStrategy

/**
 * The [ToLabGroupsCopier] implementation copying project groups from [github.com](https://github.com).
 */
class HubToLabGroupsCopier : ToLabGroupsCopier() {

    override val sourceHostingType: HostingType = HostingType.GitHub

    override fun copyProjectGroups(
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        sourceHostingStrategy: SourceHostingStrategy,
        targetGitLabApi: GitLabApi,
        targetGitLabVisibility: Visibility,
        targetInternalGroups: List<InternalGroup>
    ): Group? {

        var previousTargetGroup: Group? = null

        for (targetInternalGroup in targetInternalGroups) {
            val targetGroup: Group =
                Group()
                    .withPath(targetInternalGroup.name)
                    .withName(targetInternalGroup.name)

            previousTargetGroup = copyGroup(targetGroup, previousTargetGroup, targetGitLabApi, targetGitLabVisibility)
        }

        return previousTargetGroup
    }
}
