package org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab.groupsCopying

import org.gitlab4j.api.GitLabApi
import org.gitlab4j.api.GitLabApiException
import org.gitlab4j.api.models.Group
import org.gitlab4j.api.models.Visibility
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djigGradlePlugin.hostingPlace.InternalGroup
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.SourceHostingStrategy

/**
 * A base class, a subclass of which helps copy project groups to [gitlab.com](https://gitlab.com) from a hosting type supported by the subclass.
 * @see HubToLabGroupsCopier
 * @see LabToLabGroupsCopier
 */
abstract class ToLabGroupsCopier {

    abstract val sourceHostingType: HostingType

    abstract fun copyProjectGroups(
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        sourceHostingStrategy: SourceHostingStrategy,
        targetGitLabApi: GitLabApi,
        targetGitLabVisibility: Visibility,
        targetInternalGroups: List<InternalGroup>
    ): Group?

    protected fun copyGroup(
        sourceGroup: Group,
        targetParentGroup: Group?,
        targetGitLabApi: GitLabApi,
        targetGitLabVisibility: Visibility
    ): Group? {
        val targetGroup = Group()
            .withName(sourceGroup.name)
            .withPath(sourceGroup.path)
            .withVisibility(targetGitLabVisibility)
            .withParentId(targetParentGroup?.id)

        return recreateGroup(targetGitLabApi, targetParentGroup, targetGroup)
    }

    private fun recreateGroup(
        targetGitLabApi: GitLabApi,
        targetParentGroup: Group?,
        targetGroup: Group
    ): Group? {
        val pathPrefix =
            if (targetParentGroup == null)
                ""
            else
                (targetParentGroup.fullPath + "/")
        val targetGroupFullPath = pathPrefix + targetGroup.path

        return targetGitLabApi.groupApi.getOptionalGroup(targetGroupFullPath).orElseGet {
            try {
                targetGitLabApi.groupApi.addGroup(targetGroup)
            } catch (e: GitLabApiException) {
                if (e.httpStatus == 403 && targetParentGroup != null) {
                    throw RuntimeException(
                        "It's forbidden to create a group $targetGroupFullPath. " +
                                "Is the PARENT group ${targetParentGroup.webUrl} yours? " +
                                "Or have you been granted proper access to it?", e
                    )
                } else {
                    throw e
                }
            }
        }
    }
}
