package org.taruts.djigGradlePlugin.hostingPlace.strategies.target

import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djig.configurationProperties.utils.HostingTypeDetector
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.hub.TargetHubStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab.TargetLabStrategy
import java.net.URL

/**
 * The selector of the [TargetHostingStrategy] to use, depending on the software brand of the target hosting.
 */
class TargetHostingStrategySelector(
    private val taskDescription: TaskDescription,
    private val targetHubStrategy: TargetHubStrategy,
    private val targetLabStrategy: TargetLabStrategy,
) {

    /**
     * Returns the [TargetHostingStrategy] to use, based on the [TaskDescription.Target.HostingDescription.hostingType] value specified by user.
     * If it's not specified, then the [TargetHostingStrategy] is deduced from [TaskDescription.Target.HostingDescription.url].
     */
    fun select(): TargetHostingStrategy {
        val targetHostingType: HostingType = taskDescription.target.hosting.hostingType.orNull ?: deduceHostingTypeFromUrl()
        return when (targetHostingType) {
            HostingType.GitHub -> targetHubStrategy
            HostingType.GitLab -> targetLabStrategy
        }
    }

    private fun deduceHostingTypeFromUrl(): HostingType {
        val targetHostingUrl: URL = taskDescription.target.hosting.url.get()
        return HostingTypeDetector.detect(targetHostingUrl)
    }
}
