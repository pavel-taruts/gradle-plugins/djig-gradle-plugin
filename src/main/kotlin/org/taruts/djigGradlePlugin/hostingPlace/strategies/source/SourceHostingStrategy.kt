package org.taruts.djigGradlePlugin.hostingPlace.strategies.source

import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.hub.SourceHubStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.lab.SourceLabStrategy

/**
 * The interface that defines operations used in the process of copying source projects into target ones,
 * that must be performed differently depending on the source hosting software brand.
 * @see SourceHubStrategy
 * @see SourceLabStrategy
 */
interface SourceHostingStrategy {

    /**
     * The source [HostingType] this strategy is for.
     */
    val sourceHostingType: HostingType

    /**
     * The character sequence, separating the groups of a project in its clone URL.
     */
    val sourceGroupsSeparator: String

    /**
     * Returns the path a project has on its hosting, given the path part of the clone URL, stripped of the **.git**.
     */
    fun sourceProjectUrlPathWithoutExtensionToProjectPath(sourceProjectUrlPathWithoutFileExtension: String): String
}
