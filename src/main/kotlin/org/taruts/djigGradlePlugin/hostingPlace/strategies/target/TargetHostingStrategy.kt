package org.taruts.djigGradlePlugin.hostingPlace.strategies.target

import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djigGradlePlugin.credentials.Credentials
import org.taruts.djigGradlePlugin.hostingPlace.InternalGroup
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.SourceHostingStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.hub.TargetHubStrategy
import org.taruts.djigGradlePlugin.hostingPlace.strategies.target.lab.TargetLabStrategy

/**
 * This interface defines operations used in the process of copying source projects into target ones,
 * that must be performed differently depending on the target hosting software brand.
 * @see TargetHubStrategy
 * @see TargetLabStrategy
 */
interface TargetHostingStrategy {
    val targetHostingType: HostingType

    fun recreateProject(
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject,
        sourceHostingStrategy: SourceHostingStrategy,
        targetHostingCredentials: Credentials,
        targetInternalGroups: List<InternalGroup>,
        targetHostingProjectName: String
    ): String
}
