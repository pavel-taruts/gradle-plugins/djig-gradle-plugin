package org.taruts.djigGradlePlugin.hostingPlace.strategies.source.hub

import org.taruts.djig.configurationProperties.HostingType
import org.taruts.djigGradlePlugin.extension.DjigPluginExtension
import org.taruts.djigGradlePlugin.hostingPlace.strategies.source.SourceHostingStrategy

/**
 * The [SourceHostingStrategy] for [sourceHostingType] of [HostingType.GitHub].
 */
class SourceHubStrategy(
    extension: DjigPluginExtension
) : SourceHostingStrategy {
    override val sourceHostingType: HostingType = HostingType.GitHub

    override val sourceGroupsSeparator: String = extension.sourceDynamicProjects.gitHub.groupsSeparator.get()

    /**
     * Extracts the path the project has on the hosting by removing the first **something&#47;** from the path of the URL.
     */
    override fun sourceProjectUrlPathWithoutExtensionToProjectPath(sourceProjectUrlPathWithoutFileExtension: String): String {
        return sourceProjectUrlPathWithoutFileExtension.replace(
            """^/[^/]+/""".toRegex(),
            ""
        )
    }
}
