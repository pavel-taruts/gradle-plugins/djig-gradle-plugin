package org.taruts.djigGradlePlugin.hostingPlace.util

import org.gitlab4j.api.GitLabApi
import org.taruts.djigGradlePlugin.hostingPlace.util.GitLabApiFactory.getGitLabApi
import java.net.URL

/**
 * @see getGitLabApi
 */
object GitLabApiFactory {

    /**
     * Gets an authenticated [GitLabApi] by credentials.
     *
     * If both [username] and [password] are specified, then the login-password authentication is used.
     *
     * If there is only [username] then it's expected to be a personal access token (PAT).
     */
    fun getGitLabApi(url: URL, username: String?, password: String?): GitLabApi {
        val urlStr = url.toString()
        return if (password.isNullOrBlank()) {
            GitLabApi(urlStr, username)
        } else {
            GitLabApi.oauth2Login(urlStr, username, password, true)
        }
    }
}
