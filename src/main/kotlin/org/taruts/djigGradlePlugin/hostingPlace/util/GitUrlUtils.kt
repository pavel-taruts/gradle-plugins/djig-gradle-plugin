package org.taruts.djigGradlePlugin.hostingPlace.util

import org.taruts.gitUtils.GitRemoteUrlParser

object GitUrlUtils {

    /**
     * Extracts the path part from a project clone URL and removes the **.git** at the end.
     */
    fun getProjectUrlPathWithoutFileExtension(urlStr: String): String {
        val urlPath: String = GitRemoteUrlParser.extractPath(urlStr)
        return urlPath.replace("""\.git$""".toRegex(), "")
    }
}
