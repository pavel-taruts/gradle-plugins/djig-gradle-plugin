package org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer

import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.replacement.PathCharacterPosition
import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.replacement.PathCharacterRange
import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.replacement.Replacement

/**
 * See [transformPath].
 */
class PathTransformer<PathSegment> {

    /**
     * Transforms [path] performing replacements described in [expression].
     *
     * [path] and the return value are lists of [PathSegment], which can be any type.
     *
     * [parse] and [format] parameters must be supplied to transform such lists to [String] and back.
     *
     * &nbsp;
     *
     * [expression] is an Ant style pattern, but with additional syntax of describing replacements.
     *
     * The replacement format is
     * (**part-to-replace**>**substitute**).
     *
     * Where
     *
     * **part-to-replace** - a part of the template, describing the part of the input path to be replaced.
     * As this is a part of the template, it has the Ant style format.
     *
     * **substitute** - the string that must replace **part-to-replace**.
     * It can include **@**, which is a placeholder for the matched fragment of the input path.
     *
     * The >**substitute** part is optional.
     * If a replacement doesn't have it, then the substitute is the empty string
     * and so the matched fragments will be just removed from the path.
     *
     * See [PathTransformerTest.kt](https://gitlab.com/pavel-taruts/gradle-plugins/djig-gradle-plugin/-/blob/master/src/test/kotlin/org/taruts/djigGradlePlugin/tasks/gitHostingProjectCopier/util/pathTransformer/PathTransformerTest.kt)
     * for examples.
     */
    fun transformPath(
        path: List<PathSegment>,
        expression: String,
        parse: (str: String) -> List<PathSegment>,
        format: (segments: List<PathSegment>) -> String
    ): List<PathSegment> {
        val (expressionPathPattern, replacements) = ExpressionParser.parseExpression(expression)

        val (pathAfterMatching: List<PathSegment>, segmentMatches: List<SegmentMatch>?) = match(path, expressionPathPattern, parse)

        if (segmentMatches == null) {
            error("Failed to match path against $expression")
        }

        calculateReplacementPositionsInPath(segmentMatches, replacements, expression)

        return applyReplacements(pathAfterMatching, replacements, parse, format)
    }

    private fun match(
        path: List<PathSegment>,
        expressionPathPattern: List<String>,
        parse: (str: String) -> List<PathSegment>
    ): Pair<List<PathSegment>, List<SegmentMatch>?> {
        val segmentMatches: List<SegmentMatch> = matchInternal(path, expressionPathPattern) ?: return path to null

        val newPath = mutableListOf<PathSegment>()

        var i = 0
        segmentMatches.forEach { segmentMatch ->
            segmentMatch.from = i

            if (segmentMatch.pathSegments.isEmpty()) {
                val listWithEmptySegment = parse("")
                segmentMatch.pathSegments = listWithEmptySegment
            }

            newPath.addAll(segmentMatch.pathSegments)

            i += segmentMatch.pathSegments.size
            segmentMatch.to = i
        }

        if (expressionPathPattern.isNotEmpty() && segmentMatches.isEmpty()) {
            // It's ok. It means match didn't happen
        } else if (expressionPathPattern.size != segmentMatches.size) {
            error("Expression elements number = ${expressionPathPattern.size}, matches number = ${segmentMatches.size}. They must be equal")
        }

        return newPath to segmentMatches
    }

    private fun matchInternal(path: List<PathSegment>, expressionPathPattern: List<String>): List<SegmentMatch>? {

        if (expressionPathPattern.isEmpty()) {
            return null
        }

        val expressionPathPatternSegment = expressionPathPattern[0]

        val minCurrentMatchSize: Int
        val maxCurrentMatchSize: Int

        if (expressionPathPatternSegment.isBlank()) {
            minCurrentMatchSize = 0
            maxCurrentMatchSize = 0
        } else if (expressionPathPatternSegment == "**") {
            minCurrentMatchSize = 0
            maxCurrentMatchSize = path.size
        } else if (expressionPathPatternSegment.contains("**")) {
            throw IllegalArgumentException(
                "Incorrect expression path pattern segment [$expressionPathPatternSegment]. " +
                        "** cannot be mixed with other characters in one path segment"
            )
        } else if (!isNormalPathPatternSegment(expressionPathPatternSegment)) {
            throw IllegalArgumentException(
                "Incorrect expression path pattern segment [$expressionPathPatternSegment]. " +
                        "The only allowed characters are: " +
                        "latin letters, digits and symbols *?._-"
            )
        } else {

            if (path.isEmpty()) {
                return null
            }

            val currentPathSegment: PathSegment = path[0]
            val regex = pathPatternSegmentToRegex(expressionPathPatternSegment)
            if (!currentPathSegment.toString().matches(regex)) {
                return null
            }

            minCurrentMatchSize = 1
            maxCurrentMatchSize = 1
        }

        val furtherExpressionPathPattern = expressionPathPattern.subList(1, expressionPathPattern.size)

        for (currentMatchSize in minCurrentMatchSize..maxCurrentMatchSize) {
            val currentMatch = SegmentMatch(
                expressionPathPatternSegment = expressionPathPatternSegment,
                pathSegments = path.subList(0, currentMatchSize),
            )

            val furtherPath = path.subList(currentMatchSize, path.size)

            if (furtherExpressionPathPattern.isEmpty() && furtherPath.isEmpty()) {
                return listOf(currentMatch)
            } else {
                val furtherMatches: List<SegmentMatch>? = matchInternal(furtherPath, furtherExpressionPathPattern)
                if (furtherMatches != null) {
                    return (sequenceOf(currentMatch) + furtherMatches.asSequence()).toList()
                }
            }
        }

        return null
    }

    private fun calculateReplacementPositionsInPath(
        segmentMatches: List<SegmentMatch>,
        replacements: List<Replacement>,
        expression: String
    ) {
        replacements.forEach { replacement ->
            val startInPath = getPositionInPath(
                segmentMatches = segmentMatches,
                positionInExpressionPathPattern = replacement.inExpressionPathPattern.start!!,
                expression = expression,
                positionExpressionString = replacement.inExpressionString.start
            )
            val endInPath = getPositionInPath(
                segmentMatches = segmentMatches,
                positionInExpressionPathPattern = replacement.inExpressionPathPattern.end!!,
                expression = expression,
                positionExpressionString = replacement.inExpressionString.end
            )
            replacement.inInputPath = PathCharacterRange(start = startInPath, end = endInPath)
        }
    }

    private fun getPositionInPath(
        segmentMatches: List<SegmentMatch>,
        positionInExpressionPathPattern: PathCharacterPosition,
        expression: String,
        positionExpressionString: Int
    ): PathCharacterPosition {
        val pathSegmentIndex: Int
        val positionInPathSegment: Int

        val segmentMatch: SegmentMatch = segmentMatches[positionInExpressionPathPattern.segment]

        if (positionInExpressionPathPattern.character == 0) {
            pathSegmentIndex = segmentMatch.from
            positionInPathSegment = 0
        } else if (positionInExpressionPathPattern.character == segmentMatch.expressionPathPatternSegment!!.length) {
            pathSegmentIndex = segmentMatch.to - 1
            positionInPathSegment = segmentMatch.pathSegments.last().toString().length
        } else if (!isNormalPathPatternSegment(segmentMatch.expressionPathPatternSegment)) {
            error(
                "In expression $expression '${expression[positionExpressionString]}' ($positionExpressionString) " +
                        "sits in a segment ${segmentMatch.expressionPathPatternSegment}. " +
                        "In segments like this a parenthesis can only be either in the beginning or in the end."
            )
        } else {
            // The parenthesis is somewhere in the middle of the "normal" (not ** and not empty) expression element

            pathSegmentIndex = segmentMatch.from

            val expressionSegmentWithEmptyGroup =
                StringBuilder(segmentMatch.expressionPathPatternSegment).insert(positionInExpressionPathPattern.character, "()").toString()
            val regex: Regex = pathPatternSegmentToRegex(expressionSegmentWithEmptyGroup)

            val pathSegment: PathSegment = segmentMatch.pathSegments.single()
            val pathSegmentStr: String = pathSegment.toString()
            val matchResult: MatchResult = regex.matchEntire(pathSegmentStr) ?: error("Match expected here")
            val matchGroup: MatchGroup = matchResult.groups[1]!!
            positionInPathSegment = matchGroup.range.first
        }

        return PathCharacterPosition(segment = pathSegmentIndex, character = positionInPathSegment)
    }

    private fun isNormalPathPatternSegment(pathPatternSegment: String): Boolean {
        return pathPatternSegment.matches("""[\w-*?.]*""".toRegex())
    }

    private fun pathPatternSegmentToRegex(pathPatternSegment: String) = pathPatternSegment
        .replace(".", "\\.")
        .replace("*", ".*")
        .replace("?", ".?")
        .toRegex()

    private fun applyReplacements(
        pathAfterMatching: List<PathSegment>,
        replacements: List<Replacement>,
        parse: (str: String) -> List<PathSegment>,
        format: (segments: List<PathSegment>) -> String
    ): List<PathSegment> {
        val pathToReturn = pathAfterMatching.toMutableList()
        replacements.asReversed().forEach { replacement ->
            applyReplacement(pathToReturn, replacement, parse, format)
        }
        return pathToReturn.filter { it.toString().isNotBlank() }
    }

    private fun applyReplacement(
        pathToReturn: MutableList<PathSegment>,
        replacement: Replacement,
        parse: (str: String) -> List<PathSegment>,
        format: (segments: List<PathSegment>) -> String
    ) {
        val inInputPath = replacement.inInputPath!!
        val startInInputPath = inInputPath.start!!
        val endInInputPath = inInputPath.end!!

        val pathSegmentsToInsertStr: String = if (startInInputPath.segment == endInInputPath.segment) {
            val pathSegment: PathSegment = pathToReturn[startInInputPath.segment]
            val pathSegmentStr: String = pathSegment.toString()

            val stringToReplace = pathSegmentStr.substring(startInInputPath.character, endInInputPath.character)
            val substitute = replacement.substitute.replace("@", stringToReplace)

            StringBuilder(pathSegmentStr)
                .replace(startInInputPath.character, endInInputPath.character, substitute)
                .toString()
        } else {
            val startPathSegment: PathSegment = pathToReturn[startInInputPath.segment]
            val startPathSegmentStr: String = startPathSegment.toString()
            val startPathSegmentUntouchedPart: String = startPathSegmentStr.substring(0, startInInputPath.character)
            val startPathSegmentTouchedPart: String = startPathSegmentStr.substring(startInInputPath.character)

            val endPathSegment: PathSegment = pathToReturn[endInInputPath.segment]
            val endPathSegmentStr: String = endPathSegment.toString()
            val endPathSegmentTouchedPart: String = endPathSegmentStr.substring(0, endInInputPath.character)
            val endPathSegmentUntouchedPart: String = endPathSegmentStr.substring(endInInputPath.character)

            val list1 = if (startPathSegmentTouchedPart.isNotBlank()) {
                parse(startPathSegmentTouchedPart)
            } else {
                listOf()
            }
            val list2 = pathToReturn.subList(startInInputPath.segment + 1, endInInputPath.segment)
            val list3 = if (endPathSegmentTouchedPart.isNotBlank()) {
                parse(endPathSegmentTouchedPart)
            } else {
                listOf()
            }
            val stringToReplace = format(listOf(list1, list2, list3).flatten())

            val substitute = replacement.substitute.replace("@", stringToReplace)

            startPathSegmentUntouchedPart + substitute + endPathSegmentUntouchedPart
        }

        val pathSegmentsToInsert: List<PathSegment> = parse(pathSegmentsToInsertStr)
        repeat(endInInputPath.segment - startInInputPath.segment + 1) {
            pathToReturn.removeAt(startInInputPath.segment)
        }
        pathToReturn.addAll(startInInputPath.segment, pathSegmentsToInsert)
    }

    inner class SegmentMatch(
        val expressionPathPatternSegment: String?,
        var pathSegments: List<PathSegment>,
    ) {
        var from: Int = -1
        var to: Int = -1
        private val path: String = pathSegments.joinToString("/")

        override fun toString(): String {
            return "$expressionPathPatternSegment -> $path"
        }
    }
}
