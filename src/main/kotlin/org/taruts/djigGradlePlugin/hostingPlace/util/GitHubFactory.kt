package org.taruts.djigGradlePlugin.hostingPlace.util

import org.apache.commons.lang3.StringUtils
import org.kohsuke.github.GitHub
import org.kohsuke.github.GitHubBuilder
import org.taruts.djigGradlePlugin.hostingPlace.util.GitHubFactory.getGitHub

/**
 * @see getGitHub
 */
object GitHubFactory {

    /**
     * Gets an authenticated GitHub API by credentials.
     *
     * If both [username] and [password] are specified, then the login-password authentication is used.
     *
     * If there is only [username] then it's expected to be a personal access token (PAT).
     * In theory, it should also work with a OAuth2 token in [username], but we don't use OAuth2 in this project.
     */
    fun getGitHub(username: String?, password: String?): GitHub {
        if (StringUtils.isBlank(username)) {
            throw IllegalArgumentException("username must be specified")
        }

        return if (StringUtils.isBlank(password))
            GitHubBuilder().withOAuthToken(username).build()
        else
            GitHubBuilder().withPassword(username, password).build()
    }
}
