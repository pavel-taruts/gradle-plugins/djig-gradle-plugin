package org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer

import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.replacement.PathCharacterPosition
import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.replacement.Replacement
import java.util.regex.Matcher
import java.util.regex.Pattern

object ExpressionParser {

    private val pattern: Pattern = """$|/|\(|(?:>(?<substitute>[^)]*))?\)""".toPattern()

    /**
     * Parses [expression] separating the path pattern of [expression] from its **replacements**.
     *
     * The return value is a pair.
     *
     * The first pair member is the path pattern of the expression, the list of the path pattern elements.
     *
     * The second pair member is the list of [Replacement] objects each modelling a replacement in the expression.
     *
     * The path pattern of an expression is the expression where every replacement is replaced with the path fragment it wants to replace.
     *
     * That is, if the expression is one/(two>AAA)/th?ee/f*, then its path pattern is one/two/th?ee/f*.
     *
     * For the expression syntax see [PathTransformer.transformPath].
     *
     * @see Replacement
     */
    fun parseExpression(expression: String): Pair<List<String>, List<Replacement>> {
        val matcher: Matcher = pattern.matcher(expression)
        val sb = StringBuilder()

        var currentReplacement: Replacement? = null
        val replacements = mutableListOf<Replacement>()

        val expressionSegments = mutableListOf<String>()

        while (matcher.find()) {

            val match = matcher.group()
            val firstIndex = matcher.start()
            val lastIndex = matcher.end() - 1

            matcher.appendReplacement(sb, "")

            if (match == "/" || match == "") {
                expressionSegments += sb.toString()
                sb.clear()

                if (match == "") {
                    currentReplacement?.run {
                        if (inExpressionString.end == -1) {
                            error("'(' without the corresponding ')' in expression $expression at the index ${inExpressionString.start}")
                        }
                    }
                }
            } else if (match == "(") {
                if (currentReplacement != null) {
                    error("Nested '(' in expression $expression at the index $firstIndex")
                }
                currentReplacement = Replacement().apply {
                    inExpressionString.start = firstIndex
                    inExpressionPathPattern.start = PathCharacterPosition(segment = expressionSegments.size, character = sb.length)
                    replacements += this
                }
            } else if (match.lastOrNull() == ')') {
                if (currentReplacement == null) {
                    error("')' without the corresponding '(' in expression $expression at the index $lastIndex")
                }
                currentReplacement.run {
                    inExpressionString.end = matcher.end() - 1
                    inExpressionPathPattern.end = PathCharacterPosition(segment = expressionSegments.size, character = sb.length)
                    substitute = matcher.group("substitute") ?: ""
                    if (match.first() == '>') {
                        substituteMarkerInExpressionString = firstIndex
                    }
                }
                currentReplacement = null
            }
        }

        return expressionSegments to replacements
    }
}
