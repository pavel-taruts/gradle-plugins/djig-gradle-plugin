package org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.replacement

/**
 * A position of a character in a sequence of string elements.
 */
class PathCharacterPosition(
    val segment: Int,
    val character: Int
)
