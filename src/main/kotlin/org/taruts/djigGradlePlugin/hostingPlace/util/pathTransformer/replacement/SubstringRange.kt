package org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.replacement

/**
 * The positions of a range in a string.
 */
class SubstringRange {
    /**
     * The first character index.
     */
    var start: Int = -1

    /**
     * The last character index (the **inclusive** margin).
     */
    var end: Int = -1
}
