package org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.replacement

/**
 * The positions of a range in a sequence of string elements (a path).
 */
class PathCharacterRange(

    /**
     * The position of the first character
     */
    var start: PathCharacterPosition?,

    /**
     * The position of the character after the last character (the **exclusive** margin).
     *
     * This is without shifting the [PathCharacterPosition.segment] to the next segment if the last character of the range is the last one in its segment.
     *
     * In other words, [PathCharacterPosition.segment] is **inclusive** while [PathCharacterPosition.character] is **exclusive**.
     */
    var end: PathCharacterPosition?
)
