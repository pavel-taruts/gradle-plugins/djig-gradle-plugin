package org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.replacement

import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.ExpressionParser
import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.PathTransformer

/**
 * The model of a replacement clause in an expression.
 *
 * For what expression is and its format see [PathTransformer.transformPath].
 *
 * @see ExpressionParser
 */
class Replacement {

    /**
     * The positions of the replacement in the expression string.
     */
    val inExpressionString = SubstringRange()

    /**
     * The positions of the replacement in the path pattern of the expression.
     *
     * For what the path pattern of an expression is, see [ExpressionParser].
     */
    val inExpressionPathPattern: PathCharacterRange = PathCharacterRange(
        start = null,
        end = null,
    )

    /**
     * The positions of the replacement in the input path.
     */
    var inInputPath: PathCharacterRange? = null

    /**
     * The position of the ">" symbol of the replacement in the expression string.
     */
    var substituteMarkerInExpressionString: Int = -1

    /**
     * The substitute string of the replacement.
     */
    var substitute: String = ""
}
