package org.taruts.djigGradlePlugin.prefixes

import org.taruts.djigGradlePlugin.DjigPlugin
import org.taruts.djigGradlePlugin.credentials.impl.GradleProjectPropertiesCredentialsProvider
import org.taruts.djigGradlePlugin.prefixes.coordinates.ProjectCoordinates
import org.taruts.djigGradlePlugin.targetBranch.impl.GradleProjectPropertiesTargetBranchProvider

/**
 * # In short
 *
 * The Gradle property prefixes allow us to have just one Gradle property with e.g. a GitHub password used for several dynamic projects,
 * rather than having an individual Gradle property with the same value for each project.
 *
 * **There are different types of dynamic project groups that you can have a common Gradle property for.
 * A prefix is what identifies a group like that.**
 *
 * A Gradle property name then would be &lt;group prefix&gt;.&lt;variable&gt;,
 * where &lt;variable&gt; identifies a variable the plugin needs in order to work with a dynamic project, like **username**, **password** or **branch**.
 *
 * # Why store values in Gradle
 *
 * For some values that the plugin needs in order to work with dynamic projects it makes sense not to have them in the source code.
 * First of all it's secrets that you don't want to be seen by anyone who has access to the source code.
 * Also, it's values that might be different among the team members.
 *
 * Let's take GitHub credentials for example. They might be both secret and unique among the team members.
 *
 * Another example is a personal branch name of a team member. It's not a secret, but it's unique for this team member.
 *
 * Gradle properties can be such a place external to the source code where you can have such values.
 *
 * # Prefixes
 *
 * When the plugin wants, say, a GitHub password for a dynamic project it's working with,
 * there are more than one Gradle property the plugin can get the password from.
 *
 * The properties all end with ".password" but have different prefixes.
 *
 * A prefix identifies a group of dynamic projects the property sets password for.
 *
 * The prefixes allow us to specify passwords and other values once for a group of dynamic projects,
 * rather than for each project individually.
 *
 * One example of a group like that is all dynamic projects from all djig applications sharing the same Gradle groupId "my-group".
 *
 * # Example
 *
 * Let's say we have a djig application.
 *
 * It's a project in a [workspace](https://plugins.gradle.org/plugin/org.taruts.workspace) as every djig application is supposed to be,
 * and the workspace also uses the djig plugin.
 *
 * The project name of the djig application is **my-app** and the group ID is **my-group**.
 *
 * In the **dev** profile of the djig application there is a dynamic project named **my-dynamic-project** being stored at
 * https://github.com/my-group/my-dynamic-project-dev.git
 *
 * A djig Gradle task was launched and wants to copy the dynamic project to the local profile of the djig application.
 *
 * Then for the password to access the dynamic project the plugin can use the following Gradle properties:
 *
 * **djig.github-com.password** - for all dynamic projects on GitHub
 *
 * **djig.github-com.my-group.password** - for all dynamic projects on GitHub from djig apps with my-group groupId
 *
 * **djig.github-com.my-group.my-app.password** - for all dynamic projects of our djig app stored on github.com
 *
 * **djig.github-com.my-group.my-app.dev.password** - for all dynamic projects in the dev profile of our djig app which are stored on github.com
 *
 * **djig.github-com.my-group.my-app.dev.my-dynamic-project.password** - individually for my-dynamic-project
 *
 * &nbsp;
 *
 * **More specific property names (longer ones) have higher priority.**
 *
 * # Alternative group ID
 *
 * There is also a means to join several djig applications together in an **alternative group**
 * and set password and other values for all dynamic projects from these djig applications.
 *
 * To make it work you'll need to come up with an **alternative group ID**.
 *
 * It must be a string of characters different from the group ID of any djig application your team will probably come across.
 *
 * Then you'll need to specify it in the constructor of [GradleProjectPropertiesCredentialsProvider] or [GradleProjectPropertiesTargetBranchProvider].
 *
 * In fact, it's not the djig applications you join together in the alternative group,
 * but the providers, which are those who actually read Gradle properties.
 *
 * # See the source code below
 *
 * In the source code below you can find out what prefixes there are and what is the precedence.
 * @see GradleProjectPropertiesCredentialsProvider
 * @see GradleProjectPropertiesTargetBranchProvider
 */
class GradleProjectPropertyPrefixesProvider(private val appCoordinates: ProjectCoordinates) {

    fun getPrefixes(hostingName: String, springBootProfile: String, dynamicProjectName: String?, alternativeGroupId: String?): List<String> {
        val (groupId, gradleProjectName) = appCoordinates

        return listOfNotNull(
            "${DjigPlugin.DJIG_NAME}.$hostingName.$groupId.$gradleProjectName.$springBootProfile.$dynamicProjectName",
            "${DjigPlugin.DJIG_NAME}.$hostingName.$groupId.$gradleProjectName.$springBootProfile",
            "${DjigPlugin.DJIG_NAME}.$hostingName.$groupId.$gradleProjectName",
            alternativeGroupId?.let {
                "${DjigPlugin.DJIG_NAME}.$hostingName.$alternativeGroupId"
            },
            "${DjigPlugin.DJIG_NAME}.$hostingName.$groupId",
            "${DjigPlugin.DJIG_NAME}.$hostingName",
        )
    }
}
