package org.taruts.djigGradlePlugin.prefixes.coordinates

import org.taruts.gradleUtils.GradleBuilder
import java.io.File

object GradleProjectCoordinatesFactory {

    /**
     * Gets the group and name for a Gradle project.
     */
    fun get(projectDirectory: File): ProjectCoordinates {
        val projectProperties: MutableMap<String, String> = GradleBuilder.getProperties(projectDirectory)
        val groupId = projectProperties["group"]!!
        val gradleProjectName = projectProperties["name"]!!
        return ProjectCoordinates(groupId, gradleProjectName)
    }
}
