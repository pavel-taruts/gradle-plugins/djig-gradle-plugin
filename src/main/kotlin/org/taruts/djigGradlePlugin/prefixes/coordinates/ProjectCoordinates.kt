package org.taruts.djigGradlePlugin.prefixes.coordinates

/**
 * The group and name of a Gradle project.
 */
class ProjectCoordinates(val group: String, val name: String) {
    operator fun component1() = group
    operator fun component2() = name
}
