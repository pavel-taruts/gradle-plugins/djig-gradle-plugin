package org.taruts.djigGradlePlugin.propertyWriting

/**
 * See [serialize].
 */
class PropertyBlocksSerializer {

    /**
     * Serializes [propertyBlocks] into a string in the format implied by [fileExtension].
     *
     * The file extension can be "properties", "yaml" or "yml".
     *
     * The property blocks will be separated by empty lines between them.
     */
    fun serialize(
        propertyBlocks: List<Map<String, Any?>>,
        fileExtension: String
    ): String {
        val allPropertiesStr: String = propertyBlocks
            .asSequence()
            .filter { propertyBlock -> propertyBlock.isNotEmpty() }
            .map { propertyBlock ->
                propertyBlock.map { (name, value) ->
                    formatPropertyAssignment(name, value, fileExtension)
                }.joinToString("\n")
            }
            .joinToString("\n\n")
        return allPropertiesStr
    }

    private fun formatPropertyAssignment(name: String, value: Any?, fileExtension: String): String {
        val valueStr: String = PropertyWritingUtils.toStringOrEmpty(value)

        val operator: String = when (fileExtension) {
            "properties" -> "="
            "yaml", "yml" -> ": "
            else -> throw IllegalArgumentException("Unexpected fileExtension $fileExtension")
        }

        return "$name$operator$valueStr"
    }
}
