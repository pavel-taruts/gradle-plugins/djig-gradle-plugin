package org.taruts.djigGradlePlugin.propertyWriting

import java.io.File

/**
 * See [write].
 */
class PropertyBlocksWriter(
    private val environmentPropertyBlocksWriter: EnvironmentPropertyBlocksWriter,
    private val filePropertyBlocksWriter: FilePropertyBlocksWriter
) {

    /**
     * Saves the blocks of properties in [propertyBlocks] to [file] if it's specified or to the environment variables otherwise.
     */
    fun write(file: File?, propertyBlocks: List<Map<String, String>>) {
        if (file == null) {
            environmentPropertyBlocksWriter.write(propertyBlocks)
        } else {
            filePropertyBlocksWriter.write(file, propertyBlocks)
        }
    }
}
