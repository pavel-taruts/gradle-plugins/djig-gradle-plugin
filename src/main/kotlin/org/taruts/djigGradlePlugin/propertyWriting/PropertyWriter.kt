package org.taruts.djigGradlePlugin.propertyWriting

import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djigGradlePlugin.propertyWriting.enums.PropertyType
import org.taruts.djigGradlePlugin.propertyWriting.strategies.BranchPropertyWriteStrategy
import org.taruts.djigGradlePlugin.propertyWriting.strategies.CredentialsPropertyWriteStrategy
import org.taruts.djigGradlePlugin.propertyWriting.strategies.ProjectPropertyWriteStrategy
import org.taruts.djigGradlePlugin.propertyWriting.strategies.PropertyWriteStrategy
import org.taruts.gitUtils.hosting.HostingNameExtractor
import java.io.File

/**
 * See [writeTargetProperties].
 */
class PropertyWriter(
    private val projectPropertyWriteStrategy: ProjectPropertyWriteStrategy,
    private val branchPropertyWriteStrategy: BranchPropertyWriteStrategy,
    private val credentialsPropertyWriteStrategy: CredentialsPropertyWriteStrategy,
    private val propertyBlocksWriter: PropertyBlocksWriter
) {
    /**
     * Saves all properties of all target dynamic projects to the target profile, to whatever places all those kinds of properties are configured to be saved.
     *
     * Thus, the djig application launched with the target profile will read those properties and will work with that list of dynamic projects.
     *
     * Some properties of the projects can be saved [PropertyType.PER_HOSTING] or as [PropertyType.COMMON] rather than [PropertyType.PER_PROJECT].
     * This is defined by the [PropertyWriteStrategy]s
     * ([CredentialsPropertyWriteStrategy], [BranchPropertyWriteStrategy], [ProjectPropertyWriteStrategy]),
     * as well as the exact places where the properties are saved.
     * See those [PropertyWriteStrategy] implementations for more detail.
     */
    fun writeTargetProperties(targetDynamicProjectsMap: Map<String, DjigConfigurationProperties.DynamicProject>) {
        val propertyWriteStrategiesByFiles: Map<File?, List<PropertyWriteStrategy>> = getPropertyWriteStrategiesByFiles()
        val hostingNames: List<String> = getHostingNames(targetDynamicProjectsMap)
        propertyWriteStrategiesByFiles.forEach { (file: File?, filePropertyWriteStrategies: List<PropertyWriteStrategy>) ->
            val filePropertyBlocks: List<Map<String, String>> = buildFilePropertyBlocks(filePropertyWriteStrategies, hostingNames, targetDynamicProjectsMap)
            propertyBlocksWriter.write(file, filePropertyBlocks)
        }
    }

    private fun getPropertyWriteStrategiesByFiles(): Map<File?, List<PropertyWriteStrategy>> {
        // Filter only those PropertyWriteStrategies that are turned on by the user in the extension
        val activePropertyWriteStrategies: List<PropertyWriteStrategy> =
            sequenceOf(projectPropertyWriteStrategy, branchPropertyWriteStrategy, credentialsPropertyWriteStrategy)
                .filter { it.getPropertyType() != null }
                .toList()

        // Each PropertyWriteStrategy is configured to write to a particular file. Group the PropertyWriter by their files.
        return activePropertyWriteStrategies.groupBy { it.getFile() }
    }

    private fun getHostingNames(targetDynamicProjectsMap: Map<String, DjigConfigurationProperties.DynamicProject>): List<String> {
        return targetDynamicProjectsMap
            .values
            .map { HostingNameExtractor.extractHostingName(it.url) }
            .distinct()
    }

    private fun buildFilePropertyBlocks(
        filePropertyWriteStrategies: List<PropertyWriteStrategy>,
        hostingNames: List<String>,
        targetDynamicProjectsMap: Map<String, DjigConfigurationProperties.DynamicProject>
    ): List<Map<String, String>> {
        val filePropertyBlocks = mutableListOf<Map<String, String>>()
        addCommonPropertyBlock(filePropertyBlocks, filePropertyWriteStrategies, targetDynamicProjectsMap)
        addPerHostingPropertyBlocks(filePropertyBlocks, filePropertyWriteStrategies, targetDynamicProjectsMap, hostingNames)
        addPerProjectPropertyBlocks(filePropertyBlocks, filePropertyWriteStrategies, targetDynamicProjectsMap)
        return filePropertyBlocks
    }

    private fun addCommonPropertyBlock(
        filePropertyBlocks: MutableList<Map<String, String>>,
        filePropertyWriteStrategies: List<PropertyWriteStrategy>,
        targetDynamicProjectsMap: Map<String, DjigConfigurationProperties.DynamicProject>
    ) {
        val commonFilePropertyWriteStrategies: List<PropertyWriteStrategy> = filePropertyWriteStrategies.filter {
            it.getPropertyType() == PropertyType.COMMON
        }
        val commonPropertyBlock: Map<String, String> = commonFilePropertyWriteStrategies
            .flatMap { propertyWriteStrategy ->
                val commonProperties: List<Pair<String, String>> = getCommonProperties(propertyWriteStrategy, targetDynamicProjectsMap)
                commonProperties
            }
            .toMap()
        filePropertyBlocks += commonPropertyBlock
    }

    private fun addPerHostingPropertyBlocks(
        filePropertyBlocks: MutableList<Map<String, String>>,
        filePropertyWriteStrategies: List<PropertyWriteStrategy>,
        targetDynamicProjectsMap: Map<String, DjigConfigurationProperties.DynamicProject>,
        hostingNames: List<String>
    ) {
        val perHostingFilePropertyWriteStrategies: List<PropertyWriteStrategy> = filePropertyWriteStrategies.filter {
            it.getPropertyType() == PropertyType.PER_HOSTING
        }
        hostingNames.forEach { hostingName ->
            val hostingPropertyBlock: Map<String, String> = perHostingFilePropertyWriteStrategies.flatMap { propertyWriteStrategy ->
                val hostingProperties: List<Pair<String, String>> = getHostingProperties(propertyWriteStrategy, targetDynamicProjectsMap, hostingName)
                hostingProperties
            }.toMap()
            filePropertyBlocks += hostingPropertyBlock
        }
    }

    private fun addPerProjectPropertyBlocks(
        filePropertyBlocks: MutableList<Map<String, String>>,
        filePropertyWriteStrategies: List<PropertyWriteStrategy>,
        targetDynamicProjectsMap: Map<String, DjigConfigurationProperties.DynamicProject>
    ) {
        val individualFilePropertyWriteStrategies: List<PropertyWriteStrategy> = filePropertyWriteStrategies.filter {
            it.getPropertyType() == PropertyType.PER_PROJECT
        }
        targetDynamicProjectsMap.forEach { (projectName, projectProperties) ->
            val projectIndividualPropertiesBlock: Map<String, String> = individualFilePropertyWriteStrategies
                .flatMap { propertyWriteStrategy ->
                    val projectIndividualProperties: List<Pair<String, String>> = getProjectIndividualProperties(propertyWriteStrategy, projectName, projectProperties)
                    projectIndividualProperties
                }
                .toMap()
            filePropertyBlocks += projectIndividualPropertiesBlock
        }
    }

    private fun getCommonProperties(
        propertyWriteStrategy: PropertyWriteStrategy,
        targetDynamicProjectsMap: Map<String, DjigConfigurationProperties.DynamicProject>
    ): List<Pair<String, String>> {
        val propertyCombinationToProjectNames: Map<ShortPropertyMap, List<String>> =
            getPropertyCombinationToProjectNamesMap(propertyWriteStrategy, targetDynamicProjectsMap)

        ensureSinglePropertyCombination(
            "Cannot write target project properties ${propertyWriteStrategy.properties.joinToString()} as common properties, " +
                    "because they are different for some projects.",
            propertyCombinationToProjectNames
        )

        return if (propertyCombinationToProjectNames.isEmpty()) {
            listOf()
        } else {
            val shortPropertyMap: ShortPropertyMap = propertyCombinationToProjectNames.keys.single()
            shortPropertyMap.toCommonProperties()
        }
    }

    private fun getHostingProperties(
        propertyWriteStrategy: PropertyWriteStrategy,
        targetDynamicProjectsMap: Map<String, DjigConfigurationProperties.DynamicProject>,
        hostingName: String
    ): List<Pair<String, String>> {
        val hostingDynamicProjects: Map<String, DjigConfigurationProperties.DynamicProject> = getHostingDynamicProjects(targetDynamicProjectsMap, hostingName)

        val propertyCombinationToProjectNames: Map<ShortPropertyMap, List<String>> =
            getPropertyCombinationToProjectNamesMap(propertyWriteStrategy, hostingDynamicProjects)

        ensureSinglePropertyCombination(
            "Cannot write target project properties ${propertyWriteStrategy.properties.joinToString()} as common properties for $hostingName, " +
                    "because they are different for some projects.",
            propertyCombinationToProjectNames
        )

        return if (propertyCombinationToProjectNames.isEmpty()) {
            listOf()
        } else {
            val shortPropertyMap: ShortPropertyMap = propertyCombinationToProjectNames.keys.single()
            shortPropertyMap.toHostingProperties(hostingName)
        }
    }

    private fun getProjectIndividualProperties(
        propertyWriteStrategy: PropertyWriteStrategy,
        projectName: String,
        targetProjectProperties: DjigConfigurationProperties.DynamicProject
    ): List<Pair<String, String>> {
        val shortPropertyMap: ShortPropertyMap = getShortPropertyMap(propertyWriteStrategy, targetProjectProperties)
        return shortPropertyMap.toProjectProperties(projectName)
    }

    private fun getPropertyCombinationToProjectNamesMap(
        propertyWriteStrategy: PropertyWriteStrategy,
        targetDynamicProjectsMap: Map<String, DjigConfigurationProperties.DynamicProject>
    ): Map<ShortPropertyMap, List<String>> {
        return targetDynamicProjectsMap
            .entries
            .groupBy(
                { getShortPropertyMap(propertyWriteStrategy, it.value) },
                { it.key }
            )
    }

    private fun ensureSinglePropertyCombination(errorMessageStart: String, propertyValueCombinationToProjectNames: Map<ShortPropertyMap, List<String>>) {
        val propertyValueCombinations: List<ShortPropertyMap> = propertyValueCombinationToProjectNames.keys.toList()
        if (propertyValueCombinations.size > 1) {

            val combination1: ShortPropertyMap = propertyValueCombinations[0]
            val projects1: List<String> = propertyValueCombinationToProjectNames[combination1]!!

            val combination2: ShortPropertyMap = propertyValueCombinations[1]
            val projects2: List<String> = propertyValueCombinationToProjectNames[combination2]!!

            val (diffView1, diffView2) = combination1.diff(combination2)

            error(
                errorMessageStart + " " +
                        "For project${if (projects1.size == 1) "" else "s"} ${projects1.joinToString()} it's $diffView1. " +
                        "For project${if (projects2.size == 1) "" else "s"} ${projects2.joinToString()} it's $diffView2."
            )
        }
    }

    private fun getHostingDynamicProjects(
        targetDynamicProjectsMap: Map<String, DjigConfigurationProperties.DynamicProject>,
        hostingName: String
    ): Map<String, DjigConfigurationProperties.DynamicProject> {
        val dynamicProjectsByHostings: Map<String, Map<String, DjigConfigurationProperties.DynamicProject>> = getDynamicProjectsByHostings(targetDynamicProjectsMap)
        return dynamicProjectsByHostings[hostingName]!!
    }

    private fun getDynamicProjectsByHostings(targetDynamicProjectsMap: Map<String, DjigConfigurationProperties.DynamicProject>): Map<String, Map<String, DjigConfigurationProperties.DynamicProject>> {
        val dynamicProjectsByHostings: Map<String, Map<String, DjigConfigurationProperties.DynamicProject>> = targetDynamicProjectsMap
            .map { it.key to it.value }
            .groupBy {
                HostingNameExtractor.extractHostingName(it.second.url)
            }
            .mapValues { (_, projectNameToProjectPropertiesPairs) ->
                projectNameToProjectPropertiesPairs.toMap()
            }
        return dynamicProjectsByHostings
    }

    private fun getShortPropertyMap(propertyWriteStrategy: PropertyWriteStrategy, targetDynamicProject: DjigConfigurationProperties.DynamicProject): ShortPropertyMap {
        return ShortPropertyMap(targetDynamicProject, propertyWriteStrategy.properties)
    }

    private fun ShortPropertyMap.toCommonProperties(): List<Pair<String, String>> {
        return withPrefix("djig.common")
    }

    private fun ShortPropertyMap.toHostingProperties(hostingName: String): List<Pair<String, String>> {
        return withPrefix("djig.hostings.$hostingName")
    }

    private fun ShortPropertyMap.toProjectProperties(projectName: String): List<Pair<String, String>> {
        return withPrefix("djig.dynamicProjects.$projectName")
    }

    private fun ShortPropertyMap.withPrefix(prefix: String): List<Pair<String, String>> {
        return map.mapNotNull { (name, value) ->
            if (value == null) null else "$prefix.$name" to value
        }
    }
}
