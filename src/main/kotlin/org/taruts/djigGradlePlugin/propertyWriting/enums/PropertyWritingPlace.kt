package org.taruts.djigGradlePlugin.propertyWriting.enums

/**
 * The kind of the place in general to write the variables to.
 */
enum class PropertyWritingPlace {
    /**
     * The variables must be written as OS environment variables, that can be interpreted as Spring Boot configuration properties by a djig application.
     *
     * Note, that if not overridden they'll be visible by a djig application no matter the profiles it uses.
     */
    ENVIRONMENT_VARIABLES,

    /**
     * The variables must be written to a Spring Boot configuration property file on the classpath of the djig application or to an external one.
     */
    APP_PROPERTIES_RESOURCE,

    /**
     * The variables must not be written anywhere (perhaps because they are already in place).
     */
    NOWHERE,
}
