package org.taruts.djigGradlePlugin.propertyWriting.enums

import org.taruts.djig.configurationProperties.DjigConfigurationProperties

/**
 * There are three kinds of Spring Boot configuration properties that a variable for working with a dynamic project can be taken from.
 *
 * If, for example, the project named *foo* sits on [github.com](https://github.com)
 * then the branch name the project must be taken from, can be taken from these properties:
 * - djig.common.branch
 * - djig.hostings.github-com.branch
 * - djig.dynamic-projects.foo.branch
 *
 * So, the types of properties that we talk about are:
 * - djig.common.*
 * - djig.hostings.**hosting-name**.*
 * - djig.dynamic-projects.**project-name**.*
 *
 * @see DjigConfigurationProperties
 */
enum class PropertyType {

    /**
     * The variables must be written as
     * djig.dynamic-projects.**project-name**.*
     */
    PER_PROJECT,

    /**
     * The variables must be written as
     * djig.hostings.**hosting-name**.*
     */
    PER_HOSTING,

    /**
     * The variables must be written as
     * djig.common.*
     */
    COMMON,
}
