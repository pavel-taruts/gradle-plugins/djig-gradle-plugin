package org.taruts.djigGradlePlugin.propertyWriting.utils

import org.apache.commons.io.FileUtils
import org.springframework.context.ApplicationContext
import org.springframework.core.io.ClassPathResource
import org.springframework.core.io.FileUrlResource
import org.springframework.core.io.Resource
import java.io.File

/**
 * Utils for searching for Spring Boot configuration property files to write target properties to.
 * See the docs on the methods.
 */
class LocationUtils(private val appClassPathResourcesDirectory: File, private val appWorkingDirectory: File) {

    private val possibleExtensions = listOf("properties", "yml", "yaml")
    private val yamlExtensions = listOf("yml", "yaml")

    /**
     * Gets a Spring Boot configuration property file for profile [springBootProfile]
     * of a djig application the Spring Boot context of which is [applicationContext].
     *
     * The search is performed according to the following properties in the context:
     * - spring.config.location
     * - spring.config.additional-location
     * - spring.config.name
     *
     * Also the return value of [configLocationFunction]
     * can be used either to put a filter on the standard locations (built from the properties mentioned above)
     * or to override them.
     *
     * Return value of [configLocationFunction] must be formatted as a Spring location (i.e. prepended with either classpath: or file: and other things).
     *
     * The following is how different return values of [configLocationFunction] are treated:
     *
     * - **null** - the standard locations will be used for the search without any filtering
     *
     * - **file**: or **classpath:** - the standard locations are filtered by either the **file:** or **classpath:** prefix respectively
     *
     * - A **directory** (a string ending with "/" or "\") - the only location for the property file would be this directory.
     * The spring.config.name property and [springBootProfile] will still be used for the names of files to be searched in this directory
     *
     * - A **file without an extension** - it will search for this file with extensions "properties", "yaml" or "yml".
     * If multiple files are found it'll choose the one with [preferredFileExtension] if it exists.
     * If not then it'll use one of the existing files.
     * The priority is first "properties" then "yaml" then "yml"
     *
     * - A **file with extension** - it's the exact path of the file to be found or created
     *
     * If there is no such a file, it's created in the first location in the following ways:
     *
     * - If the first location is a **directory**, the file name will be standard for Spring Boot, which is
     * &lt;spring.config.name&gt;-&lt;[springBootProfile]&gt;.&lt;[preferredFileExtension]&gt;.
     *
     * - If the first location is a **file without an extension**, then [preferredFileExtension] will be used.
     *
     * - If the first location is a **file with an extension**, then this extension will be used, and [preferredFileExtension] will be ignored.
     */
    fun getPropertyFile(
        applicationContext: ApplicationContext,
        springBootProfile: String,
        preferredFileExtension: String,
        configLocationFunction: ((configName: String, profile: String) -> String)?
    ): File {
        val defaultConfigLocation: String = applicationContext.environment.getProperty("spring.config.location")!!
        val additionalConfigLocation: String? = applicationContext.environment.getProperty("spring.config.additional-location")
        val configName: String = applicationContext.environment.getProperty("spring.config.name") ?: "application"

        val defaultConfigLocations = defaultConfigLocation.split(",", ";")
        val additionalConfigLocations = additionalConfigLocation?.split(",", ";") ?: listOf()
        var locations = defaultConfigLocations + additionalConfigLocations

        locations = locations
            .map {
                it.replace("^optional:".toRegex(), "")
            }

        var configLocationSetting: String? = if (configLocationFunction != null)
            configLocationFunction(configName, springBootProfile)
        else null

        if (configLocationSetting != null) {
            validateConfigLocationSetting(configLocationSetting)
            configLocationSetting = fixConfigLocationSetting(configLocationSetting)
        }

        val possibleExtensions: List<String> = getPossibleExtensions(preferredFileExtension)

        if (configLocationSetting == null) {
            // The list configDirectoryTemplates remains as it is
        } else if (configLocationSetting.endsWith(":")) {
            locations = locations.filter {
                it.startsWith(configLocationSetting)
            }
        } else if (isLocationFileWithoutExtension(configLocationSetting)) {
            locations = possibleExtensions.map { extension -> "$configLocationSetting.$extension" }
        } else {
            locations = listOf(configLocationSetting)
        }

        return getFileForLocations(
            applicationContext,
            locations,
            configName,
            springBootProfile,
            possibleExtensions
        )
    }

    private fun validateConfigLocationSetting(configLocationSetting: String) {
        val prefixOk = configLocationSetting.startsWith("classpath:") || configLocationSetting.startsWith("file:")
        if (!prefixOk) {
            error("Bad location $configLocationSetting. It must start with classpath: or file:")
        }
    }

    /**
     * In [configLocationSetting] replaces the dot representing the current directory (the working directory)
     * with the absolute path of the working directory of the djig application.
     *
     * We do it because in this process the "." will mean the working directory of launching Gradle,
     * rather than the working directory of the djig application.
     */
    private fun fixConfigLocationSetting(configLocationSetting: String): String {
        // We use this marker because the regex override of CharSequence.replace
        // treats the substitute as string using a special syntax for regex substitutes rather than just a string.
        // In our case if we don't use the marker it would lead to losing all backslashes from the substitute.
        val marker = "<<<HERE!!!>>>"
        return configLocationSetting
            .replace("""(?<=^file:).(?=[/\\])""".toRegex(), marker)
            .replace(marker, appWorkingDirectory.canonicalPath)
    }

    /**
     * Gets possible file extensions in the order of priority.
     * It's this method that defines the priority of extensions, i.e. the order in which files are searched in a location.
     * First goes [preferredExtension]. If [possibleExtensions] is on of the YAML extensions (.yaml or .yml), then the remaining YAML extension goes next.
     * Then go the remaining extensions
     */
    private fun getPossibleExtensions(preferredExtension: String): List<String> {

        val list: MutableList<String> = mutableListOf(preferredExtension)

        val isYaml: Boolean = yamlExtensions.contains(preferredExtension)
        if (isYaml) {
            list += yamlExtensions - list
        }

        list += possibleExtensions - list

        return list
    }

    private fun isLocationFileWithoutExtension(location: String): Boolean {
        return location.contains("""(?:[^\w\-.]|^)[\w\-]++$""".toRegex())
    }

    /**
     * Searches for a Spring Boot configuration property file in [locations].
     * If no files are found it returns a [File] to be created.
     *
     * [locations] are searched in **reverse** order, because it's expected that the bottom has the highest priority, and so we go from the bottom.
     * This is the same way Spring Boot treats location lists in properties spring.config.location and spring.config.additional-location.
     * In Spring Boot the last location of spring.config.additional-location has the highest priority.
     *
     * [locations] can contain both file and directory locations.
     *
     * For directory locations [configName], [springBootProfile] and [extensions] are used to build file names.
     *
     * The first element of [extensions] is considered the **preferred** extension for the new file if no existing files are found.
     */
    private fun getFileForLocations(
        applicationContext: ApplicationContext,
        locations: List<String>,
        configName: String,
        springBootProfile: String,
        extensions: List<String>
    ): File {
        if (locations.isEmpty()) {
            error("locations list should not be empty")
        }

        val firstFile: File? = locations
            .reversed()
            .flatMap { location ->
                findFilesForLocation(applicationContext, location, configName, springBootProfile, extensions)
            }
            .firstOrNull()

        if (firstFile != null) {
            // There were files. We return the first one
            return firstFile
        }

        // No files were found

        // The lowest priority location is probably the shortest and most obvious one, like file:./
        val lowestPriorityLocation: String = locations
            .filterNot {
                it.contains('*')
            }
            .first()

        val isDirectory: Boolean = isLocationDirectory(lowestPriorityLocation)
        return if (isDirectory) {
            val preferredFileExtension: String = extensions.first()
            getLocationFile(applicationContext, "$lowestPriorityLocation$configName-$springBootProfile.$preferredFileExtension")
        } else {
            getLocationFile(applicationContext, lowestPriorityLocation)
        }
    }

    /**
     * Searches files for [location].
     * It just searches, without creating files.
     * [location] can be a file or directory location.
     * If it's a directory location, then [configName], [springBootProfile] and [possibleExtensions] are used to build possible file names.
     */
    private fun findFilesForLocation(
        applicationContext: ApplicationContext,
        location: String,
        configName: String,
        springBootProfile: String,
        possibleExtensions: List<String>
    ): List<File> {
        val isDirectory: Boolean = isLocationDirectory(location)
        val existingFilesForLocation: List<File> = if (isDirectory) {
            findFilesForDirectoryLocation(applicationContext, location, configName, springBootProfile, possibleExtensions)
        } else {
            findFilesForFileLocation(applicationContext, location)
        }
        return existingFilesForLocation
    }

    private fun isLocationDirectory(location: String): Boolean {
        val lastCharacter = location.last()
        return lastCharacter == '/' || lastCharacter == '\\'
    }

    /**
     * Searches files for [directoryLocation].
     * It just searches, without creating files.
     * [configName], [springBootProfile] and [possibleExtensions] are used to build possible file names.
     */
    private fun findFilesForDirectoryLocation(
        applicationContext: ApplicationContext,
        directoryLocation: String,
        configName: String,
        springBootProfile: String,
        possibleExtensions: List<String>
    ): List<File> {
        val locationWithoutExtension = "$directoryLocation$configName-$springBootProfile."
        val existingFiles: List<File> = possibleExtensions
            .map { fileExtension ->
                "$locationWithoutExtension$fileExtension"
            }
            .flatMap { resourceTemplate ->
                findFilesForFileLocation(applicationContext, resourceTemplate)
            }
        return existingFiles
    }

    /**
     * Searches files for [fileLocation].
     * It just searches, without creating files.
     */
    private fun findFilesForFileLocation(applicationContext: ApplicationContext, fileLocation: String): List<File> {
        val resources: Array<Resource> = try {
            applicationContext.getResources(fileLocation)
        } catch (e: java.io.FileNotFoundException) {
            // Some parent directory in the path was not found
            return listOf()
        }
        if (resources.isEmpty()) {
            return listOf()
        }

        return resources
            .asSequence()
            .filter { resource ->
                resource.exists() && resource.isFile
            }
            .map { resource ->
                resource.file
            }
            .toList()
    }

    /**
     * Gets the [File] denoted by [fileLocation].
     * It doesn't have to exist, it is not created right away, but it is returned to be created somewhere in the future.
     */
    private fun getLocationFile(
        applicationContext: ApplicationContext,
        fileLocation: String
    ): File {
        return if (fileLocation.contains("*")) {
            error("Cannot create file for location $fileLocation because the location contains *")
        } else {
            when (val resource: Resource = applicationContext.getResource(fileLocation)) {
                is FileUrlResource -> resource.file
                is ClassPathResource -> FileUtils.getFile(appClassPathResourcesDirectory, resource.path).canonicalFile
                else -> error("Unexpected resource class ${resource::class}")
            }
        }
    }
}
