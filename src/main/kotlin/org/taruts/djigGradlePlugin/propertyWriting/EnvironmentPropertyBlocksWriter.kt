package org.taruts.djigGradlePlugin.propertyWriting

import org.apache.commons.lang3.SystemUtils
import org.apache.commons.text.StringEscapeUtils
import org.taruts.djigGradlePlugin.DjigPlugin
import org.taruts.processUtils.ProcessRunner

/**
 * See [write].
 */
class EnvironmentPropertyBlocksWriter(private val propertyBlocksSerializer: PropertyBlocksSerializer) {

    /**
     * Saves [propertyBlocks] to the environment variables.
     *
     * Note that **for Linux it doesn't save anything** but only prints out what the user themselves needs to save to whatever place is appropriate.
     * This is because of the huge variety of ways of how environment variables are saved in different distributions.
     * See the message text in the code below.
     */
    fun write(propertyBlocks: List<Map<String, Any?>>) {
        val environmentVariableBlocks: List<Map<String, String>> = propertyBlocks.map { propertyBlock ->
            propertyBlock.map { (propertyName, propertyValue) ->
                environmentVariableName(propertyName) to PropertyWritingUtils.toStringOrEmpty(propertyValue)
            }.toMap()
        }

        if (SystemUtils.IS_OS_WINDOWS) {
            val environmentVariables: Map<String, String> = environmentVariableBlocks
                .flatMap { environmentVariableBlock ->
                    environmentVariableBlock.map { (key, value) ->
                        key to value
                    }
                }
                .toMap()

            environmentVariables.forEach { (name, value) ->
                ProcessRunner.runProcess(null, "setx", name, value)
            }
            println("\n\n")
            println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            println(
                "Depending on how you launch your ${DjigPlugin.DJIG_NAME} application, " +
                        "you might need to restart your OS, IDE, Jetbrains Toolbox and/or other software " +
                        "for the new environment variable values to apply"
            )
            println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            println("\n\n")
        } else {
            val nixEnvironmentVariableBlocks = environmentVariableBlocks.map { environmentVariableBlock ->
                environmentVariableBlock.map { (name, value) ->
                    name to "\"${StringEscapeUtils.escapeJava(value)}\""
                }.toMap()
            }
            val propertyBlocksStr: String = propertyBlocksSerializer.serialize(nixEnvironmentVariableBlocks, "properties")

            println("\n\n")
            println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            println("Please, manually add these environment variables to your system.")
            println("")
            println(propertyBlocksStr)
            println("")
            println("This is done via adding them to a file like those in the list:")
            println("/etc/environment")
            println("/etc/environment.d/*.conf")
            println("/etc/profile")
            println("/etc/profile.d/*.sh")
            println("~/.bash_profile")
            println("~/.bash_login")
            println("~/.profile")
            println("Which one is better depends on your distribution and the way you're going to run you djig application in it.")
            println("Don't forget to restart your system afterwards.")
            println("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!")
            println("\n\n")
        }
    }

    private fun environmentVariableName(propertyName: String): String {
        return propertyName
            .replace(".", "_")
            .replace("-", "_")
            .uppercase()
    }
}
