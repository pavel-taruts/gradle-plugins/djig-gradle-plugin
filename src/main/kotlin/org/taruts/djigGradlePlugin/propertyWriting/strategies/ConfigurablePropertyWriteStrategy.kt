package org.taruts.djigGradlePlugin.propertyWriting.strategies

import org.springframework.context.ApplicationContext
import org.taruts.djigGradlePlugin.extension.DjigPluginExtension
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription.Target.PropertyWriteStrategyConfiguration
import org.taruts.djigGradlePlugin.propertyWriting.enums.PropertyType
import org.taruts.djigGradlePlugin.propertyWriting.enums.PropertyWritingPlace
import org.taruts.djigGradlePlugin.propertyWriting.utils.LocationUtils
import java.io.File

/**
 * The base class for a [PropertyWriteStrategy] implementation that gets its settings from [DjigPluginExtension] or, to be more precise,
 * from a [PropertyWriteStrategyConfiguration] object from the [TaskDescription].
 *
 * See the subclasses for more detail.
 *
 * @see CredentialsPropertyWriteStrategy
 * @see BranchPropertyWriteStrategy
 */
abstract class ConfigurablePropertyWriteStrategy(
    protected val taskDescription: TaskDescription,
    protected val settings: PropertyWriteStrategyConfiguration,
    private val locationUtils: LocationUtils,
    private val targetSpringBootProfile: String,
    private val targetApplicationContext: ApplicationContext
) : PropertyWriteStrategy {
    protected var place: PropertyWritingPlace? = null

    /**
     * Should have value only if [place] is [PropertyWritingPlace.APP_PROPERTIES_RESOURCE], [PropertyWritingPlace.ENVIRONMENT_VARIABLES].
     * Should be null when [place] is [PropertyWritingPlace.NOWHERE].
     */
    private var propertyTypeInternal: PropertyType? = null

    override fun getPropertyType() = propertyTypeInternal

    /**
     * Should have value only if [place] is [PropertyWritingPlace.APP_PROPERTIES_RESOURCE].
     */
    private var location: ((configName: String, profile: String) -> String)? = null

    init {
        place = settings.place.orNull
        location = settings.location.orNull
        propertyTypeInternal = settings.propertyType.orNull
        validateAndSetDefaults()
    }

    protected open fun validateAndSetDefaults() {
        validatePlaceAndSetDefault()
        validatePropertyTypeAndSetDefault()
        validateLocationAndSetDefault()
    }

    override fun getFile(): File? {
        return if (location == null) {
            null
        } else {
            locationUtils.getPropertyFile(
                applicationContext = targetApplicationContext,
                springBootProfile = targetSpringBootProfile,
                preferredFileExtension = taskDescription.target.preferredFileExtension.get(),
                configLocationFunction = location!!
            )
        }
    }

    protected open fun validatePlaceAndSetDefault() {
        if (place == null) {
            error("Properties writing place must be set")
        }
    }

    private fun validatePropertyTypeAndSetDefault() {
        // If we save credentials to NOWHERE then it's incorrect to specify properties to save credentials to.
        if (place == PropertyWritingPlace.NOWHERE) {
            if (propertyTypeInternal != null) {
                error(
                    "Incorrect propertyType $propertyTypeInternal " +
                            "where the place to write the properties is ${settings.place.orNull}. " +
                            "The propertyType must be null in this situation."
                )
            }
        } else {
            if (propertyTypeInternal == null) {
                error(
                    "When the place to write properties is ${settings.place.orNull}, " +
                            "propertyType must be specified"
                )
            }
        }
    }

    private fun validateLocationAndSetDefault() {
        if (place == PropertyWritingPlace.APP_PROPERTIES_RESOURCE) {
            if (location == null) {
                location = { _, _ -> "classpath:" }
            }
        } else {
            if (location != null) {
                throw IllegalStateException(
                    "Properties write location != null where the place to write is $place. " +
                            "Non-null values are only allowed " +
                            "when the place is ${PropertyWritingPlace.APP_PROPERTIES_RESOURCE}."
                )
            }
        }
    }
}
