package org.taruts.djigGradlePlugin.propertyWriting.strategies

import org.springframework.context.ApplicationContext
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription.Target.PropertyWriteStrategyConfiguration
import org.taruts.djigGradlePlugin.propertyWriting.enums.PropertyType
import org.taruts.djigGradlePlugin.propertyWriting.utils.LocationUtils
import java.io.File

/**
 * The [PropertyWriteStrategy] implementation managing the writing of the "normal" properties of target dynamic projects.
 * I.e. url, dynamicInterfacePackage and buildType.
 * They are "normal" as opposed to [BranchPropertyWriteStrategy] and [CredentialsPropertyWriteStrategy] that may want to write properties outside
 * the codebase, to an "external" place, like external Spring Boot configuration property files or to the environment variables.
 *
 * When [TaskDescription.Target.sourceProjectBranchMode] is true then the user is allowed not to specify [PropertyWriteStrategyConfiguration.place].
 * In this case the branches will be written together with the normal dynamic project properties.
 */
class ProjectPropertyWriteStrategy(
    private val taskDescription: TaskDescription,
    private val locationUtils: LocationUtils,
    private val targetSpringBootProfile: String,
    private val targetApplicationContext: ApplicationContext
) : PropertyWriteStrategy {
    override val properties: List<String> = listOf(
        "url",
        "dynamicInterfacePackage",
        "buildType",
    )

    override fun getPropertyType() = PropertyType.PER_PROJECT

    override fun getFile(): File {
        val locationFunction: (configName: String, profile: String) -> String =
            taskDescription.target.projectPropertyWriteStrategy.location.get()
        return locationUtils.getPropertyFile(
            applicationContext = targetApplicationContext,
            springBootProfile = targetSpringBootProfile,
            preferredFileExtension = taskDescription.target.preferredFileExtension.get(),
            configLocationFunction = locationFunction
        )
    }
}
