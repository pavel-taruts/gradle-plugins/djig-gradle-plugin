package org.taruts.djigGradlePlugin.propertyWriting.strategies

import org.taruts.djigGradlePlugin.propertyWriting.enums.PropertyType
import java.io.File

/**
 * The interface of a strategy of writing target project properties.
 * A strategy must define the properties that it manages and some details of how and where the properties must be written.
 */
interface PropertyWriteStrategy {
    /**
     * The property names this strategy applies for.
     */
    val properties: List<String>

    /**
     * The [PropertyType] this strategy wants to be used when writing the properties.
     *
     * **null** means that the properties should not be written anywhere,
     * the writing of the properties is disabled by the user.
     */
    fun getPropertyType(): PropertyType?

    /**
     * The file this strategy wants the properties to be written to.
     *
     * **null** means the properties must be written not a file but to the environment variables.
     */
    fun getFile(): File?
}
