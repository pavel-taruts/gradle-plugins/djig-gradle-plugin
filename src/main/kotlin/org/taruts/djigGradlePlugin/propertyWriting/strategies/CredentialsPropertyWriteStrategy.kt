package org.taruts.djigGradlePlugin.propertyWriting.strategies

import org.gradle.api.logging.Logger
import org.springframework.context.ApplicationContext
import org.taruts.djigGradlePlugin.credentials.CredentialsProvider
import org.taruts.djigGradlePlugin.credentials.impl.ApplicationPropertiesCredentialsProvider
import org.taruts.djigGradlePlugin.extension.Visibility
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.djigGradlePlugin.propertyWriting.enums.PropertyWritingPlace
import org.taruts.djigGradlePlugin.propertyWriting.utils.LocationUtils

/**
 * The [PropertyWriteStrategy] implementation managing the writing of the **username** and **password** properties of target dynamic projects.
 */
class CredentialsPropertyWriteStrategy(
    private val logger: Logger,
    taskDescription: TaskDescription,
    locationUtils: LocationUtils,
    targetSpringBootProfile: String,
    targetApplicationContext: ApplicationContext,
    private val targetCredentialsProvider: CredentialsProvider
) : ConfigurablePropertyWriteStrategy(
    taskDescription,
    taskDescription.target.credentialsPropertyWriteStrategy,
    locationUtils,
    targetSpringBootProfile,
    targetApplicationContext
) {
    override val properties: List<String> = listOf("username", "password")

    override fun validatePlaceAndSetDefault() {
        if (targetCredentialsProvider is ApplicationPropertiesCredentialsProvider) {
            if (place == null) {
                place = PropertyWritingPlace.NOWHERE
            } else if (place != PropertyWritingPlace.NOWHERE) {
                error(
                    "Target credentials writing place is ${settings.place.orNull} " +
                            "where target hosting credentials provider is ${ApplicationPropertiesCredentialsProvider::class.simpleName}. " +
                            "The place here should be ${PropertyWritingPlace.NOWHERE} or unset. "
                )
            }
        }

        // For PUBLIC projects credentialsWritePropertyPlace has default of NOWHERE. Other values are kind of exotic and lead to a warning.
        val visibility = taskDescription.target.visibility.get()
        if (visibility == Visibility.PUBLIC) {
            if (place == null) {
                place = PropertyWritingPlace.NOWHERE
            } else if (place != PropertyWritingPlace.NOWHERE) {
                logger.warn(
                    "Target credentials writing place is ${settings.place.orNull} " +
                            "where target project visibility is $visibility. " +
                            "For PUBLIC target projects this is kind of exotic. " +
                            "You might want it to be ${PropertyWritingPlace.NOWHERE} instead."
                )
            }
        } else {
            // For PRIVATE projects credentialsWritePropertyPlace has no default and must be specified.
            if (place == null) {
                error("Target credentials writing place must be set")
            }
        }

        super.validatePlaceAndSetDefault()
    }
}
