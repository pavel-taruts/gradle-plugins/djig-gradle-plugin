package org.taruts.djigGradlePlugin.propertyWriting.strategies

import org.springframework.context.ApplicationContext
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription.Target.PropertyWriteStrategyConfiguration
import org.taruts.djigGradlePlugin.propertyWriting.enums.PropertyType
import org.taruts.djigGradlePlugin.propertyWriting.utils.LocationUtils
import java.io.File

/**
 * The [PropertyWriteStrategy] implementation managing the writing of the **branch** property of target dynamic projects.
 *
 * When [TaskDescription.Target.sourceProjectBranchMode] is true then the user is allowed not to specify [PropertyWriteStrategyConfiguration.place].
 * In this case the branches will be written together with the normal dynamic project properties.
 */
class BranchPropertyWriteStrategy(
    taskDescription: TaskDescription,
    private val projectPropertyWriteStrategy: ProjectPropertyWriteStrategy,
    locationUtils: LocationUtils,
    private val sourceProjectBranchMode: Boolean,
    targetSpringBootProfile: String,
    targetApplicationContext: ApplicationContext
) : ConfigurablePropertyWriteStrategy(
    taskDescription,
    taskDescription.target.branchPropertyWriteStrategy,
    locationUtils,
    targetSpringBootProfile,
    targetApplicationContext
) {
    override val properties: List<String> = listOf("branch")

    override fun validateAndSetDefaults() {
        if (delegateToProjectWriter()) {
            return
        } else {
            super.validateAndSetDefaults()
        }
    }

    override fun getFile(): File? {
        return if (delegateToProjectWriter()) {
            projectPropertyWriteStrategy.getFile()
        } else {
            super.getFile()
        }
    }

    override fun getPropertyType(): PropertyType? {
        return if (delegateToProjectWriter()) {
            projectPropertyWriteStrategy.getPropertyType()
        } else {
            super.getPropertyType()
        }
    }

    private fun delegateToProjectWriter(): Boolean {
        return !sourceProjectBranchMode && !isConfigured()
    }

    private fun isConfigured(): Boolean {
        return place != null
    }
}
