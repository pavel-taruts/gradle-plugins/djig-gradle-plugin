package org.taruts.djigGradlePlugin.propertyWriting

object PropertyWritingUtils {

    /**
     * Gets the string representation of [propertyValue].
     *
     * It's its [Object.toString] if value is specified and the empty string otherwise.
     */
    fun toStringOrEmpty(propertyValue: Any?) = propertyValue?.toString() ?: ""
}
