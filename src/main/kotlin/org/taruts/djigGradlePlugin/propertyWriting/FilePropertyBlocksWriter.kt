package org.taruts.djigGradlePlugin.propertyWriting

import org.taruts.djigGradlePlugin.DjigPlugin
import org.taruts.propertyFileSectionUtils.PropertiesFileSectionUtils
import java.io.File

/**
 * See [write].
 */
class FilePropertyBlocksWriter(private val propertyBlocksSerializer: PropertyBlocksSerializer) {

    /**
     * Saves [propertyBlocks] to [file].
     *
     * The blocks will be separated by an empty line and all of them as a whole will be enclosed in the brackets of **## section djig {** and **## }**.
     */
    fun write(file: File, propertyBlocks: List<Map<String, Any?>>) {
        val propertyBlocksStr: String = propertyBlocksSerializer.serialize(propertyBlocks, file.extension)
        PropertiesFileSectionUtils.replaceSectionContentsOrAppend(
            file = file,
            sectionName = DjigPlugin.DJIG_NAME,
            newContents = propertyBlocksStr
        )
    }
}
