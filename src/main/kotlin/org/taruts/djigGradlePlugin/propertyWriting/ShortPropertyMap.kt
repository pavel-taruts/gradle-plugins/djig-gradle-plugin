package org.taruts.djigGradlePlugin.propertyWriting

import org.apache.commons.lang3.builder.HashCodeBuilder
import org.springframework.beans.BeanWrapperImpl
import org.springframework.boot.context.properties.bind.DataObjectPropertyName
import org.taruts.djig.configurationProperties.DjigConfigurationProperties.DynamicProject

/**
 * This represents a combination of property values that a group of target dynamic projects share.
 *
 * The names of properties used here are those of [DynamicProject],
 * the short ones,
 * not the long ones that will be in files or environment variables.
 *
 * This is why the **Short** in the class name.
 */
class ShortPropertyMap(val map: Map<String, String?>) {

    override fun equals(other: Any?): Boolean {
        if (other === this) {
            return true
        }
        if (other === null || other !is ShortPropertyMap || map.size != other.map.size) {
            return false
        }
        return map.all { (propertyName, propertyValue) ->
            other.map[propertyName] == propertyValue
        }
    }

    override fun hashCode(): Int {
        val builder = HashCodeBuilder()
        map
            .entries
            .sortedBy { it.key }
            .forEach {
                builder.append(it.key).append(it.value)
            }
        return builder.toHashCode()
    }

    /**
     * Returns the diff between this [ShortPropertyMap] and [that] as a pair.
     *
     * The first pair element is [ShortPropertyMap] containing only those properties **this** has
     * that are different from **that** or that **that** doesn't have.
     *
     * The second pair element is vice versa.
     * It contains only those properties of **that**, that are different from **this** or that **this** doesn't have.
     */
    fun diff(that: ShortPropertyMap): Pair<ShortPropertyMap, ShortPropertyMap> {
        val diffKeys: List<String> = (this.map.keys.asSequence() + that.map.keys.asSequence())
            .filter {
                this.map[it] != that.map[it]
            }
            .toList()

        val thisDiff: ShortPropertyMap = this.view(diffKeys)
        val thatDiff: ShortPropertyMap = that.view(diffKeys)

        return thisDiff to thatDiff
    }

    /**
     * Returns a [ShortPropertyMap] having only properties with names from [keys] copied from this [ShortPropertyMap].
     */
    private fun view(keys: List<String>): ShortPropertyMap {
        val viewMap: Map<String, String?> = keys
            .asSequence()
            .map { key ->
                key to map[key]
            }
            .toMap()
        return ShortPropertyMap(viewMap)
    }

    override fun toString(): String {
        return map
            .entries
            .asSequence()
            .map {
                val valueRepresentation: String = it.value ?: "<null>"
                "${it.key}=$valueRepresentation"
            }
            .joinToString()
    }
}

/**
 * A factory method for [ShortPropertyMap]. It creates a [ShortPropertyMap] object populating it with [bean] properties with names from [propertyNames].
 *
 * The property names are transformed to the **format-with-dashes**.
 */
fun ShortPropertyMap(bean: Any, propertyNames: List<String>): ShortPropertyMap {
    val wrapper = BeanWrapperImpl(bean)
    val mapInternal = mutableMapOf<String, String?>()
    propertyNames.forEach { propertyName ->
        val propertyValue: Any? = wrapper.getPropertyValue(propertyName)
        val dashedPropertyName: String = DataObjectPropertyName.toDashedForm(propertyName)
        mapInternal[dashedPropertyName] = propertyValue?.toString()
    }
    return ShortPropertyMap(mapInternal)
}
