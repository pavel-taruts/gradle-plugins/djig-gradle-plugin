package org.taruts.djigGradlePlugin.credentials

import org.taruts.djigGradlePlugin.credentials.impl.*

/**
 * The base interface for strategies of getting credentials to access a Git project at its hosting. There are several ways we can get them from.
 * @see ApplicationPropertiesCredentialsProvider
 * @see GitLabContainerCredentialsProvider
 * @see GradleProjectPropertiesCredentialsProvider
 * @see SimpleCredentialsProvider
 * @see SimpleGradleProjectPropertiesCredentialsProvider
 */
interface CredentialsProvider {

    fun getCredentials(
        hostingName: String,

        /**
         * The source or target profile, depending on whether it's a source or target project.
         */
        springBootProfile: String,

        /**
         * The project name (its key in the map) in the djig settings.
         */
        dynamicProjectName: String
    ): Credentials
}
