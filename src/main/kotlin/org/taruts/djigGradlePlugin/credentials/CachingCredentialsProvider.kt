package org.taruts.djigGradlePlugin.credentials

import org.taruts.djigGradlePlugin.InitPersonalDynamicProjectsTask
import org.taruts.djigGradlePlugin.cache.Caches
import org.taruts.djigGradlePlugin.cache.CachingComputationWrapper3

/**
 * The base class for [CredentialsProvider] implementations that want to cache their returned credentials.
 * The caching is performed using all the three parameters of [getCredentials].
 * Invalidation happens when [Caches.clear] is called, which is at the beginning and at the end of execution of [InitPersonalDynamicProjectsTask].
 * This means that for any combination of the parameters of [getCredentials]
 * the calculation of credentials will happen not more than once during an execution of [InitPersonalDynamicProjectsTask].
 */
abstract class CachingCredentialsProvider : CredentialsProvider {

    private val cachedComputeCredentials = CachingComputationWrapper3(::computeCredentials)

    final override fun getCredentials(hostingName: String, springBootProfile: String, dynamicProjectName: String): Credentials {
        return cachedComputeCredentials.computeOrGet(hostingName, springBootProfile, dynamicProjectName)
    }

    abstract fun computeCredentials(
        hostingName: String,
        springBootProfile: String,
        dynamicProjectName: String?
    ): Credentials
}
