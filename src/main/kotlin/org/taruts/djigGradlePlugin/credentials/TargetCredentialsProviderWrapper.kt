package org.taruts.djigGradlePlugin.credentials

import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djigGradlePlugin.extension.tasks.TaskDescription
import org.taruts.gitUtils.hosting.HostingNameExtractor

/**
 * A service returning credentials for target projects using the [CredentialsProvider] the user specified for target projects.
 */
class TargetCredentialsProviderWrapper(
    private val targetCredentialsProvider: CredentialsProvider,
    private val sourceProjectBranchMode: Boolean,
    private val targetSpringBootProfile: String,
    private val taskDescription: TaskDescription
) {

    fun getTargetHostingCredentials(
        projectName: String,
        sourceProjectProperties: DjigConfigurationProperties.DynamicProject
    ): Credentials {
        return targetCredentialsProvider.getCredentials(
            hostingName = extractHostingName(sourceProjectProperties),
            springBootProfile = targetSpringBootProfile,
            dynamicProjectName = projectName
        )
    }

    private fun extractHostingName(sourceProjectProperties: DjigConfigurationProperties.DynamicProject): String {
        val hostingOrProjectUrl: String = if (sourceProjectBranchMode) {
            sourceProjectProperties.url
        } else {
            taskDescription.target.hosting.url.get().toString()
        }
        return HostingNameExtractor.extractHostingName(hostingOrProjectUrl)
    }
}
