package org.taruts.djigGradlePlugin.credentials.impl

import org.apache.commons.lang3.StringUtils
import org.gradle.api.Project
import org.taruts.djigGradlePlugin.credentials.CachingCredentialsProvider
import org.taruts.djigGradlePlugin.credentials.Credentials
import org.taruts.djigGradlePlugin.credentials.CredentialsProvider
import org.taruts.djigGradlePlugin.prefixes.GradleProjectPropertyPrefixesProvider

/**
 * An implementation of [CredentialsProvider] that gets credentials from the Gradle project properties.
 * There are a lot of ways to specify Gradle project properties.
 * One of the most common ways is having them in ~/.gradle/gradle.properties.
 * All the possible credentials property names have the form of:
 *      &lt;prefix&gt;.username and &lt;prefix&gt;.password,
 * where &lt;prefix&gt; is any of those supported by [GradleProjectPropertyPrefixesProvider].
 * The prefixes differ by scope and precedence.
 * @see GradleProjectPropertyPrefixesProvider
 */
class GradleProjectPropertiesCredentialsProvider(
    private val alternativeGroupId: String? = null
) : CachingCredentialsProvider() {

    private lateinit var gradleProject: Project
    private lateinit var gradleProjectPropertyPrefixesProvider: GradleProjectPropertyPrefixesProvider

    fun init(gradleProject: Project, gradleProjectPropertyPrefixesProvider: GradleProjectPropertyPrefixesProvider) {
        this.gradleProject = gradleProject
        this.gradleProjectPropertyPrefixesProvider = gradleProjectPropertyPrefixesProvider
    }

    override fun computeCredentials(
        hostingName: String,
        springBootProfile: String,
        dynamicProjectName: String?
    ): Credentials {
        val prefixes: List<String> = gradleProjectPropertyPrefixesProvider.getPrefixes(hostingName, springBootProfile, dynamicProjectName, alternativeGroupId)

        val workspaceGradleProject = gradleProject

        return prefixes.firstNotNullOf { prefix ->
            val usernamePropertyName = "$prefix.username"
            val passwordPropertyName = "$prefix.password"

            val username = if (workspaceGradleProject.hasProperty(usernamePropertyName))
                StringUtils.trimToNull(workspaceGradleProject.property(usernamePropertyName).toString())
            else null

            val password = if (workspaceGradleProject.hasProperty(passwordPropertyName))
                StringUtils.trimToNull(workspaceGradleProject.property(passwordPropertyName).toString())
            else null

            if (StringUtils.isNotBlank(username)) {
                Credentials(username!!, password)
            } else {
                null
            }
        }
    }
}
