package org.taruts.djigGradlePlugin.credentials.impl

import org.apache.commons.lang3.StringUtils
import org.gradle.api.Project
import org.taruts.djigGradlePlugin.credentials.Credentials
import org.taruts.djigGradlePlugin.credentials.CredentialsProvider

/**
 * An implementation of [CredentialsProvider] returning the set of credentials from the Gradle project properties whose names are specified in the constructor call.
 */
class SimpleGradleProjectPropertiesCredentialsProvider(
    private val usernamePropertyName: String,
    private val passwordPropertyName: String? = null
) : CredentialsProvider {

    private lateinit var gradleProject: Project

    fun init(gradleProject: Project) {
        this.gradleProject = gradleProject
    }

    override fun getCredentials(hostingName: String, springBootProfile: String, dynamicProjectName: String): Credentials {
        val project = gradleProject

        val username = StringUtils.trimToNull(project.property(usernamePropertyName).toString())

        val password = if (passwordPropertyName != null && project.hasProperty(passwordPropertyName))
            StringUtils.trimToNull(project.property(passwordPropertyName).toString())
        else null

        return Credentials(username, password)
    }
}
