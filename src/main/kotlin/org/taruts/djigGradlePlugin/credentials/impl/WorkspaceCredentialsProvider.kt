package org.taruts.djigGradlePlugin.credentials.impl

import org.gradle.api.Project
import org.taruts.djigGradlePlugin.credentials.Credentials
import org.taruts.djigGradlePlugin.credentials.CredentialsProvider
import org.taruts.djigGradlePlugin.utils.ReflectionUtils

/**
 * An implementation of [CredentialsProvider] that gets credentials from the settings of Gradle Plugin org.taruts.workspace.
 */
class WorkspaceCredentialsProvider : CredentialsProvider {

    private lateinit var gradleProject: Project

    fun init(gradleProject: Project) {
        this.gradleProject = gradleProject
    }

    override fun getCredentials(hostingName: String, springBootProfile: String, dynamicProjectName: String): Credentials {

        // We need to interact with an extension of another plugin.
        // We can retrieve it from the project, but we don't have its class in the ClassLoader of our current plugin.
        // Different Gradle plugins are run using separate class loaders.
        // So we access data of the extension of the other plugin via reflection.
        val extension: Any = gradleProject.extensions.getByName("workspace")

        val apiCredentials: Array<String?> = ReflectionUtils.getObjectPropertyValue(extension, "apiCredentials")
        val (username, password) = apiCredentials

        return Credentials(username!!, password)
    }
}
