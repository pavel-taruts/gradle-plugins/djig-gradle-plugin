package org.taruts.djigGradlePlugin.credentials.impl

import org.taruts.djigGradlePlugin.credentials.Credentials
import org.taruts.djigGradlePlugin.credentials.CredentialsProvider

/**
 * An implementation of [CredentialsProvider] returning the set of credentials that is specified in the constructor call.
 */
class SimpleCredentialsProvider(username: String, password: String? = null) : CredentialsProvider {

    private val credentials = Credentials(username, password)

    fun init() {
    }

    override fun getCredentials(hostingName: String, springBootProfile: String, dynamicProjectName: String): Credentials = credentials
}
