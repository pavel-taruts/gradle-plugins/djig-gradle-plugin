package org.taruts.djigGradlePlugin.credentials.impl

import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djig.configurationProperties.utils.DynamicProjectPropertiesDefaultsApplier
import org.taruts.djigGradlePlugin.boot.SpringBootUtils
import org.taruts.djigGradlePlugin.credentials.Credentials
import org.taruts.djigGradlePlugin.credentials.CredentialsProvider

/**
 * An implementation of [CredentialsProvider] that gets credentials from the Spring Boot properties of the djig application,
 * from the profile of the project. The profile is either source or target one, depending on whether it's a source or target project.
 */
class ApplicationPropertiesCredentialsProvider : CredentialsProvider {

    private lateinit var springBootUtils: SpringBootUtils

    fun init(springBootUtils: SpringBootUtils) {
        this.springBootUtils = springBootUtils
    }

    override fun getCredentials(hostingName: String, springBootProfile: String, dynamicProjectName: String): Credentials {

        val djigConfigurationProperties: DjigConfigurationProperties = springBootUtils.getDjigConfigurationPropertiesForTask(springBootProfile)

        val dynamicProjectProperties: DjigConfigurationProperties.DynamicProject =
            DynamicProjectPropertiesDefaultsApplier.applyDefaults(djigConfigurationProperties, dynamicProjectName)

        return Credentials(dynamicProjectProperties.username, dynamicProjectProperties.password)
    }
}
