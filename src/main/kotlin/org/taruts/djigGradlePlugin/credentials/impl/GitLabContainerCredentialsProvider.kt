package org.taruts.djigGradlePlugin.credentials.impl

import org.gradle.api.Project
import org.gradle.api.provider.Property
import org.taruts.djigGradlePlugin.credentials.Credentials
import org.taruts.djigGradlePlugin.credentials.CredentialsProvider
import org.taruts.djigGradlePlugin.utils.ReflectionUtils

/**
 * An implementation of [CredentialsProvider] that gets credentials from the settings of Gradle Plugin gitlab-container.
 * It should be used for target projects when they are to be created at the GitLab instance created locally with the gitlab-container plugin.
 */
class GitLabContainerCredentialsProvider : CredentialsProvider {

    private lateinit var gradleProject: Project

    fun init(gradleProject: Project) {
        this.gradleProject = gradleProject
    }

    override fun getCredentials(hostingName: String, springBootProfile: String, dynamicProjectName: String): Credentials {

        // We need to interact with an extension of another plugin.
        // We can retrieve it from the project, but we don't have its class in the ClassLoader of our current plugin.
        // Different Gradle plugins are run using separate class loaders.
        // So we access data of the extension of the other plugin via reflection.
        val gitLabContainerPluginExtension: Any = gradleProject.extensions.getByName("gitLabContainer")

        val usernameProperty: Property<String> = ReflectionUtils.getObjectPropertyValue(gitLabContainerPluginExtension, "username")
        val username = usernameProperty.get()

        val passwordProperty: Property<String> = ReflectionUtils.getObjectPropertyValue(gitLabContainerPluginExtension, "password")
        val password = passwordProperty.get()

        return Credentials(username, password)
    }
}
