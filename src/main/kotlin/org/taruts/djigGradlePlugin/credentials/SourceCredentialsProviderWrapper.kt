package org.taruts.djigGradlePlugin.credentials

import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.gitUtils.hosting.HostingNameExtractor

/**
 * A service returning credentials for source projects using the [CredentialsProvider] the user specified for source projects.
 */
class SourceCredentialsProviderWrapper(
    private val sourceCredentialsProvider: CredentialsProvider?,
    private val sourceSpringBootProfile: String
) {

    fun getSourceHostingCredentials(projectName: String, sourceProjectProperties: DjigConfigurationProperties.DynamicProject): Credentials? {
        return sourceCredentialsProvider?.getCredentials(
            hostingName = HostingNameExtractor.extractHostingName(sourceProjectProperties.url),
            springBootProfile = sourceSpringBootProfile,
            dynamicProjectName = projectName
        )
    }
}
