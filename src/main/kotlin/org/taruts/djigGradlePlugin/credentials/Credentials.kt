package org.taruts.djigGradlePlugin.credentials

/**
 * Credentials returned by a [CredentialsProvider].
 */
data class Credentials(val username: String, val password: String?)
