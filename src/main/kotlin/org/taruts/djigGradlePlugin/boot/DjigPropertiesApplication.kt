package org.taruts.djigGradlePlugin.boot

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djig.configurationProperties.DjigPropertiesAutoConfiguration

/**
 * This is the Spring Boot application that [SpringBootUtils] launches
 * to get [DjigConfigurationProperties] for a given profile.
 *
 * It is lightweight,
 * the only thing it has from the djig application
 * is the configuration files (both on the classpath and in the working directory)
 * and [DjigConfigurationProperties] built from them.
 *
 * It has minimum beans that a Spring Boot application can have,
 * plus a bean of type [DjigConfigurationProperties].
 *
 * This is thanks to [DjigPropertiesAutoConfiguration]
 * which goes with djig-properties-spring-boot-starter.
 *
 *
 * @see SpringBootUtils
 * @see DjigConfigurationProperties
 * @see DjigPropertiesAutoConfiguration
 */
@SpringBootApplication
open class DjigPropertiesApplication
