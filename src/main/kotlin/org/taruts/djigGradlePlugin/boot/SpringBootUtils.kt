package org.taruts.djigGradlePlugin.boot

import org.springframework.boot.Banner
import org.springframework.boot.WebApplicationType
import org.springframework.boot.builder.SpringApplicationBuilder
import org.springframework.context.ApplicationContext
import org.springframework.core.io.DefaultResourceLoader
import org.springframework.core.io.ResourceLoader
import org.taruts.djig.configurationProperties.DjigConfigurationProperties
import org.taruts.djig.configurationProperties.utils.DynamicProjectPropertiesDefaultsApplier
import org.taruts.djigGradlePlugin.cache.CachingComputationWrapper1
import org.taruts.djigGradlePlugin.extension.DjigPluginExtension
import java.io.File
import java.net.URLClassLoader

/**
 * Utils to get information about the djig application configuration.
 * See the documentation on the methods for more information.
 */
class SpringBootUtils(
    private val extensionApp: DjigPluginExtension.App,
    private val appClassPathResourcesDirectory: File,
    private val appWorkingDirectory: File
) {

    private val cachedLaunchApplicationInternal = CachingComputationWrapper1(this::launchApplicationInternal)

    /**
     * Launches a Spring Boot application [DjigPropertiesApplication]
     * with profile [springBootProfile]
     * and returns the [DjigConfigurationProperties]
     * from the application context.
     *
     * The return values are cached by [springBootProfile].
     *
     * @see DjigPropertiesApplication
     */
    fun getDjigConfigurationPropertiesForTask(springBootProfile: String): DjigConfigurationProperties {
        val applicationContext = launchApplication(springBootProfile)
        return getDjigConfigurationPropertiesFromContext(applicationContext)
    }

    /**
     * Launches a Spring Boot application [DjigPropertiesApplication]
     * with profile [springBootProfile] and returns the context.
     *
     * The return values are cached by [springBootProfile].
     *
     * @see DjigPropertiesApplication
     */
    fun launchApplication(springBootProfile: String): ApplicationContext {
        return cachedLaunchApplicationInternal.computeOrGet(springBootProfile)
    }

    /**
     * The actual calculation of the [ApplicationContext].
     * It's called when there is no [ApplicationContext] in the cache.
     */
    private fun launchApplicationInternal(springBootProfile: String): ApplicationContext {
        val springBootProperties: MutableMap<String, String> = extensionApp.springBootProperties.get()

        val thisClassLoader = javaClass.classLoader
        val bootAppClassLoader = URLClassLoader(
            arrayOf(appClassPathResourcesDirectory.toURI().toURL()),
            thisClassLoader
        )
        val resourceLoader: ResourceLoader = DefaultResourceLoader(bootAppClassLoader)

        val configLocationStr = listOf(
            "optional:classpath:/",
            "optional:classpath:config/",
            "optional:classpath:config/*/",
            "optional:file:${appWorkingDirectory.canonicalPath}/",
            "optional:file:${appWorkingDirectory.canonicalPath}/config/",
            "optional:file:${appWorkingDirectory.canonicalPath}/config/*/",
        ).joinToString(",")
        val localSpringBootProperties: MutableMap<String, Any> = mutableMapOf(
            "spring.config.location" to configLocationStr,
        )
        localSpringBootProperties.putAll(springBootProperties)

        return SpringApplicationBuilder()
            .bannerMode(Banner.Mode.OFF)
            .resourceLoader(resourceLoader)
            .sources(DjigPropertiesApplication::class.java)
            .profiles(
                springBootProfile
            )
            .web(WebApplicationType.NONE)
            .properties(
                localSpringBootProperties
            )
            .build()
            .run()
    }

    /**
     * Extracts the [DjigConfigurationProperties] bean from [ApplicationContext].
     */
    private fun getDjigConfigurationPropertiesFromContext(context: ApplicationContext): DjigConfigurationProperties {
        val djigConfigurationProperties: DjigConfigurationProperties = context.getBean(DjigConfigurationProperties::class.java)
        DynamicProjectPropertiesDefaultsApplier.applyDefaultsForAllProjectsInPlace(djigConfigurationProperties)
        return djigConfigurationProperties
    }
}
