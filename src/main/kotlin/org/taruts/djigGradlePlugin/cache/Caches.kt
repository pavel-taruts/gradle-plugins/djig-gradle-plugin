package org.taruts.djigGradlePlugin.cache

/**
 * The factory and aggregator of the caches for caching computation wrappers [CachingComputationWrapper1], [CachingComputationWrapper2] and [CachingComputationWrapper3].
 *
 * The purpose of it is keeping the caches together to invalidate them all at once with [clear] when needed.
 */
object Caches {

    private val caches = mutableListOf<MutableMap<*, *>>()

    /**
     * Creates a new cache and attaches it to this object.
     *
     * All the values put in the cache will live all the way until [clear] is called.
     */
    fun <K, V> createCache(): MutableMap<K, V> {
        val newCache = mutableMapOf<K, V>()
        caches.add(newCache)
        return newCache
    }

    /**
     * Clears all the caches and detaches them from this object.
     */
    fun clear() {
        caches.forEach { it.clear() }
        caches.clear()
    }
}
