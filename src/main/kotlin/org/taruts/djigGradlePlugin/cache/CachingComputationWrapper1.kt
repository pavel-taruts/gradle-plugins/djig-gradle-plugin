package org.taruts.djigGradlePlugin.cache

/**
 * A caching wrapper for a one parameter computation.
 *
 * The cache is invalidated when [Caches.clear] is called.
 */
open class CachingComputationWrapper1<K1, V>(private val compute: (K1) -> V) {
    private val cache: MutableMap<K1, V> = Caches.createCache()

    /**
     * Retrieves the cached value for argument [k1] or computes it if it's absent.
     */
    fun computeOrGet(k1: K1): V {
        return cache.computeIfAbsent(k1) {
            compute(k1)
        }
    }
}
