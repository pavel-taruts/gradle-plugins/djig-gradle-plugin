package org.taruts.djigGradlePlugin.cache

/**
 * A caching wrapper for a two parameter computation.
 *
 * The cache is invalidated when [Caches.clear] is called.
 */
open class CachingComputationWrapper2<K1, K2, V>(private val compute: (K1, K2) -> V) {
    private val cache: MutableMap<Pair<K1, K2>, V> = Caches.createCache()

    /**
     * Retrieves the cached value for arguments [k1] and [k2] or computes it if it's absent.
     */
    fun computeOrGet(k1: K1, k2: K2): V {
        val key = Pair(k1, k2)
        return cache.computeIfAbsent(key) {
            compute(k1, k2)
        }
    }
}
