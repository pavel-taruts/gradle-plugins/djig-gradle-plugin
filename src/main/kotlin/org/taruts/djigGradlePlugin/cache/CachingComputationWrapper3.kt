package org.taruts.djigGradlePlugin.cache

/**
 * A caching wrapper for a three parameter computation.
 *
 * The cache is invalidated when [Caches.clear] is called.
 */
open class CachingComputationWrapper3<K1, K2, K3, V>(private val compute: (K1, K2, K3) -> V) {
    private val cache: MutableMap<Triple<K1, K2, K3>, V> = Caches.createCache()

    /**
     * Retrieves the cached value for arguments [k1], [k2] and [k3] or computes it if it's absent.
     */
    fun computeOrGet(k1: K1, k2: K2, k3: K3): V {
        val key = Triple(k1, k2, k3)
        return cache.computeIfAbsent(key) {
            compute(k1, k2, k3)
        }
    }
}
