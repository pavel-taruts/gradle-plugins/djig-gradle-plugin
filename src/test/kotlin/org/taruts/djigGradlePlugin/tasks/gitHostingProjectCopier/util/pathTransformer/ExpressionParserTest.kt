package org.taruts.djigGradlePlugin.tasks.gitHostingProjectCopier.util.pathTransformer

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.ExpressionParser
import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.replacement.Replacement

internal class ExpressionParserTest {

    @Test
    fun test01() {
        val (expressionSegments, _) = ExpressionParser.parseExpression(
            "one/two/three/four/five/six/seven/eight/nine/ten"
        )

        Assertions.assertEquals("one", expressionSegments[0])
        Assertions.assertEquals("two", expressionSegments[1])
        Assertions.assertEquals("three", expressionSegments[2])
        Assertions.assertEquals("four", expressionSegments[3])
        Assertions.assertEquals("five", expressionSegments[4])
        Assertions.assertEquals("six", expressionSegments[5])
        Assertions.assertEquals("seven", expressionSegments[6])
        Assertions.assertEquals("eight", expressionSegments[7])
        Assertions.assertEquals("nine", expressionSegments[8])
        Assertions.assertEquals("ten", expressionSegments[9])
    }

    @Test
    fun test12() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "()one/two/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("()".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals(0, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test13() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one()/two/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("one(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("one()".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals("one".length, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals("one".length, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test14() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "(one)/two/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("(one)".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals("one".length, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test15() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "(on)e/two/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("(on)".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals("on".length, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test16() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "o(ne)/two/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("o(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("o(ne)".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals("o".length, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals("one".length, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test17() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "o(n)e/two/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("o(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("o(n)".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals("o".length, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals("on".length, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test18() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "o()ne/two/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("o(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("o()".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals("o".length, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(0, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals("o".length, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test19() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "o()n()e/two/three"
        )

        Assertions.assertEquals(2, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("o(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("o()".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("o".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("o".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }

        replacements[1].run {
            Assertions.assertEquals("o()n(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("o()n()".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("on".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("on".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test1A() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "o()()ne/two/three"
        )

        Assertions.assertEquals(2, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("o(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("o()".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("o".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("o".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }

        replacements[1].run {
            Assertions.assertEquals("o()(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("o()()".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("o".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("o".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test1B() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "(segment)(one)/two/three"
        )

        Assertions.assertEquals(2, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("(segment)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("segment".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }

        replacements[1].run {
            Assertions.assertEquals("(segment)(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("(segment)(one)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("segment".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("segmentone".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test1C() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "the-(segment)-(one)/two/three"
        )

        Assertions.assertEquals(2, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("the-(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("the-(segment)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("the-".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("the-segment".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }

        replacements[1].run {
            Assertions.assertEquals("the-(segment)-(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("the-(segment)-(one)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("the-segment-".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("the-segment-one".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test22() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/()two/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("one/(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("one/()".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals(0, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test23() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/two()/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("one/two(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("one/two()".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals("two".length, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals("two".length, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test24() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/(two)/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("one/(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("one/(two)".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals("two".length, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test25() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/(tw)o/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("one/(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("one/(tw)".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals(0, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals("tw".length, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test26() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/t(wo)/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("one/t(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("one/t(wo)".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals("t".length, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals("two".length, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test27() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/t(w)o/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("one/t(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("one/t(w)".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals("t".length, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals("tw".length, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test28() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/t()wo/three"
        )

        Assertions.assertEquals(1, replacements.size)

        val replacement: Replacement = replacements[0]

        Assertions.assertEquals("one/t(".lastIndex, replacement.inExpressionString.start)
        Assertions.assertEquals("one/t()".lastIndex, replacement.inExpressionString.end)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.start!!.segment)
        Assertions.assertEquals("t".length, replacement.inExpressionPathPattern.start!!.character)

        Assertions.assertEquals(1, replacement.inExpressionPathPattern.end!!.segment)
        Assertions.assertEquals("t".length, replacement.inExpressionPathPattern.end!!.character)

        Assertions.assertEquals(-1, replacement.substituteMarkerInExpressionString)
        Assertions.assertEquals("", replacement.substitute)
    }

    @Test
    fun test29() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/t()w()o/three"
        )

        Assertions.assertEquals(2, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("one/t(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("one/t()".lastIndex, inExpressionString.end)

            Assertions.assertEquals(1, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("t".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(1, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("t".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }

        replacements[1].run {
            Assertions.assertEquals("one/t()w(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("one/t()w()".lastIndex, inExpressionString.end)

            Assertions.assertEquals(1, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("tw".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(1, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("tw".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test2A() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/t()()wo/three"
        )

        Assertions.assertEquals(2, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("one/t(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("one/t()".lastIndex, inExpressionString.end)

            Assertions.assertEquals(1, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("t".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(1, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("t".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }

        replacements[1].run {
            Assertions.assertEquals("one/t()(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("one/t()()".lastIndex, inExpressionString.end)

            Assertions.assertEquals(1, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("t".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(1, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("t".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test2B() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/(segment)(two)/three"
        )

        Assertions.assertEquals(2, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("one/(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("one/(segment)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(1, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(1, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("segment".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }

        replacements[1].run {
            Assertions.assertEquals("one/(segment)(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("one/(segment)(two)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(1, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("segment".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(1, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("segmenttwo".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test2C() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/the-(segment)-(two)/three"
        )

        Assertions.assertEquals(2, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("one/the-(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("one/the-(segment)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(1, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("the-".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(1, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("the-segment".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }

        replacements[1].run {
            Assertions.assertEquals("one/the-(segment)-(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("one/the-(segment)-(two)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(1, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("the-segment-".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(1, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("the-segment-two".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test31() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "o(n)e/(tw)o/(th)r(e)e"
        )

        Assertions.assertEquals(4, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("o(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("o(n)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("o".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("on".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }

        replacements[1].run {
            Assertions.assertEquals("o(n)e/(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("o(n)e/(tw)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(1, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(1, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("tw".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }

        replacements[2].run {
            Assertions.assertEquals("o(n)e/(tw)o/(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("o(n)e/(tw)o/(th)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(2, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(2, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("th".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }

        replacements[3].run {
            Assertions.assertEquals("o(n)e/(tw)o/(th)r(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("o(n)e/(tw)o/(th)r(e)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(2, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("thr".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(2, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("thre".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test41() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "(one/two/three/four)"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("(one/two/three/four)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(3, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("four".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }

    }

    @Test
    fun test42() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "(one/two/three/fo)ur"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("(one/two/three/fo)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(3, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("fo".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test43() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "(one/two/three/)four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("(one/two/three/)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(3, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test44() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "(one/two/three)/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("(one/two/three)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(2, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("three".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test45() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "(one/two/thr)ee/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("(one/two/thr)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(2, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("thr".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test46() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "on(e/two/thr)ee/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("on(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("on(e/two/thr)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("on".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(2, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("thr".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test47() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one(/two/thr)ee/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("one(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("one(/two/thr)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("one".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(2, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("thr".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test48() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/(two/thr)ee/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("one/(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("one/(two/thr)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(1, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(2, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("thr".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test4A() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/t(wo/thr)ee/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("one/t(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("one/t(wo/thr)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(1, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("t".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(2, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("thr".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test4B() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "one/two(/)three/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("one/two(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("one/two(/)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(1, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("two".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(2, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals(-1, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test51() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "(>)one/two/three/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("(>)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals("(>".lastIndex, substituteMarkerInExpressionString)
            Assertions.assertEquals("", substitute)
        }
    }

    @Test
    fun test52() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "(>AAA)one/two/three/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("(>AAA)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals("(>".lastIndex, substituteMarkerInExpressionString)
            Assertions.assertEquals("AAA", substitute)
        }
    }

    @Test
    fun test53() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "(o>AAA)ne/two/three/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("(o>AAA)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("o".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals("(o>".lastIndex, substituteMarkerInExpressionString)
            Assertions.assertEquals("AAA", substitute)
        }
    }

    @Test
    fun test54() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "o(n>AAA)e/two/three/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("o(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("o(n>AAA)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("o".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(0, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("on".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals("o(n>".lastIndex, substituteMarkerInExpressionString)
            Assertions.assertEquals("AAA", substitute)
        }
    }

    @Test
    fun test55() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "o(ne/two/>AAA)three/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("o(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("o(ne/two/>AAA)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("o".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(2, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals("o(ne/two/>".lastIndex, substituteMarkerInExpressionString)
            Assertions.assertEquals("AAA", substitute)
        }
    }

    @Test
    fun test56() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "o(ne/two/th>AAA)ree/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("o(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("o(ne/two/th>AAA)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("o".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(2, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("th".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals("o(ne/two/th>".lastIndex, substituteMarkerInExpressionString)
            Assertions.assertEquals("AAA", substitute)
        }
    }

    @Test
    fun test57() {
        val (_, replacements) = ExpressionParser.parseExpression(
            "o(ne/two/three>AAA)/four"
        )

        Assertions.assertEquals(1, replacements.size)

        replacements[0].run {
            Assertions.assertEquals("o(".lastIndex, inExpressionString.start)
            Assertions.assertEquals("o(ne/two/three>AAA)".lastIndex, inExpressionString.end)

            Assertions.assertEquals(0, inExpressionPathPattern.start!!.segment)
            Assertions.assertEquals("o".length, inExpressionPathPattern.start!!.character)

            Assertions.assertEquals(2, inExpressionPathPattern.end!!.segment)
            Assertions.assertEquals("three".length, inExpressionPathPattern.end!!.character)

            Assertions.assertEquals("o(ne/two/three>".lastIndex, substituteMarkerInExpressionString)
            Assertions.assertEquals("AAA", substitute)
        }
    }

    @Test
    fun test61() {
        try {
            ExpressionParser.parseExpression("o(ne/(two/three)/fou)r")
        } catch (e: IllegalStateException) {
            println("Message: ${e.message}")
            return
        } catch (ignored: Exception) {
        }
        Assertions.fail<Any>("Should throw exception on nested parentheses")
    }

    @Test
    fun test62() {
        try {
            ExpressionParser.parseExpression("o(ne/two/three/four")
        } catch (e: IllegalStateException) {
            println("Message: ${e.message}")
            return
        } catch (ignored: Exception) {
        }
        Assertions.fail<Any>("Should throw exception on unclosed")
    }

    @Test
    fun test63() {
        try {
            ExpressionParser.parseExpression("o(ne)(/two/three/four")
        } catch (e: IllegalStateException) {
            println("Message: ${e.message}")
            return
        } catch (ignored: Exception) {
        }
        Assertions.fail<Any>("Should throw exception on unclosed")
    }

    @Test
    fun test64() {
        try {
            ExpressionParser.parseExpression(")one/two/three/four")
        } catch (e: IllegalStateException) {
            println("Message: ${e.message}")
            return
        } catch (ignored: Exception) {
        }
        Assertions.fail<Any>("Should throw exception on closings without openings")
    }

    @Test
    fun test65() {
        try {
            ExpressionParser.parseExpression("o)ne/two/three/four")
        } catch (e: IllegalStateException) {
            println("Message: ${e.message}")
            return
        } catch (ignored: Exception) {
        }
        Assertions.fail<Any>("Should throw exception on closings without openings")
    }

    @Test
    fun test66() {
        try {
            ExpressionParser.parseExpression("one/two/thr)ee/four")
        } catch (e: IllegalStateException) {
            println("Message: ${e.message}")
            return
        } catch (ignored: Exception) {
        }
        Assertions.fail<Any>("Should throw exception on closings without openings")
    }

    @Test
    fun test67() {
        try {
            ExpressionParser.parseExpression("one/two/three/four)")
        } catch (e: IllegalStateException) {
            println("Message: ${e.message}")
            return
        } catch (ignored: Exception) {
        }
        Assertions.fail<Any>("Should throw exception on closings without openings")
    }
}
