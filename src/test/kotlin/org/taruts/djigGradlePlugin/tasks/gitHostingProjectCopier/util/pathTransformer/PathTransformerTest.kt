package org.taruts.djigGradlePlugin.tasks.gitHostingProjectCopier.util.pathTransformer

import org.junit.jupiter.api.Assertions
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import org.taruts.djigGradlePlugin.hostingPlace.util.pathTransformer.PathTransformer

internal class PathTransformerTest {

    companion object {

        @JvmStatic
        private fun testArguments(): List<Arguments> {
            return listOf(
                //@formatter:off
                Arguments.of("**"                                                                       , "one/two/three/four/five/six/seven/eight/nine/ten"),
                Arguments.of("()/()/()/**"                                                              , "one/two/three/four/five/six/seven/eight/nine/ten"),
                Arguments.of("one/two/()/()/()/three/four/five/six/seven/eight/nine/ten"                , "one/two/three/four/five/six/seven/eight/nine/ten"),
                Arguments.of("one/two/()/(>two-and-a-half)/()/three/four/five/six/seven/eight/nine/ten" , "one/two/two-and-a-half/three/four/five/six/seven/eight/nine/ten"),
                Arguments.of("one/two/()/(>two-and-a-half)/()/**/ten"                                   , "one/two/two-and-a-half/three/four/five/six/seven/eight/nine/ten"),
                Arguments.of("**/three/f*/*/six/(*/eight)/**"                                           , "one/two/three/four/five/six/nine/ten"),
                Arguments.of("**/three/f*/*/six/(*/eight>AAA)/**"                                       , "one/two/three/four/five/six/AAA/nine/ten"),
                Arguments.of("**/three/(f*/*>BBB)/six/(*/eight>AAA)/**"                                 , "one/two/three/BBB/six/AAA/nine/ten"),
                Arguments.of("**/thr(ee/f*/*/six/*/ei)ght/**"                                           , "one/two/thrght/nine/ten"),
                Arguments.of("**/thr(>-EE-)(ee/f*/*/six/*/ei)(>-EI-)ght/**"                             , "one/two/thr-EE--EI-ght/nine/ten"),
                Arguments.of("one/**(>1)(>2)/ten"                                                       , "one/two/three/four/five/six/seven/eight/nine12/ten"),
                Arguments.of("one/**(>/1)(>/2)/ten"                                                     , "one/two/three/four/five/six/seven/eight/nine/1/2/ten"),
                Arguments.of("one/(>1)(>2)**(>1)(>2)/ten"                                               , "one/12two/three/four/five/six/seven/eight/nine12/ten"),
                Arguments.of("one/(>1/)(>2/)**(>/1)(>/2)/ten"                                           , "one/1/2/two/three/four/five/six/seven/eight/nine/1/2/ten"),
                Arguments.of("**/three/f?ur/*/six/(*/eight>AAA)/**"                                     , "one/two/three/four/five/six/AAA/nine/ten"),
                Arguments.of("one/two/(>AAA)/**"                                                        , "one/two/AAA/three/four/five/six/seven/eight/nine/ten"),
                Arguments.of("**/(>AAA)/nine/ten"                                                       , "one/two/three/four/five/six/seven/eight/AAA/nine/ten"),
                Arguments.of("(**>AAA)"                                                                 , "AAA"),
                Arguments.of("(**>AAA/BBB)"                                                             , "AAA/BBB"),
                Arguments.of("one/two/three/four/five/(>AAA)/six/seven/eight/nine/ten"                  , "one/two/three/four/five/AAA/six/seven/eight/nine/ten"),
                Arguments.of("one/two/three/four/five/(**>AAA)/six/seven/eight/nine/ten"                , "one/two/three/four/five/AAA/six/seven/eight/nine/ten"),
                Arguments.of("one/two/three/four/(five/six>AAA)/seven/eight/nine/ten"                   , "one/two/three/four/AAA/seven/eight/nine/ten"),
                Arguments.of("one/two/three/four/five/six/seven/eight/nine/ten/(>AAA)"                  , "one/two/three/four/five/six/seven/eight/nine/ten/AAA"),
                Arguments.of("(>AAA)/one/two/three/four/five/six/seven/eight/nine/ten"                  , "AAA/one/two/three/four/five/six/seven/eight/nine/ten"),
                Arguments.of("one/two/three/four/five/six/seven/eight/(nine/ten>AAA)"                   , "one/two/three/four/five/six/seven/eight/AAA"),
                Arguments.of("one/two/three/four/five/six/seven/eight/(**>AAA)"                         , "one/two/three/four/five/six/seven/eight/AAA"),
                Arguments.of("one/(two/three/four/five/six/seven/eight/nine)/ten"                       , "one/ten"),
                Arguments.of("one(/)two(/)three/four(/)five(/)six/seven(/)eight(/)nine(/)ten"           , "onetwothree/fourfivesix/seveneightnineten"),
                Arguments.of("one/two/(three>segment-@)/four/five/six/seven/eight/nine/ten"             , "one/two/segment-three/four/five/six/seven/eight/nine/ten"),
                Arguments.of("one/two/thr(ee/fo>_@_)ur/five/six/seven/eight/nine/ten"                   , "one/two/thr_ee/fo_ur/five/six/seven/eight/nine/ten"),
                Arguments.of("one/two/thr(ee/four/five/six/seven/ei>_@_)ght/nine/ten"                   , "one/two/thr_ee/four/five/six/seven/ei_ght/nine/ten"),
                //@formatter:on
            )
        }
    }

    private val path = listOf(
        TestPathSegment("one", true),
        TestPathSegment("two", true),
        TestPathSegment("three", true),
        TestPathSegment("four", true),
        TestPathSegment("five", true),
        TestPathSegment("six", true),
        TestPathSegment("seven", true),
        TestPathSegment("eight", true),
        TestPathSegment("nine", true),
        TestPathSegment("ten", true),
    )

    private val pathTransformer = PathTransformer<TestPathSegment>()

    private var groupsSeparator: String = "/"

    private val parse: (str: String?) -> List<TestPathSegment> = { str ->
        str?.split("/")?.map { TestPathSegment(it, false) } ?: listOf()
    }

    private val format: (segments: List<TestPathSegment>) -> String = { segments: List<TestPathSegment> ->
        segments.joinToString(groupsSeparator)
    }

    @ParameterizedTest(name = "{index} - {0} should transform path to {1}")
    @MethodSource("testArguments")
    fun test(expression: String, expected: String) {
        val newPath: List<TestPathSegment> = pathTransformer.transformPath(path, expression, parse, format)
        Assertions.assertEquals(expected, format(newPath))
    }

    class TestPathSegment(val text: String, val original: Boolean) {
        override fun toString(): String {
            return text
        }
    }
}
