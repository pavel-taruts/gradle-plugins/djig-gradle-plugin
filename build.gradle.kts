import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm") version "1.7.10"
    `java-gradle-plugin`
    `maven-publish`
    id("org.jetbrains.dokka") version "1.8.10"
    id("com.gradle.plugin-publish") version "1.0.0"
}

group = "org.taruts"
version = "1.0.0"

repositories {
    mavenLocal()
    mavenCentral()
}

dependencies {
    implementation("org.taruts:taruts-process-utils:1.0.3")
    implementation("org.taruts:taruts-git-utils:1.0.1")
    implementation("org.taruts:taruts-gradle-utils:1.0.1")
    implementation("org.taruts:property-file-section-utils:1.0.0")
    implementation("org.taruts.djig:djig-properties-spring-boot-starter:1.0.3")

    implementation("org.apache.commons:commons-lang3:3.12.0")
    implementation("commons-io:commons-io:2.11.0")
    implementation("org.apache.commons:commons-text:1.10.0")

    implementation("org.gitlab4j:gitlab4j-api:5.0.1")
    implementation("org.kohsuke:github-api:1.308")

    testImplementation(platform("org.junit:junit-bom:5.9.0"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

tasks.test {
    useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
    kotlinOptions.jvmTarget = "1.8"
}

pluginBundle {
    website = "https://djig.org/djig-gradle-plugin/"
    vcsUrl = "https://gitlab.com/pavel-taruts/gradle-plugins/djig-gradle-plugin.git"
    tags = listOf("djig", "dynamic", "runtime", "class", "refresh", "reload", "spring", "context")
}

gradlePlugin {
    plugins {
        create("djig") {
            id = "org.taruts.djig"
            displayName = "DJiG"

            description = """
            Adds a task for copying DJiG dynamic projects 
            from the dev environment (that all the developers in the team can access)
            to the local development environment
            """.trimIndent()

            implementationClass = "org.taruts.djigGradlePlugin.DjigPlugin"
        }
    }
}
